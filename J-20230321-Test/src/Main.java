import java.util.*;
public class Main {
    public static void main(String[]args){
        Scanner scan=new Scanner(System.in);
        String str=scan.nextLine();
        int count=0;//计数
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='"'){
                do{
                    i++;
                }while(str.charAt(i)!='"');
            }
            if(str.charAt(i)==' '){
                count++;
            }

        }
        //输出解析的参数个数
        System.out.println(count+1);
        //接下来输出各个参数
        //找一个标志来表示是双引号内部元素
        // boolean flag=false;
        boolean flag=true;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='"'){
                flag=!flag;
            }
            //非空格和引号外的都输出
            if(str.charAt(i)!='"'&&str.charAt(i)!=' '){
                System.out.print(str.charAt(i));
            }
            //引号内的空格
            if(str.charAt(i)==' '&&flag==false){
                System.out.print(str.charAt(i));
            }
            //换行
            if(str.charAt(i)==' '&&flag==true){
                System.out.println();
            }
        }
    }

}