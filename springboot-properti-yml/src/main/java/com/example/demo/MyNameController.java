package com.example.demo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;

import java.util.List;
@ConfigurationProperties("myName")
@Controller
@Data
public class MyNameController {
    public List<String> name;
}
