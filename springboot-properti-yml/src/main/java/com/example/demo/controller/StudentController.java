package com.example.demo.controller;

import com.example.demo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class StudentController {
    @Autowired
    private Student student;

    public Student getStudent(){
        return student;
    }
}
