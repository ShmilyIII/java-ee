package thread;

public class ThreadDemo3 {
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(()->{
                System.out.println("hello thread");
        });
        System.out.println("start之前线程状态："+t.getState());
        t.start();
        System.out.println("start之后线程状态："+t.getState());

        t.join();
        System.out.println("join之后"+t.getState());
    }
}
