package thread;
//实现单例模式的懒汉模式，线程安全的
class SingletonLazy{

   volatile private static SingletonLazy singletonLazy=null;

    public static SingletonLazy getSingletonLazy() {

        if(singletonLazy==null){
            synchronized (SingletonLazy.class){
                if(singletonLazy==null){
                    singletonLazy=new SingletonLazy();
                }
            }
        }
        return singletonLazy;
    }

    //封禁构造方法
    private SingletonLazy(){}
}
public class ThreadDemo9 {
    public static void main(String[] args) {

        Thread t1=new Thread(()->{
            SingletonLazy s1=SingletonLazy.getSingletonLazy();
            System.out.println("s1 ="+s1);

        });

        Thread t2=new Thread(()->{
            SingletonLazy s2=SingletonLazy.getSingletonLazy();
            System.out.println("s2 ="+s2);
        });

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
