package thread;
//死锁演示和破解

public class ThreadDemo5 {
    public static void main(String[] args) {

        //创建两个锁对象
        Object locker1=new Object();
        Object locker2=new Object();

        //线程t1先对locker1加锁再获取locker2的锁
        Thread t1=new Thread(()->{

            synchronized (locker1){//对locker1加锁
                System.out.println("t1成功获取到locker1");
                try {
                    Thread.sleep(1000);  //让获取到locker1后进入阻塞，给线程t2获取locker2的机会
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (locker2){//对locker1加锁
                    System.out.println("t1成功获取到locker1和locker2");
                }

            }



        });
        t1.start();
        //线程t2先对locker2加锁再获取locker1的锁
        Thread t2=new Thread(()->{

            synchronized (locker1){//对locker2加锁
                System.out.println("t2成功获取到locker1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (locker2){//对locker1加锁  破解?编号大小，都先加locker1再加locker2
                    System.out.println("t2成功获取到locker2和locker1");
                }
            }

        });

        t2.start();
    }
}
