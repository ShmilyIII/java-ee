package thread;

import java.util.Timer;
import java.util.TimerTask;

public class ThreadDemo13 {
    public static void main(String[] args) {
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello 我是定时器，任务开始执行！");
            }
        },3000);
    }
}
