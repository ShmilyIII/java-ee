package thread;
//自己实现阻塞队列

class MyBlockingQueue{
    //借助循环队列实现
    private int[] item=new int[1000];
    private int size=0;
    private int head=0;
    private int tail=0;

    public void put(int num) throws InterruptedException {
        //如果队列满，就阻塞
        synchronized (this) {
            if(size==item.length){
                this.wait();
            }
            item[tail]=num;
            tail++;
            if(tail>=item.length){
                tail=0;
            }
            size++;
            this.notify();
        }
    }

    public Integer take() throws InterruptedException {
        Integer res;
        synchronized (this) {
            res = 0;
            //如果队列空，就阻塞
            if(size==0){
                this.wait();
            }

            res=item[head];
            head++;
            if(head>=item.length){
                head=0;
            }
            size--;
            this.notify();
        }
        return res;

    }

}
public class ThreadDemo12 {
    public static void main(String[] args) throws InterruptedException {
        MyBlockingQueue queue=new MyBlockingQueue();
//        queue.put(1);
//        queue.put(2);
//        queue.put(3);
//        queue.put(4);
//
//        System.out.println("取出元素："+queue.take());
//        System.out.println("取出元素："+queue.take());
//        System.out.println("取出元素："+queue.take());
//        System.out.println("取出元素："+queue.take());
        Thread t1=new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    queue.put(i);
                    System.out.println("生产："+i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2=new Thread(()->{
            int res=0;
            try {
                while(true){
                    res=queue.take();
                    System.out.println("消费："+res);
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();
    }
}
