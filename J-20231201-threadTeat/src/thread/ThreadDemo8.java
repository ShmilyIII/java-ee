package thread;
//单例模式---饿汉模式实现
class SingletonHungry{
    //直接创建类对象
    private static SingletonHungry singletonHungry=new SingletonHungry();

    //通过getSingletonHungry获取实例
    public static SingletonHungry getSingletonHungry(){
        return singletonHungry;
    }
    //封禁构造方法，不允许new对象
    private SingletonHungry(){}
}
public class ThreadDemo8 {
    public static void main(String[] args) {

        SingletonHungry s1=SingletonHungry.getSingletonHungry();
        SingletonHungry s2=SingletonHungry.getSingletonHungry();
        System.out.println(s1==s2);

    }
}
