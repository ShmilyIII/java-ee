package thread;
//使用线程提供的标志位

public class ThreadDemo2 {
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(()->{
            while (!Thread.currentThread().isInterrupted()){
                System.out.println("Hello Thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //该线程是否中断，取决于代码的实现，比如使用break就直接中断了，什么都不写，后续还会接着执行，也可以先sleep再break，这叫等待后再中断
            }
        });
        t.start();

        Thread.sleep(3000);
        //通知t线程中断
        t.interrupt();
    }
}
