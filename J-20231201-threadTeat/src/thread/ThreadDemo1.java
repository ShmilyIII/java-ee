package thread;

public class ThreadDemo1 {
    static boolean flag=true; //手动创建标志位,使用类属性
    public static void main(String[] args) throws InterruptedException {

        Thread t =new Thread(()->{
            while (flag){
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });

        t.start();
        Thread.sleep(3000);
        flag=false;

    }
}
