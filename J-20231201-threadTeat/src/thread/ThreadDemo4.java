package thread;
//线程安全
class Counter{
    public static int count=0;

   synchronized public void add(){
        count++;
    }
}
public class ThreadDemo4 {
    public static void main(String[] args) {
        Counter counter=new Counter();
        Thread t1=new Thread(()->{
            for (int i = 0; i <50000; i++) {
                counter.add();
            }
        });

        Thread t2=new Thread(()->{
            for (int i = 0; i <50000; i++) {
                counter.add();
            }
        });

        t1.start();
        t2.start();


        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(counter.count);

    }
}
