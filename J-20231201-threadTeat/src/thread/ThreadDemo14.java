package thread;
//实现一个定时器

import java.util.concurrent.PriorityBlockingQueue;

//实现任务
class MyTask implements Comparable<MyTask>{

    //任务
    private Runnable runnable;

    //时间  时间用当前的毫秒级时间戳+需要的时间
    private long after;

    //获取时间
    public long getAfter() {
        return after;
    }

    //执行run方法
    public void run(){
        runnable.run();
    }

    public MyTask(Runnable runnable, long after) {
        this.runnable = runnable;
        this.after = after;
    }
    //按照时间的先后，所以任务需要实现Comparable接口
    //重写compare to方法

    @Override
    public int compareTo(MyTask o) {
        return (int)(this.getAfter()-o.getAfter());
    }
}
class MyTimer{
    //在定时器中使用一个优先级阻塞队列来存储任务，
   private PriorityBlockingQueue<MyTask> queue=new PriorityBlockingQueue();//队列
    //使用一个锁对象进行等待和唤醒
   private Object locker=new Object();
    //使用一个扫描线程来扫描线程中的任务
    public MyTimer(){
        //使用构造方法实现任务的执行
            //使用这个线程来扫描队列中的任务。如果任务时间没有到就接着扫描队首元素
              Thread t = new Thread(()->{
                  while (true){
                      synchronized (locker) {
                          try {
                              MyTask task= queue.take();//取出队首元素
                              long curTime=System.currentTimeMillis();
                              //如果没有到时间就再塞回队列
                              if(curTime<task.getAfter()){
                                  queue.put(task);
                                  //然后计算要等待的时间进行等待
                                  locker.wait(task.getAfter()-curTime);
                              }else{
                                  //如果到时间就执行run方法
                                  task.run();
                              }

                          } catch (InterruptedException e) {
                              e.printStackTrace();
                          }
                      }
                  }

                });
                t.start();
    }

    //实现schedule方法
    public void schedule(Runnable runnable,long after){
        MyTask myTask=new MyTask(runnable,System.currentTimeMillis()+after);
        queue.put(myTask);
        synchronized (locker) {
            locker.notify();//加入新的任务就唤醒
        }

    }
}

public class ThreadDemo14 {
    public static void main(String[] args) {
        MyTimer myTimer=new MyTimer();
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("我是任务2");
            }
        },3000);

        MyTimer myTimer1=new MyTimer();
        myTimer1.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("我是任务1");
            }
        },2000);
    }
}
