package thread;
//通过使用三个线程打印ABC来练习一下wait和notify的使用
public class ThreadDemo7 {
    public static void main(String[] args) throws InterruptedException {
        //创建两个锁对象来进行控制顺序
        Object locker1=new Object();
        Object locker2=new Object();

        Thread t1 =new Thread(()->{

            System.out.println("A");
            synchronized (locker1){
                locker1.notify();
            }

        });


        Thread t2 =new Thread(()->{
            synchronized (locker1) {
                try {
                    locker1.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("B");

            synchronized (locker2){
                locker2.notify();
            }

        });


        Thread t3 =new Thread(()->{
            synchronized (locker2) {
                try {
                    locker2.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("C");
        });



        t2.start();
        t3.start();
        Thread.sleep(1000);
        t1.start();
    }
}
