import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class ThreadDemo11 {
    public static void main(String[] args) {
        //使用阻塞队列
        BlockingDeque<Integer>blockingDeque=new LinkedBlockingDeque<>();

        //生产者
        Thread t1=new Thread(()->{
            for (int i = 0; i <10 ; i++) {
                try {
                    blockingDeque.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("生产者生产数字："+i);
            }




        });
        //消费者
        Thread t2=new Thread(()->{
            while (true){
                try {
                int res= blockingDeque.take();
                    System.out.println("消费者消费数字："+res);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });

        t1.start();
        t2.start();
    }
}
