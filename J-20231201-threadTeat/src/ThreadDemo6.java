
class Counter1{
    public int num=0;

    synchronized public void add(){  //加锁一次
        synchronized (this){
            num++;
        }
    }

}
public class ThreadDemo6 {

    //synchronized监视锁是可重入锁
    public static void main(String[] args) throws InterruptedException {
        Counter1 counter1=new Counter1();
        Thread t1 =new Thread(()->{
            for (int i = 0; i <500_0000; i++) {
                counter1.add();
            }
        });
        t1.start();
        Thread.sleep(3000);
        System.out.println(counter1.num);
    }


}
