//获取url中的value
function getParamByKey(key){
    var params=location.search;//拿到地址栏后面的参数
    params=params.substring(1);//去掉前面的问号"?"

    var paramArr=params.split("&");//使用数组截取key-value的组合

    if(paramArr!=null&&paramArr.length>0){
        //paramArr数组中存在的所有key-value进行比较
        for(var i=0;i<paramArr.length;i++){
            var item=paramArr[i];//比如这里找id=1
            var itemArr=item.split("=");//分解key和value
            //key == 目标key
            if(itemArr.length==2 && itemArr[0]==key){
                return itemArr[1];//拿到当前key的value
            }
        }

    }
    return null;

}