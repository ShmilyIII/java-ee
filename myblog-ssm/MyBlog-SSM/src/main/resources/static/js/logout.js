// 退出登录为公共方法
        // 注销功能
function logout(){

    if(confirm("是否退出？")){
        // 执行退出操作
        jQuery.ajax({
            url:"/user/logout",
            type:"POST",
            data:{},//cookie和session可以确定目标对象
            success:function(res){
                if(res.code==200 && res.data==1){
                    // 注销成功，跳转到文章首页（文章列表页）（现在会退出到登录页面）
                    location.href = "blog_list.html";
                }else{
                    //注销失败
                    alert("抱歉：操作失败！"+res.msg);
                }
            }
        });
    }


}