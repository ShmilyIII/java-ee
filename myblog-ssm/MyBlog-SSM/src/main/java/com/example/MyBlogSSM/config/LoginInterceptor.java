package com.example.MyBlogSSM.config;

import com.example.MyBlogSSM.common.ApplicationVariable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component //注入到Spring中 方便在Myconfig中注入

//登录拦截器
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断用户登录
        //1、拿到session对象
        HttpSession session=request.getSession(false);//默认为true,这里如果有session就得到，没有就不要强求
        if(session!=null && session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO)!=null){
            //用户已经登录
            return true;
        }
        //当代码执行到这里，说明用户没有登录
        response.sendRedirect("/login.html");//只要没有登录，就跳转到登录页面
        return false;
    }
}
