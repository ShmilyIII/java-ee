package com.example.MyBlogSSM.service;

import com.example.MyBlogSSM.entity.UserInfo;
import com.example.MyBlogSSM.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    //注册方法
    public int reg(UserInfo userInfo){
        return userMapper.reg(userInfo);
    }

    //登录方法
    public UserInfo login(String username){
        return userMapper.login(username);
    }

}
