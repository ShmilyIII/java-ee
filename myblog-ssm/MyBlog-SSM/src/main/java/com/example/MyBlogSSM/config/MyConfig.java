package com.example.MyBlogSSM.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器
 */
@Configuration  //托管给Spring
public class MyConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;
    //配置拦截规则
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**") //拦截所有url地址
                .excludePathPatterns("/login.html") //放行的地址
                .excludePathPatterns("/reg.html")
                .excludePathPatterns("/blog_list.html")
                .excludePathPatterns("/blog_content.html")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/editor.md/**")
                .excludePathPatterns("/img/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/user/reg") //放行注册和登录接口
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/article/addrcount")
                .excludePathPatterns("/article/getdetail")//放行文章详情接口
                .excludePathPatterns("/user/islogin")
                .excludePathPatterns("/article/getlistbypage")
                .excludePathPatterns("/article/getcount")
        ;

    }
}
