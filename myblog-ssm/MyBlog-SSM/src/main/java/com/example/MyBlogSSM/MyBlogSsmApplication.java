package com.example.MyBlogSSM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyBlogSsmApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBlogSsmApplication.class, args);
    }


}
