package com.example.MyBlogSSM.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//工具类
//统一异常处理
@RestControllerAdvice
public class MyExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public AjaxResult doException(Exception e){
        return AjaxResult.fail(-1,e.getMessage());
    }
}
