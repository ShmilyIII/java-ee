package com.example.MyBlogSSM.entity.vo;

import com.example.MyBlogSSM.entity.ArticleInfo;
import lombok.Data;

@Data
public class ArticleInfoVO extends ArticleInfo {
    private String username;//发表文章的名字
}
