package com.example.MyBlogSSM.mapper;

import com.example.MyBlogSSM.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

//用户数据持久层
@Mapper
public interface UserMapper {
    int reg(UserInfo userInfo); //注册

    UserInfo login(@Param("username") String username);//登录  （返回的是对象） 在controller中对比该userinfo对象的密码

}
