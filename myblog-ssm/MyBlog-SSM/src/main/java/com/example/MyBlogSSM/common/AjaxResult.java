package com.example.MyBlogSSM.common;

import lombok.Data;

import java.io.Serializable;
//工具类
/**
 * 统一JSON返回对象
 */
@Data //该注解包括了get和set
public class AjaxResult implements Serializable {
    private int code;
    private String msg;
    private Object data;//data信息有可能是任意的信息

    /**
     * 返回成功数据
     * @param
     * @param
     * @return
     */

    public static AjaxResult success(Object data) {
        AjaxResult result = new AjaxResult();
        result.setCode(200);
        result.setMsg("");
        result.setData(data);
        return result;
    }
    public static AjaxResult success(int code, Object data) {
        AjaxResult result = new AjaxResult();
        result.setCode(code);
        result.setMsg("");
        result.setData(data);
        return result;
    }
    public static AjaxResult success(int code, String msg, Object data) {
        AjaxResult result = new AjaxResult();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    /**
     * 返回错误数据
     * @param code
     * @param msg
     * @param
     * @return
     */
    public static AjaxResult fail(Integer code,String msg){
        AjaxResult ajaxResult=new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(null);
        return ajaxResult;
    }

    public static AjaxResult fail(Integer code,String msg,Object data){
        AjaxResult ajaxResult=new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(data);
        return ajaxResult;
    }

}
