package com.example.MyBlogSSM.common;

import com.example.MyBlogSSM.entity.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//工具类
/**
 * 获取当前的登录用户
 */
public class UserSessionTools {
    public static UserInfo getLoginUser(HttpServletRequest request){
        HttpSession session=request.getSession(false);
        if(session!=null && session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO)!=null){
            return (UserInfo) session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO);
        }

        return null;
    }
}
