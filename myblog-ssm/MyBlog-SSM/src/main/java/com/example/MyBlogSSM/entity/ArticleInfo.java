package com.example.MyBlogSSM.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ArticleInfo {
    private int id;
    private String title;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")//设置时间格式
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int uid;
    private int rcount;//阅读量
    private int state;
    
}
