package com.example.MyBlogSSM.controller;

import com.example.MyBlogSSM.common.AjaxResult;
import com.example.MyBlogSSM.common.ApplicationVariable;
import com.example.MyBlogSSM.common.StringTools;
import com.example.MyBlogSSM.common.UserSessionTools;
import com.example.MyBlogSSM.entity.ArticleInfo;
import com.example.MyBlogSSM.entity.UserInfo;
import com.example.MyBlogSSM.entity.vo.ArticleInfoVO;
import com.example.MyBlogSSM.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.InsufficientResourcesException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    //文章添加
    @RequestMapping("/add")  //传参数和传对象都可，但是传对象可以方便后续的修改，可维护性比较好，不管属性怎么变，这里都比较容易改
    public AjaxResult add(ArticleInfo articleInfo, HttpServletRequest request){
        //request方便取session从而获得uid
        /**
         * 1、非空校验
         */
        if(articleInfo==null || !StringUtils.hasLength(articleInfo.getTitle())

                            || !StringUtils.hasLength(articleInfo.getContent()))
            return AjaxResult.fail(-1,"参数异常！");
        /**
         * 2、组装数据，得到uid
         */
//        HttpSession session=request.getSession();//能走到这，拦截器是通过的，session中是有值的，所以不用再进行非空判断
//        //获取到当前用户的session key 并强转为userinfo类型，从而获取到userinfo的id，也就是uid
//        UserInfo userInfo=(UserInfo) session.getAttribute(ApplicationVariable.SESSION_KEY_USERINFO);
        UserInfo userInfo= UserSessionTools.getLoginUser(request);
        articleInfo.setUid(userInfo.getId());

        /**
         * 3、持久化，将结果返回给前端
         */
        int result=articleService.add(articleInfo);
        return AjaxResult.success(result);
    }

    //文章修改时查询当前文章内容并展示

    /**
     * 获取文章的详细信息，但是需要鉴权（判断权限归属是否非当前登录用户）
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/getdetailbyid")
    public AjaxResult getDetailByIdAndUid(Integer id,HttpServletRequest request){
        /**
         * 1、非空校验
         */
        if(id==null || id<=0){
            return AjaxResult.fail(-1,"非法参数");
        }
        /**
         * 2、获取当前的用户信息
         */
        UserInfo userInfo= UserSessionTools.getLoginUser(request);//直接写了个类，方便用于获取用户信息
        /**
         * 将结果返回给前端
         */
        return AjaxResult.success(articleService.getDetailByIdAndUid(id, userInfo.getId()));

    }


    //修改文章
    @RequestMapping("/update")
    public AjaxResult update(ArticleInfo articleInfo,HttpServletRequest request){
        /**
         * 1、非空校验
         */
        if(null==articleInfo || articleInfo.getId()<=0 ||
            !StringUtils.hasLength(articleInfo.getTitle())||
            !StringUtils.hasLength(articleInfo.getContent())){
            return AjaxResult.fail(-1,"参数有误");
        }

        /**
         * 2/获取登录用户的id,填充到articleinfo对象中（修改需要验证权限)
         *
         */
        UserInfo userInfo=UserSessionTools.getLoginUser(request);//获取当前用户
        articleInfo.setUid(userInfo.getId());//设置当前文章的uid
        articleInfo.setUpdatetime(LocalDateTime.now());//设置当前文章更改的时间
        int result=articleService.update(articleInfo);
        return AjaxResult.success(result);

    }

    //获取文章详情
    @RequestMapping("/getdetail")
    public AjaxResult getDetail(Integer id){
        //查找文章失败
        if(id==null || id<= 0){
            return AjaxResult.fail(-1,"参数有误");
        }
        //查询成功
        ArticleInfoVO articleInfoVO=articleService.getDetail(id);
        return AjaxResult.success(articleInfoVO);

    }

    //阅读量增加
    @RequestMapping("/addrcount")
    public AjaxResult addRCount(Integer id){
        if(id==null || id<=0){
            return AjaxResult.fail(-1,"参数错误");
        }
        int result=articleService.addRCount(id);
        return AjaxResult.success(result);


    }


    //获取我的博客文章列表

    @RequestMapping("/mylist")
    public AjaxResult myList(HttpServletRequest request){
        UserInfo userInfo=UserSessionTools.getLoginUser(request);//获取到当前用户
        List<ArticleInfo> list =articleService.getListByUid(userInfo.getId());//获取到文章列表
        //将文章正文截取为文章摘要
        for(ArticleInfo item:list){
            String content=StringTools.subLength(item.getContent(),
                    150);
            //item.setContent(StringTools.subLength(item.getContent(), 200));//拿到文章的前200长度的文字作为摘要
            item.setContent(content);
        }

        return AjaxResult.success(list);
    }

    //删除我的文章

    @RequestMapping("/del")
    public AjaxResult del(Integer id,HttpServletRequest request){
        if(id==null || id<=0 ){
            return AjaxResult.fail(-1,"参数错误");
        }
        UserInfo userInfo=UserSessionTools.getLoginUser(request);//获取当前用户
        int result=articleService.del(id, userInfo.getId());
        return AjaxResult.success(result);//成功返回受影响的行数
    }


    //分页功能
    @RequestMapping("/getlistbypage")
    public AjaxResult getListByPage(Integer pageSize,Integer pageIndex){
        if(pageSize==null || pageSize<=0){
            pageSize=2;//默认为2
        }
        if(pageIndex==null || pageIndex<1){
            pageIndex=1;//默认为1
        }
        int offset=(pageIndex-1)*pageSize;//计算分页
        List<ArticleInfo> list=articleService.getListByPage(pageSize,offset);//根据计算的分页获取list
        //多线程并发循环
        list.stream().parallel().forEach(item->{
            item.setContent(StringTools.subLength(item.getContent(),150));
        });

        return AjaxResult.success(list);
    }

    //获取文章总数,方便前端进行总页数的计算
    @RequestMapping("/getcount")
    public AjaxResult getCount(){
        return AjaxResult.success(articleService.getCount());//发送获取到的文章数
    }







}
