package com.example.MyBlogSSM.service;

import com.example.MyBlogSSM.entity.ArticleInfo;
import com.example.MyBlogSSM.entity.vo.ArticleInfoVO;
import com.example.MyBlogSSM.mapper.ArticleMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {
    @Autowired
    private ArticleMapper articleMapper;


    //添加文章
    public int add(ArticleInfo articleInfo){
        return articleMapper.add(articleInfo);
    }

    //修改文章前展示文章内容
    //查询当前文章详情
    public ArticleInfo getDetailByIdAndUid(Integer id,Integer uid){
        return articleMapper.getDetailByIdAndUid(id,uid);
    }

    //修改文章
    public int update(ArticleInfo articleInfo){
        return articleMapper.update(articleInfo);
    }

    //文章详情
    public ArticleInfoVO getDetail(Integer id){
        return articleMapper.getDetail(id);
    }

    //阅读量增加
    public int addRCount(Integer id){
        return articleMapper.addRCount(id);
    }

    //获取我的博客文章列表
    public List<ArticleInfo> getListByUid(Integer uid){
        return articleMapper.getListByUid(uid);
    }

    //删除我的文章
    public int del(Integer id,Integer uid){
        return articleMapper.del(id,uid);
    }

    //分页功能
    public List<ArticleInfo> getListByPage(Integer pageSize,Integer offset){
        return articleMapper.getListByPage(pageSize,offset);

    }
    //获取文章的总页数
    public  Integer getCount(){
        return articleMapper.getCount();
    }

}
