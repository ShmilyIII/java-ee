package com.example.MyBlogSSM.common;
//工具类
/**
 * 全局变量类
 */
public class ApplicationVariable {
    /**
     * 存放当前登录用户的session key
     */
    public static final String SESSION_KEY_USERINFO="SESSION_KEY_USERINFO";
}
