package com.example.MyBlogSSM.common;

import org.springframework.util.StringUtils;

//工具类：截取文章长度来作为文章的摘要
public class StringTools {
    public static String subLength(String val,int maxLength){

        if(!StringUtils.hasLength(val) || maxLength<=0){
            return val;
        }

        if(val.length()<=maxLength){
            return val;
        }

        return val.substring(0,maxLength);
    }
}
