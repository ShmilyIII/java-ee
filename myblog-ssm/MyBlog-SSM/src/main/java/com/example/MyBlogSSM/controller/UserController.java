package com.example.MyBlogSSM.controller;

import com.example.MyBlogSSM.common.AjaxResult;
import com.example.MyBlogSSM.common.ApplicationVariable;
import com.example.MyBlogSSM.common.PasswordTools;
import com.example.MyBlogSSM.common.UserSessionTools;
import com.example.MyBlogSSM.entity.UserInfo;
import com.example.MyBlogSSM.service.UserService;
import javafx.application.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;


    //注册功能
    @RequestMapping("/reg")
    /**
     * 接收前端的username和password并返回统一对象AjaxResult
      */
    //这里直接使用userinfo对象
    public AjaxResult reg(UserInfo userInfo){
        /**
         * 1、进行非空判断
         */
        if(userInfo==null || !StringUtils.hasLength(userInfo.getUsername())
                ||!StringUtils.hasLength(userInfo.getPassword()))
            return AjaxResult.fail(-1,"参数有误！");

        /**
         * 2/调用UserService 执行添加方法 ，并将返回的结果添加AjaxResult.data 进行返回
         */

        //todo:将密码进行加盐加密
        userInfo.setPassword(PasswordTools.encrypt(userInfo.getPassword()));

        int result=userService.reg(userInfo);
        return AjaxResult.success(result);

    }




    //登录功能
    @RequestMapping("/login")
    public AjaxResult login(String username, String password, HttpServletRequest request){
        /**
         * 1、非空判断
         */
        if(!StringUtils.hasLength(username) ||!StringUtils.hasLength(password))
            return AjaxResult.fail(-1,"参数有误！");

        /**
         * 2、调用Userservice 执行login方法
         */
        UserInfo userInfo=userService.login(username);
        //判断用户名是否存在
        if(userInfo==null|| userInfo.getId()<=0){
            return AjaxResult.fail(-2,"用户名或密码错误！");
        }

        //todo:加密密码解密

        //判断密码是否一致(解密后）
        if(!PasswordTools.decrypt(password,userInfo.getPassword())){
            return AjaxResult.fail(-2,"用户名或密码错误！");
        }
        //将当前成功登录的用户信息存储在session中
        HttpSession session= request.getSession();
        session.setAttribute(ApplicationVariable.SESSION_KEY_USERINFO,userInfo);//用户存到session中
        //登录成功
        return AjaxResult.success(1);//返回给前端

    }


    //注销功能（退出操作)
    @RequestMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        HttpSession session=request.getSession();//获取到session
        session.removeAttribute(ApplicationVariable.SESSION_KEY_USERINFO);//移除session并定义为公共变量
        return AjaxResult.success(1);//注销成功
    }

    //判断当前用户是否登录
    @RequestMapping("/islogin")
    public AjaxResult isLogin(HttpServletRequest request){
        if(UserSessionTools.getLoginUser(request)==null){
            //未登录
            return AjaxResult.success(0);
        }
        //登录
        return AjaxResult.success(1);
    }






}
