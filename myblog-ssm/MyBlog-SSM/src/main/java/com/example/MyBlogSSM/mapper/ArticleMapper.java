package com.example.MyBlogSSM.mapper;

import com.example.MyBlogSSM.entity.ArticleInfo;
import com.example.MyBlogSSM.entity.vo.ArticleInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper {
    int add(ArticleInfo articleInfo);//添加文章


    /**
     *
     * @param id 文章id
     * @param uid  文章所属用户id
     * @return  文章
     */
    //修改时展示文章信息 uid和文章的id要对应
    ArticleInfo getDetailByIdAndUid(@Param("id")Integer id,@Param("uid")Integer uid);


    int update(ArticleInfo articleInfo);//修改文章  返回的是受影响的行数

    ArticleInfoVO getDetail(@Param("id")Integer id);//文章详情

    int addRCount(@Param("id")Integer id);//阅读量增加

    List<ArticleInfo> getListByUid(@Param("uid") Integer uid);//获取我的博客列表

    int del(@Param("id") Integer id,@Param("uid") Integer uid);//删除文章

    List<ArticleInfo> getListByPage(@Param("pageSize") Integer pageSize,
                                    @Param("offset")Integer offset);//分页功能

    Integer getCount();//获取文章总数,方便前端进行总页数的计算

}
