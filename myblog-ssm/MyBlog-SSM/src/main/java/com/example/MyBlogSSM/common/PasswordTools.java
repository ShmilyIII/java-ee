package com.example.MyBlogSSM.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * 密码工具类
 * 加盐加密/加盐密码验证(使用md5加密算法）
 *  password 明文密码
 * @return 加盐加密的密码
 */
public class PasswordTools {
    //加盐加密
    public static String encrypt(String password){
        //1、产生随机盐值(使用UUID)
        String salt= UUID.randomUUID().toString().replace("-","");
        //2、使用（盐值+明文密码）得到加密的密码
        String finalPassword= DigestUtils.md5DigestAsHex((salt+password).getBytes());
        //3、将盐值和加密的密码共同返回（合并盐值和加密密码）
        String dbPassword=salt+"$"+finalPassword;
        return dbPassword;
    }


    /**
     *
     * @param password 明文密码
     * @param salt  盐值
     * @return
     */
    //方法重载
    public static String encrypt(String password,String salt){
        //2、使用（盐值+明文密码）得到加密的密码
        String finalPassword= DigestUtils.md5DigestAsHex((salt+password).getBytes());
        //3、将盐值和加密的密码共同返回（合并盐值和加密密码）
        String dbPassword=salt+"$"+finalPassword;
        return dbPassword;
    }

    /**
     *
     * @param password  明文密码（需要验证，不一定对）
     * @param dbPassword 数据库存储的包含盐值+"$"+加盐加密的密码
     * @return  true==密码匹配成功，密码正确 否则失败
     */
    //加盐密码的验证(解密）
    public static boolean decrypt(String password,String dbPassword){
        if(StringUtils.hasLength(password)&& StringUtils.hasLength(dbPassword) &&
        dbPassword.length()==65 && dbPassword.contains("$")){//参数正确
            //1、得到盐值
            String[] passwordArr = dbPassword.split("\\$");
            String salt=passwordArr[0];//盐值
            //2、生成验证密码的加盐加密密码
            String checkPassword= encrypt(password,salt);
            //3、比较验证密码的加盐加密密码和加盐加密密钥
            if(dbPassword.equals(checkPassword)){
                return true;
            }
        }
        return false;
    }

}
