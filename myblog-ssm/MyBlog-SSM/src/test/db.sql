
-- 创建数据库
drop database if exists mycnblog;
create database mycnblog DEFAULT CHARACTER SET utf8mb4;

-- 使用数据数据
use mycnblog;

-- 创建表[用户表]
CREATE TABLE userinfo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL,
    photo VARCHAR(500) DEFAULT '',
    createtime DATETIME DEFAULT CURRENT_TIMESTAMP,
    updatetime DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `state` INT DEFAULT 1
) DEFAULT CHARSET 'utf8mb4';

-- 创建文章表
CREATE TABLE articleinfo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    content TEXT NOT NULL,
    createtime DATETIME DEFAULT CURRENT_TIMESTAMP,
    updatetime DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    uid INT NOT NULL,
    rcount INT NOT NULL DEFAULT 1,
    `state` INT DEFAULT 1
) DEFAULT CHARSET 'utf8mb4';





-- 创建数据库
drop database if exists mycnblog;
create database mycnblog DEFAULT CHARACTER SET utf8mb4;

-- 使用数据数据
use mycnblog;

-- 创建表[用户表]

drop table if exists  userinfo;
create table userinfo(
    id int primary key auto_increment,
    username varchar(100) unique, -- 唯一约束
    password varchar(65) not null,
    photo varchar(500) default '',
    createtime timestamp default CURRENT_TIMESTAMP,
    updatetime timestamp default CURRENT_TIMESTAMP,
    `state` int default 1
) default charset 'utf8mb4';

-- 创建文章表
drop table if exists  articleinfo;
create table articleinfo(
    id int primary key auto_increment,
    title varchar(100) not null,
    content text not null,
    createtime timestamp default CURRENT_TIMESTAMP,
    updatetime timestamp default CURRENT_TIMESTAMP,
    uid int not null,
    rcount int not null default 1,
    `state` int default 1
)default charset 'utf8mb4';
