package Test4;

import java.math.BigInteger;

public class Factorial {

    public static BigInteger factorial(int n) {
        BigInteger result = BigInteger.valueOf(1);
        for (int i = 2; i <= n; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }

    public static void main(String[] args) {
        int n = 10; // 计算10的阶乘
        System.out.println(factorial(n));
    }
}