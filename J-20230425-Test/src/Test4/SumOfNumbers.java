package Test4;
public class SumOfNumbers {

    public static void main(String[] args) {
        long sum = 0; // 存储总和
        for (int i = 1; i <= 99999999; i++) {
            sum += i; // 累加每个整数
        }
        System.out.println("1到99999999之间所有整数的和为：" + sum);
    }
}