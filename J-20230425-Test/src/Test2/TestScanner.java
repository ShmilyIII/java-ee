package Test2;

import java.util.Scanner;

public class TestScanner {
    public static void main(String[] args) {
        String s = "中央电视台:www.cctv.com 清华大学:www.tsinghua.edu.cn 音乐网站:www.violin.com";
        String regex = "[A(http〃I www)\56?\\w+\56{1}\\w+\56{1}\\p{Alpha}]+";
        Scanner scanner = new Scanner(s);
        scanner.useDelimiter(regex);
        while (scanner.hasNext()) {
            String token = scanner.next();
            if (token.startsWith("www.")) {
                System.out.println(token);
            }
        }
    }
}