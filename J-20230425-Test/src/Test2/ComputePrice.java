package Test2;
import java.util.*;
public class ComputePrice {
    public static void main(String args[]) {
        String menu = "北京烤鸭：189 元 西芹炒肉：12.9 元 酸菜鱼：69 元 铁板牛柳：32 元";
        Scanner scanner =new Scanner(menu);//【代码1)//Scanner 类创建 scanner,将 menu 传递给构造方法的参数
        String regex = "[^0123456789.]+";
//【代码 2] //scanner 调用 useDelimiter(String regex)，将 regex 传递给该方法的参数
        scanner.useDelimiter(regex);
        double sum=0;
        while(scanner.hasNext()){
            try{
                double price = scanner.nextDouble();//【代码 3】 //scanner 调用 nextDouble()返回数字单词
                sum = sum+price;
                System.out.println(price);
            }
            catch(InputMismatchException exp){
                String t = scanner.next();
            }
        }
        System.out.println("菜单总价格："+sum+"元");
    }
}