package Test5;

import java.util.regex.*;
public class ReplaceErrorWord {
    public static void main(String args[]) {
        String str = "忘记密码，不要惊慌失错，请登陆 www.yy。 或登陆 www.tt.cc";
        Pattern pattern = null;
        Matcher matcher;
        String regex = "登陆";
        pattern = pattern.compile(regex);//【代码 1】 〃使用 regex 初试化模式对象 pattern
        matcher = pattern.matcher(str);//【代码 2】 //得至 检索 str 的匹配对象 matcher
        while(matcher.find()) {
            String s = matcher.group();
            System.out.print(matcher.start()+"位置出现:");
            System.out.println(s);
        }
        System.out.println("将\"登陆\"替换为\"登录\"的字符串：");
        String result = matcher.replaceAll("登录");
        System.out.println(result);
        pattern= Pattern.compile("惊慌失错" );
        matcher = pattern.matcher(result);
        System.out.println("将、\"惊慌失错\"替换为\"惊慌失措\"的字符串：");
        result = matcher.replaceAll("惊慌失措");
        System.out.println(result);
    }
}