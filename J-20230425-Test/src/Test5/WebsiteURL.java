package Test5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebsiteURL {

    public static void main(String[] args) {
        String str = "清华大学出版社 http://www.tup.com 是著名出版，尤其在计算机图书方面";

        // 正则表达式
        String regex = "(http://|https://)(www\\.)?[a-zA-Z0-9]+\\.[a-zA-Z]+(\\.[a-zA-Z]+)?";
        Pattern pattern = Pattern.compile(regex); // 创建模式对象

        Matcher matcher = pattern.matcher(str); // 创建匹配器
        StringBuilder builder = new StringBuilder(); // 存储所有网址的StringBuilder

        // 循环查找
        while (matcher.find()) {
            builder.append(matcher.group()).append(" "); // 将每个匹配到的网址加入builder
        }

        String result = builder.toString().trim(); // 去掉最后一个空格

        System.out.println(result); // 输出结果
    }
}