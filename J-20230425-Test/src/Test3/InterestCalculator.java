package Test3;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class InterestCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // 接收用户输入的存款金额
        System.out.print("请输入存款金额（元）：");
        double amount = scanner.nextDouble();

        // 接收用户输入的起止时间
        System.out.print("请输入起始日期（yyyy-MM-dd）：");
        String startDateStr = scanner.next();
        System.out.print("请输入结束日期（yyyy-MM-dd）：");
        String endDateStr = scanner.next();

        // 将起止时间解析为日期对象
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate, endDate;
        try {
            startDate = dateFormat.parse(startDateStr);
            endDate = dateFormat.parse(endDateStr);
        } catch (ParseException e) {
            System.err.println("日期格式有误，请重新输入。");
            return;
        }

        // 计算存款时间（以天为单位）
        int days = (int) Math.ceil((endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000.0));

        // 计算利息
        double interest = amount * 0.0001 * days;

        // 输出结果
        System.out.printf("存款金额：%.2f 元\n", amount);
        System.out.printf("存款时间：%d 天\n", days);
        System.out.printf("利息：%.2f 元\n", interest);
    }
}