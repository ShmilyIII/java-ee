package Test3;

import java.util.*;
public class CompareDate {
    public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("输入第一个年，月，日，时，分，秒数据");
    System.out.print("输入年份");
    short yearOne = scanner.nextShort();
    System.out.print("输入月份");
    byte monthOne = scanner.nextByte();
    System.out.print("输入日期");
    byte dayOne = scanner.nextByte();
        System.out.println("输入时");
        byte hOne=scanner.nextByte();
        System.out.println("输入分");
        byte mOne=scanner.nextByte();
        System.out.println("输入秒");
        byte sOne=scanner.nextByte();
    System.out.println("输入第二个年，月，日，时，分，秒数据");
    System.out.print("输入年份");
    short yearTwo = scanner.nextShort();
    System.out.print("输入月份");
    byte monthTwo= scanner.nextByte();
    System.out.print("输入日期");
    byte dayTwo = scanner.nextByte();
        System.out.println("输入时");
        byte hTwo=scanner.nextByte();
        System.out.println("输入分");
        byte mTwo=scanner.nextByte();
        System.out.println("输入秒");
        byte sTwo=scanner.nextByte();
    Calendar calendar = Calendar.getInstance();//【代码 1】 〃初始化日历对象
//【代码 2】 〃将 calendar 的时间设置为 yearOne 年 monthOne 月 dayOne日
        calendar.set(yearOne,monthOne-1,dayOne);
        calendar.set(yearOne,monthOne,dayOne,hOne,mOne,sOne);
    long timeOne =calendar.getTimeInMillis(); //【代码 3】 //calendar 表示的时间转换成毫秒
    calendar.set(yearTwo,monthTwo-1,dayTwo,hTwo,mTwo,sTwo);
    long timeTwo=calendar.getTimeInMillis();
    Date date1 =new Date(timeOne); //【代码 4】 〃用 timeOne 做参数构造 date1
    Date date2 =new Date(timeTwo);
if(date2.equals(date1))
    System.out.println("两个日期的年、月、日、时分秒完全相同");
            else if(date2.after(date1))
    System.out.println("您输入的第二个日期大于第一个日期");
else if(date2.before(date1))
    System.out.println("您输入的第二个日期小于第一个日期");
    long days=Math.abs(timeTwo-timeOne)/(1000*60*60*24);//【代码 5】〃使用 timeTwo,timeOne 计算两个日期相隔天数
    System.out.println(yearOne+"年"+monthOne+"月"+dayOne+"和" +yearTwo+"年"+monthTwo+"月"+dayTwo+"相隔"+days+"天" );
}
}