import java.util.*;

public class Solution {
    public ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {


        ArrayList<Integer>list=new ArrayList<>();
        if(input==null||k<=0){
            return list;
        }
        PriorityQueue<Integer>queue=new PriorityQueue<>(new Comparator<Integer>(){

            public int compare(Integer o1,Integer o2){

                return o2.compareTo(o1);
            }

        });
        for(int i=0;i<k;i++){
            queue.offer(input[i]);
        }

        for(int i=k;i<input.length;i++){
            if(queue.peek()>input[i]){
                queue.poll();
                queue.offer(input[i]);
            }
        }

        for(int i=0;i<k;i++){
            list.add(queue.poll());
        }
        return list;

    }
}