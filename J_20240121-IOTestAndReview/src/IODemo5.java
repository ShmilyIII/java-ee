
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class IODemo5 {
    public static void main(String[] args) throws IOException {
       FileOutputStream fileOutputStream=null;
        try {
            fileOutputStream=new FileOutputStream("test.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            fileOutputStream.close();
        }
    }
}
