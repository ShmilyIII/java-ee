
import java.io.FileOutputStream;
import java.io.IOException;

public class IODemo6 {
    public static void main(String[] args) throws IOException, InterruptedException {
        try ( FileOutputStream fileOutputStream=new FileOutputStream("test.txt")){
            fileOutputStream.wait(99);
        }//这样写会进行自动释放
    }
}
