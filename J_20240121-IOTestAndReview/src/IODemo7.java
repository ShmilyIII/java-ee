
import java.io.FileReader;
import java.io.IOException;

public class IODemo7 {
    public static void main(String[] args) throws IOException {
        FileReader fileReader=new FileReader("test.txt");
        while (true){
            int b=fileReader.read();
            if(b==-1){
                break;
            }
            System.out.println("读取的值为："+(char) b);

        }
    }
}
