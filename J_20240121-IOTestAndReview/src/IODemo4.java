import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class IODemo4 {
    public static void main(String[] args) throws IOException {
        //进行写文件
        OutputStream outputStream=new FileOutputStream("test.txt");
        outputStream.write(97);
        outputStream.write(98);
        outputStream.write(99);
        outputStream.write(100);//进行写会先清空文件原有的内容，再进行写操作，如果不想清空，可以使用追加写操作
        outputStream.close();

    }
}
