import java.io.*;

public class IODemo3 {
    public static void main(String[] args) throws IOException {

        //字节流
        InputStream inputStream=new FileInputStream("test.txt");
        //进行读操作
        while (true){
            byte[] buffer=new byte[1024];
            int len=inputStream.read(buffer);//输出型参数
            System.out.println("len = "+len);//一次读一个数组
            if(len==-1){
                //读取完毕

                break;
            }
            //读取的结果在buffer数组当中
            for (int i = 0; i <len ; i++) {  //每装满一轮数据就进行打印
                System.out.printf("%x\n",buffer[i]);
            }
        }


        inputStream.close();
    }
}


//        while (true){//一般读取文件的条件
//                int b= inputStream.read();//b存放读取的ascll码
//                if(b==-1){
//                //读取完毕
//                break;
//                }
//                //读取打印
//                System.out.println(""+(byte)b);
//                }
