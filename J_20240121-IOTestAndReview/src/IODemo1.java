import java.io.File;

public class IODemo1 {
    public static void main(String[] args) {
        File file=new File("./testdemo1");
        System.out.println("文件是否存在："+file.exists());
        System.out.println("该目录是否是文件："+file.isFile());
        System.out.println("该文件的父路径为："+file.getParent());
        System.out.println("该文件的绝对路径为："+file.getAbsolutePath());
        System.out.println("该文件的文件名为："+file.getName());
        System.out.println("创建该文件"+file.mkdir());
        System.out.println("文件是否存在："+file.exists());
        //删除文件
        file.delete();
        System.out.println("文件是否存在："+file.exists());//判断文件是否存在
    }
}
