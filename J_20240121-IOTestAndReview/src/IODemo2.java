import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class IODemo2 {
    public static void main(String[] args) throws FileNotFoundException {
        //FileInputStream构造方法中字符串可以是绝对路径。也可以是相对路径，这里采用相对路径
        FileInputStream fileInputStream=new FileInputStream("TestDemo1");//文件如果不存在就会抛出FileNotFoundException

    }
}
