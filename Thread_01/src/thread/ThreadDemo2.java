package thread;
class MyRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println("Hello Thread");
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) {
        Runnable runnable=new MyRunnable();
        Thread t=new Thread(runnable);
        t.start();
    }
}
