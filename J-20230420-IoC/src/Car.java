public class Car {
    //车架
    private Frame frame;
    //在构造方法中获得该车架
    public Car(Frame frame){
        this.frame=frame;
    }

    //在车的初始化方法中执行车架的初始化方法
    public void init(){
        frame.init();
    }

    //车架类
    static class Frame{
        //轮胎
        private Tire tire;
        //构造方法中获得该轮胎
        public Frame(Tire tire){
            this.tire=tire;
        }
        //在车架的初始化方法中调用轮胎的初始化方法
        public void init(){
            tire.init();
        }
    }

    //轮胎类
    static class Tire{
        //设置轮胎的属性
        private int size;
        //轮胎的尺寸
        public Tire(int size){
            this.size=size;
        }
        //轮胎的初始化方法
        public void init() {
            System.out.println("轮胎的大小是:"+this.size);
        }
    }

    public static void main(String[] args) {
        //创建轮胎类，制定轮胎大小
        Tire tire=new Tire(20);
        //创建车架类,轮胎作为属性传递进去
        Frame frame=new Frame(tire);
        //创建一个Car类,frame作为属性传递下去
        Car car=new Car(frame);
    }
}