//实现一个定时器

import java.util.concurrent.PriorityBlockingQueue;

/**
 *  实现定时器的核心：
 *  1、定时器需要让任务有一个放置的地方（考虑使用优先级阻塞队列，可以实现线程安全和优先级的情况）
 *  2、定时器需要让任务在指定的时间进行执行（考虑使用一个线程不断进行扫描来进行判断）
 */
//任务
class MyRunnable implements Comparable<MyRunnable>{
    //任务使用Runnable创建
    private Runnable runnable;
    //推迟时间(单位：ms)
    private long time;
    //构造方法
    public MyRunnable(Runnable runnable,long time){
        this.runnable=runnable;
        this.time=time;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public long getTime() {
        return time;
    }
    //执行任务
    public void run(){
        runnable.run();
    }
    //重写

    @Override
    public int compareTo(MyRunnable o) {
        return (int)(this.time-o.time);
    }
}

class MyTimer{
    //使用object来控制阻塞队列执行顺序
    private Object loker=new Object();
    //需要一个优先级阻塞队列
    PriorityBlockingQueue<MyRunnable>queue=new PriorityBlockingQueue<>();
    //一个线程用来扫描队列中的任务(需要在构造方法中实现）
    public MyTimer(){
        Thread t=new Thread(()->{
            while(true){
                try {
                    synchronized (loker){
                        MyRunnable task=queue.take();
                        //记录当前的时间戳
                        long curTime=System.currentTimeMillis();
                        //判断是否到达执行时间
                        //如果没有到达执行时间就把任务再放回阻塞队列当中
                        if(curTime< task.getTime()){
                            queue.put(task);
                            //进行阻塞
                            loker.wait(task.getTime()-curTime);
                            //如果到达执行时间则直接执行任务
                        }else{
                            task.run();
                        }
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
        });
        t.start();

    }
    public void schedule(Runnable runnable,long after){
        MyRunnable runnable1=new MyRunnable(runnable,System.currentTimeMillis()+after);
        queue.put(runnable1);
        synchronized (loker){
            loker.notify();
        }

    }

}


public class MyTask {
    public static void main(String[] args) {
        MyTimer timer1=new MyTimer();
        timer1.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("到达任务1执行时间，执行任务1！");
            }
        },1000);
        MyTimer timer2=new MyTimer();
        timer2.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("到达任务2执行时间，执行任务2！");
            }
        },3000);

    }

}
