import java.util.Timer;
import java.util.TimerTask;

public class ThreadDemo1 {
    public static void main(String[] args) {
        //设置定时器
        Timer timer=new Timer();
        System.out.println("已经设置好定时器！！！");
        //指定定时器的任务和执行时间
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("执行任务1！！");
            }
        },1000);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("执行任务2！！");
            }
        },2000);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("执行任务3！！");
            }
        },3000);
    }
}
