import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class ThreadDemo2 {
    public static void main(String[] args) {
        ExecutorService pool=Executors.newFixedThreadPool(10);
        pool.submit(new Runnable() {
            @Override
            public void run() {
                while(true){
                    System.out.println("这是线程池中的第一个任务！");
                }
            }
        });
    }

}
