package Test03;
    abstract class Employee {
        public abstract double earnings();
    }
    class YearWorker extends Employee {
     //【代码 1】 //重写 earnings()方法
     public double earnings() {
         return 12000;
     }
    }
    class MonthWorker extends Employee {
     //【代码 2】 //重写 earnings()方法
     public double earnings() {
         return 12*2300;
     }
    }
    class WeekWorker extends Employee {
     //【代码 3】 //重写 earnings()方法
     public double earnings() {
         return 52*780;
     }
    }
    class Company {
        Employee[] employee;
        double salaries=0;
        Company(Employee[] employee) {
            this.employee=employee;
        }
        public double salariesPay() {
            salaries=0;
     //【代码 4】 //计算 salaries
            for(int i=0;i<employee.length;i++) {
                salaries=salaries+employee[i].earnings();
            }
            return salaries;

        }
    }
    public class CompanySalary {
        public static void main(String args[]) {
            Employee [] employee=new Employee[30]; //公司有 29 名雇员
            for(int i=0;i<employee.length;i++) { //雇员简单地分成三类
                if(i%3==0)
                    employee[i]=new WeekWorker();
                else if(i%3==1)
                    employee[i]=new MonthWorker();
                else if(i%3==2)
                    employee[i]=new YearWorker();

            }
            Company company=new Company(employee);
            System.out.println("公司薪水总额:"+company.salariesPay()+"元");
        }
    }