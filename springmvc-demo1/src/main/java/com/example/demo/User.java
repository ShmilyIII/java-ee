package com.example.demo;

import lombok.Data;
import org.springframework.stereotype.Controller;

@Controller
@Data
public class User {
    private Integer id;
    private String name;
    private String password;
    private Integer age;

}
