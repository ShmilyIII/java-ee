package com.example.demo.controller;

import com.example.demo.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@GetMapping("/user")
@RequestMapping(method = RequestMethod.GET,value = "/user") //一级路由
@ResponseBody  //返回值就是往页面回写的数据（相当于response.getWriter().write()),没有加就是返回页面
public class UserController {

    @RequestMapping("/get")//二级路由
    public String get(String name,String password){

        return "name-> "+name+" "+"password-> "+password;
    }

//用户提交的时候，只需要提交一个参数，也就是?后面跟着这一个参数(id)
    @RequestMapping("/getUserId")
    public User userById(Integer id,String password){
        User user=new User();
        user.setId(id);
        user.setName("张三");
        user.setPassword(password);
        user.setAge(18);
        return user;
    }

}
