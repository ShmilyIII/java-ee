package SingleMod;

//这个程序简单的实现一下一种常用的设计模式-》单例模式（饿汉模式）
//单例模式主要是有饿汉模式和懒汉模式两种的方式来实现

//饿汉模式
class Singleton{
    //首先创建类对象（直接创建实例)
    private static Singleton instance=new Singleton();

    //由于对象是私有的，我们在类外要使用时就需要实现一个方法来获得该实例

    public static Singleton getInstance(){
        return instance;
    }

    //实现单例的情况就需要构造方法改成无法在类外创建实例的情况

    private Singleton(){}

}
public class SingletonTest {
    public static void main(String[] args) {
        //测试：
        Singleton s1=Singleton.getInstance();
        Singleton s2=Singleton.getInstance();
        System.out.println(s1==s2);//true，是同一个类的实例
        //Singleton s2=new Singleton();//会直接报错！！！无法new新的对象，也就实现了单例



    }
}
