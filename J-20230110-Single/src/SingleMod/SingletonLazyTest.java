package SingleMod;
//上一个实现的是单例模式的饿汉模式实现方式，这个的实现方式就是另一种（懒汉模式）
//正常的懒汉模式写出来是会出现线程安全的，所以我们需要稍加修改（利用了锁的知识)
class SingletonLazy{
    //懒汉模式是先把对象置为null，然后再在需要的时候进行对象实例化
    private volatile static SingletonLazy instance=null;

    /**因为在多线程中使用该设计方式可能会出现线程安全，使用我们要进行加锁，只需要加一次锁即可，因为第一次锁加完之后
     * 实例已经创建出来了，多次的不断加锁只会增加开销，所以在实例为null的时候进行加锁，其他的情况就可以直接返回实例对象即可
     * @return
     */
    //获取实例的时候创建实例
    public static SingletonLazy getInstance(){
        //当instance为null的时候创建实例，否则直接返回该实例即可
        if(instance==null){
            synchronized(SingletonLazy.class){
                if(instance==null){
                    instance =new SingletonLazy();//创建实例对象
                }
            }
        }
        return instance;
    }
    private SingletonLazy(){}
}
//这样单例模式的懒汉实现方式就基本实现了
/*
但是，我们可以知道，在多线程中还会出现内存可见性和编译器指令重排序的问题，所以我们还需要使用volatile这个关键字来解决问题
 */
public class SingletonLazyTest {
    public static void main(String[] args) {

        SingletonLazy sl1=SingletonLazy.getInstance();
        SingletonLazy sl2=SingletonLazy.getInstance();
        System.out.println(sl1==sl2);//true
        //SingletonLazy sl2=new SingletonLazy();//无法创建新的实例，报错！！！说明实现了单例


    }
}
