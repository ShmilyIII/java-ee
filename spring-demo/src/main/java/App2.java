import com.Student;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class App2 {
    public static void main(String[] args) {
        //获取Spring
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        //获取Bean对象
        Student student= (Student) beanFactory.getBean("student");
        //使用bean
        student.sayHi();
    }
}
