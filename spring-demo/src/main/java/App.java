import com.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        //获取Spring
       ApplicationContext content=new ClassPathXmlApplicationContext("spring-config.xml");
        //从Spring中获取Bean对象
       Student student= (Student) content.getBean("student");
       //使用Bean对象
        student.sayHi();

    }
}
