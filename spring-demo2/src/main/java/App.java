import Test.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        //获取spring
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        //获取Bean对象
        Student student=(Student)context.getBean("student");
        //打印类属性
        System.out.println(student.toString());
    }
}
