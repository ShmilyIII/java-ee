import java.util.*;
class ListNode {
   int val;
   ListNode next = null;
  }

public class Solution {
    /**
     *
     * @param head ListNode类 the head
     * @return bool布尔型
     */
    public boolean isPail (ListNode head) {
        // write code here
        if(head==null){
            return true;
        }
        if(head.next==null){
            return true;
        }


        ListNode slow=head;
        ListNode fast=head;

        while(fast!=null&&fast.next!=null){

            fast=fast.next.next;
            slow=slow.next;

        }

        ListNode cur=slow.next;
        while(cur!=null){

            ListNode curNext=cur.next;
            cur.next=slow;
            slow=cur;
            cur=curNext;
        }

        while(head!=slow){
            if(head.val!=slow.val){
                return false;
            }
            if(head.next==slow){
                return true;
            }
            head=head.next;
            slow=slow.next;
        }
        return true;
    }
}