package ThreadDemo;
class Counter{
    volatile private int count=0;
    public void sum(){
        synchronized (this){
            count++;
        }
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}

public class ThreadDemo1 {
    public static void main(String[] args) {
        Counter counter=new Counter();
        Thread t1=new Thread(()->{
            for (int i = 0; i <10_0000 ; i++) {
                synchronized (Counter.class){
                    counter.sum();
                }
            }
        });

        Thread t2=new Thread(()->{
            for (int i = 0; i <10_0000 ; i++) {
                synchronized (Counter.class){
                    counter.sum();
                }
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println(counter.getCount());
    }
}
