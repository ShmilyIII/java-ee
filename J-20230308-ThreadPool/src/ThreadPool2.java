import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
class MyThreadPool{
    //阻塞队列当中放入任务
    private BlockingQueue<Runnable> queue=new LinkedBlockingQueue<>();
    //构造方法中创建出工作的线程
    public MyThreadPool(int n){
        for (int i = 0; i < n; i++) {
            //创建线程
            Thread t=new Thread(()->{
                while(true){
                    //任务为空
                    Runnable runnable= null;
                    try {
                        //将阻塞队列中的任务拿出给runnable
                        runnable = queue.take();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    //执行任务
                    runnable.run();
                }
            });
            t.start();
        }
    }

    //把任务放到阻塞队列中（注册任务)
    public void submit(Runnable runnable){
        try {
            queue.put(runnable);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

public class ThreadPool2 {
    public static void main(String[] args) {
        //创建一个带有10个线程的线程池
        MyThreadPool myThreadPool=new MyThreadPool(10);
        for (int i = 0; i <10 ; i++) {
            int n=i+1;
            myThreadPool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("执行线程池中任务："+n);
                }
            });
        }
    }
}
