import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ThreadPool1 {
    public static void main(String[] args) {
        //在线程池中创建10个线程
        ExecutorService pool= Executors.newFixedThreadPool(10);
        for (int i = 1; i <=10; i++) {
            int n=i;
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("线程池中执行任务"+n);
                }
            });

        }

        ExecutorService pool2=Executors.newCachedThreadPool();

        ScheduledExecutorService pool3=Executors.newScheduledThreadPool(3);

        ExecutorService pool4=Executors.newSingleThreadExecutor();


    }
}
