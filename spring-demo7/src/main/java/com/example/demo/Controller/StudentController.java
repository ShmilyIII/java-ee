package com.example.demo.Controller;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController//==@Controller+@ResponseBody
public class StudentController {
//    @Value("${server.bbbb}")
    @Value("${mystring2}")
    private String name;
    @RequestMapping("/sayhi")
    public String sayHi(){
        return "Hi "+name;
    }

    @PostConstruct
    public void StudentController(){
        System.out.println(name);
    }

}