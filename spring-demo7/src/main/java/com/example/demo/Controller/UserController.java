package com.example.demo.Controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/user")
public class UserController {
    @Value("${studentname}")
    private String name;
    @RequestMapping("/sayHi")
    public String sayHi(){
        return "Hi "+ name;
    }
}
