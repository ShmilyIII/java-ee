import java.util.Arrays;

public class BankerAlgorithm {
    private int processes; // 进程数
    private int resources; // 资源数
    private int[][] max; // 最大需求矩阵
    private int[][] allocation; // 已分配资源矩阵
    private int[][] need; // 剩余所需资源矩阵
    private int[] available; // 可用资源向量
    private boolean[] finished; // 标记进程是否完成

    public BankerAlgorithm(int[][] max, int[][] allocation, int[] available) {
        this.processes = max.length;
        this.resources = available.length;
        this.max = max;
        this.allocation = allocation;
        this.need = calculateNeed(max, allocation);
        this.available = available;
        this.finished = new boolean[processes];
    }

    private int[][] calculateNeed(int[][] max, int[][] allocation) {
        int[][] need = new int[processes][resources];
        for (int i = 0; i < processes; i++) {
            for (int j = 0; j < resources; j++) {
                need[i][j] = max[i][j] - allocation[i][j];
            }
        }
        return need;
    }

    public boolean isSafe() {
        int[] work = Arrays.copyOf(available, resources);
        boolean[] finish = Arrays.copyOf(finished, processes);

        // 初始化标记数组
        for (int i = 0; i < processes; i++) {
            finish[i] = false;
        }

        // 找到一个满足条件的进程
        int count = 0; // 计数已完成的进程数
        while (count < processes) {
            boolean found = false;
            for (int i = 0; i < processes; i++) {
                if (!finish[i] && isSafeProcess(i, work)) {
                    // 找到一个满足条件的进程
                    finish[i] = true;
                    count++;
                    for (int j = 0; j < resources; j++) {
                        work[j] += allocation[i][j]; // 回收该进程的资源
                    }
                    found = true;
                }
            }
            if (!found) {
                // 所有进程都不满足条件，系统不安全
                return false;
            }
        }

        // 所有进程都满足条件，系统安全
        return true;
    }
    private boolean isSafeProcess(int process, int[] work) {
        for (int i = 0; i < resources; i++) {
            if (need[process][i] > work[i]) {
                return false;
            }
        }
        return true;
    }



    public static void main(String[] args) {
        int[][] max = {{0, 1, 2}, {4, 2, 2}, {3, 0, 3}, {3, 1, 1}, {0, 0, 4}};
        int[][] allocation = {{0, 1, 0}, {2, 0, 0}, {3, 0, 3}, {2, 1, 1}, {0, 0, 2}};
        int[] available = {0, 0, 0};

        BankerAlgorithm banker = new BankerAlgorithm(max, allocation, available);
        boolean isSafe = banker.isSafe();

        if (isSafe) {
            System.out.println("系统处于安全状态");
        } else {
            System.out.println("系统不处于安全状态");
        }
    }
}
