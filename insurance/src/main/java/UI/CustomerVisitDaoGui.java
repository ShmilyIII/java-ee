package UI;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.CustomerVisit;
import dao.CustomerVisitDao;

import java.sql.SQLException;
import java.util.List;
//客户拜访：生成本月、下月、今天、明天过生日的客户信息、投保信息资料。
public class CustomerVisitDaoGui extends Application {

    private TableView<CustomerVisit> table;
    private CustomerVisitDao customerVisitDao;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        customerVisitDao = new CustomerVisitDao();

        // 显示当前操作状态的标签
        Label statusLabel = new Label("Ready");

        // Table view for displaying customer visit records
        table = new TableView<>();
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<CustomerVisit, Integer> visitIdColumn = new TableColumn<>("访问 ID");
        visitIdColumn.setCellValueFactory(new PropertyValueFactory<>("visitId"));

        TableColumn<CustomerVisit, Integer> customerIdColumn = new TableColumn<>("客户 ID");
        customerIdColumn.setCellValueFactory(new PropertyValueFactory<>("customerId"));

        TableColumn<CustomerVisit, String> visitDateColumn = new TableColumn<>("访问日期");
        visitDateColumn.setCellValueFactory(new PropertyValueFactory<>("visitDate"));

        TableColumn<CustomerVisit, String> visitTypeColumn = new TableColumn<>("访问类型");
        visitTypeColumn.setCellValueFactory(new PropertyValueFactory<>("visitType"));

        TableColumn<CustomerVisit, String> visitDescriptionColumn = new TableColumn<>("访问详情");
        visitDescriptionColumn.setCellValueFactory(new PropertyValueFactory<>("visitDescription"));

        table.getColumns().addAll(visitIdColumn, customerIdColumn, visitDateColumn, visitTypeColumn, visitDescriptionColumn);

        // 用于查询本月生日客户的按钮
        Button showThisMonthBirthdaysButton = new Button("显示本月生日");
        showThisMonthBirthdaysButton.setOnAction(e -> {
            try {
                List<CustomerVisit> customers = customerVisitDao.selectCustomersByBirthMonth(9);
                table.getItems().setAll(customers);
                statusLabel.setText("展示本月生日的客户");
            } catch (SQLException ex) {
                ex.printStackTrace();
                statusLabel.setText("检索客户访问记录时出错");
            }
        });

        // 用于查询下个月生日的客户的按钮
        Button showNextMonthBirthdaysButton = new Button("显示下个月生日");
        showNextMonthBirthdaysButton.setOnAction(e -> {
            try {
                List<CustomerVisit> customers = customerVisitDao.selectNextMonthBirthdays();
                table.getItems().setAll(customers);
                statusLabel.setText("展示下个月生日的客户");
            } catch (SQLException ex) {
                ex.printStackTrace();
                statusLabel.setText("检索客户访问记录时出错");
            }
        });

        // 用于查询今天生日客户的按钮
        Button showTodayBirthdaysButton = new Button("显示今天生日");
        showTodayBirthdaysButton.setOnAction(e -> {
            try {
                List<CustomerVisit> customers = customerVisitDao.selectTodayBirthdays();
                table.getItems().setAll(customers);
                statusLabel.setText("展示今天生日的客户");
            } catch (SQLException ex) {
                ex.printStackTrace();
                statusLabel.setText("检索客户访问记录时出错");
            }
        });

        // 用于查询明天生日的客户按钮
        Button showTomorrowBirthdaysButton = new Button("显示明天生日");
        showTomorrowBirthdaysButton.setOnAction(e -> {
            try {
                List<CustomerVisit> customers = customerVisitDao.selectTomorrowBirthdays();
                table.getItems().setAll(customers);
                statusLabel.setText("展示明天生日的客户");
            } catch (SQLException ex) {
                ex.printStackTrace();
                statusLabel.setText("检索客户访问记录时出错");
            }
        });

        //用于显示所有客户访问记录的按钮
        Button showAllButton = new Button("显示所有客户");
        showAllButton.setOnAction(e -> {
            try {
                List<CustomerVisit> customers = customerVisitDao.selectAll();
                table.getItems().setAll(customers);
                statusLabel.setText("显示所有客户访问记录");
            } catch (SQLException ex) {
                ex.printStackTrace();
                statusLabel.setText("检索客户访问记录时出错");
            }
        });

        //UI设置
        HBox buttonBox = new HBox(10);
        buttonBox.setPadding(new Insets(10, 10, 10, 10));
        buttonBox.getChildren().addAll(showThisMonthBirthdaysButton, showNextMonthBirthdaysButton, showTodayBirthdaysButton, showTomorrowBirthdaysButton, showAllButton);

        VBox root = new VBox(10);
        root.setPadding(new Insets(10, 10, 10, 10));
        root.getChildren().addAll(statusLabel, table, buttonBox);

        Scene scene = new Scene(root, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.setTitle("客户访问信息");
        primaryStage.show();
    }
}
