package UI;

import dao.PolicyInfoDao;
import model.PolicyInfo;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
//保单管理：保单信息的录入、修改、查询，可按投保人、被保险人姓名、联系电话、保单号码查询保单信息；

public class PolicyInfoGUI extends JFrame {
    private JTextField policyIdTextField;
    private JTextField customerIdTextField;
    private JTextField productIdTextField;
    private JTextField startDateTextField;
    private JTextField endDateTextField;
    private JTextField sumInsuredTextField;
    private JTextField premiumTextField;
    private JTextField amountPaidTextField;
    private JTextField outstandingAmountTextField;

    private JButton addButton;
    private JButton updateButton;
    private JButton searchButton;

    public PolicyInfoGUI() {
        setTitle("保单信息管理系统");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(500, 100, 800, 500);

        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblPolicyId = new JLabel("保单号：");
        lblPolicyId.setBounds(80, 20, 60, 20);
        contentPane.add(lblPolicyId);

        policyIdTextField = new JTextField();
        policyIdTextField.setBounds(150, 20, 200, 20);
        contentPane.add(policyIdTextField);
        policyIdTextField.setColumns(10);

        searchButton = new JButton("查询");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 按保单号查询保单信息
                String policyId = policyIdTextField.getText().trim();
                if (!policyId.isEmpty()) {
                    PolicyInfo policyInfo = PolicyInfoDao.queryPolicyInfoById(Integer.parseInt(policyId));
                    if (policyInfo != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        customerIdTextField.setText(String.valueOf(policyInfo.getCustomerId()));
                        productIdTextField.setText(String.valueOf(policyInfo.getProductId()));
                        startDateTextField.setText(sdf.format(policyInfo.getStartDate()));
                        endDateTextField.setText(sdf.format(policyInfo.getEndDate()));
                        sumInsuredTextField.setText(String.valueOf(policyInfo.getSumInsured()));
                        premiumTextField.setText(String.valueOf(policyInfo.getPremium()));
                        amountPaidTextField.setText(String.valueOf(policyInfo.getAmountPaid()));
                        outstandingAmountTextField.setText(String.valueOf(policyInfo.getOutstandingAmount()));
                    } else {
                        JOptionPane.showMessageDialog(null, "未找到相应的保单信息！");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "请输入有效的保单号！");
                }
            }
        });
        searchButton.setBounds(380, 20, 80, 30);
        contentPane.add(searchButton);

        JLabel lblCustomerId = new JLabel("客户ID：");
        lblCustomerId.setBounds(80, 60, 60, 20);
        contentPane.add(lblCustomerId);

        customerIdTextField = new JTextField();
        customerIdTextField.setColumns(10);
        customerIdTextField.setBounds(150, 60, 200, 20);
        contentPane.add(customerIdTextField);

        JLabel productIdLabel = new JLabel("产品ID：");
        productIdLabel.setBounds(80, 100, 60, 20);
        contentPane.add(productIdLabel);

        productIdTextField = new JTextField();
        productIdTextField.setColumns(10);
        productIdTextField.setBounds(150, 100, 200, 20);
        contentPane.add(productIdTextField);

        JLabel startDateLabel = new JLabel("开始日期：");
        startDateLabel.setBounds(80, 140, 80, 20);
        contentPane.add(startDateLabel);

        startDateTextField = new JTextField();
        startDateTextField.setColumns(10);
        startDateTextField.setBounds(150, 140, 200, 20);
        contentPane.add(startDateTextField);

        JLabel endDateLabel = new JLabel("结束日期：");
        endDateLabel.setBounds(80, 180, 80, 20);
        contentPane.add(endDateLabel);

        endDateTextField = new JTextField();
        endDateTextField.setColumns(10);
        endDateTextField.setBounds(150, 180, 200, 20);
        contentPane.add(endDateTextField);

        JLabel sumInsuredLabel = new JLabel("总保额：");
        sumInsuredLabel.setBounds(80, 220, 60, 20);
        contentPane.add(sumInsuredLabel);

        sumInsuredTextField = new JTextField();
        sumInsuredTextField.setColumns(10);
        sumInsuredTextField.setBounds(150, 220, 200, 20);
        contentPane.add(sumInsuredTextField);

        JLabel premiumLabel = new JLabel("保费：");
        premiumLabel.setBounds(80, 260, 60, 20);
        contentPane.add(premiumLabel);

        premiumTextField = new JTextField();
        premiumTextField.setColumns(10);
        premiumTextField.setBounds(150, 260, 200, 20);
        contentPane.add(premiumTextField);

        JLabel amountPaidLabel = new JLabel("已付金额：");
        amountPaidLabel.setBounds(430, 100, 80, 20);
        contentPane.add(amountPaidLabel);

        amountPaidTextField = new JTextField();
        amountPaidTextField.setColumns(10);
        amountPaidTextField.setBounds(500, 100, 200, 20);
        contentPane.add(amountPaidTextField);

        JLabel outstandingAmountLabel = new JLabel("未付金额：");
        outstandingAmountLabel.setBounds(430, 140, 80, 20);
        contentPane.add(outstandingAmountLabel);

        outstandingAmountTextField = new JTextField();
        outstandingAmountTextField.setColumns(10);
        outstandingAmountTextField.setBounds(500, 140, 200, 20);
        contentPane.add(outstandingAmountTextField);

        addButton = new JButton("添加");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 录入保单信息
                int customerId = Integer.parseInt(customerIdTextField.getText().trim());
                int productId = Integer.parseInt(productIdTextField.getText().trim());
                Date startDate = new Date();
                try {
                    startDate = new SimpleDateFormat("yyyy-mm-dd").parse(startDateTextField.getText().trim());
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
                Date endDate = new Date();
                try {
                    endDate = new SimpleDateFormat("yyyy-mm-dd").parse(endDateTextField.getText().trim());
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
                BigDecimal sumInsured = new BigDecimal(sumInsuredTextField.getText().trim());
                BigDecimal premium = new BigDecimal(premiumTextField.getText().trim());
                BigDecimal amountPaid = new BigDecimal(amountPaidTextField.getText().trim());
                BigDecimal outstandingAmount = new BigDecimal(outstandingAmountTextField.getText().trim());
                PolicyInfo policyInfo = new PolicyInfo(customerId, productId, productId,startDate, endDate, sumInsured, premium, amountPaid, outstandingAmount);

                if (PolicyInfoDao.addPolicyInfo(policyInfo)) {
                    JOptionPane.showMessageDialog(null, "保单信息添加成功！");
                } else {
                    JOptionPane.showMessageDialog(null, "保单信息添加失败！");
                }
            }
        });
        addButton.setBounds(430, 220, 80, 30);
        contentPane.add(addButton);

        updateButton = new JButton("更新");
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 更新保单信息
                int policyId = Integer.parseInt(policyIdTextField.getText().trim());
                int customerId = Integer.parseInt(customerIdTextField.getText().trim());
                int productId = Integer.parseInt(productIdTextField.getText().trim());
                Date startDate = new Date();
                try {
                    startDate = new SimpleDateFormat("yyyy-mm-dd").parse(startDateTextField.getText().trim());
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
                Date endDate = new Date();
                try {
                    endDate = new SimpleDateFormat("yyyy-mm-dd").parse(endDateTextField.getText().trim());
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
                BigDecimal sumInsured = new BigDecimal(sumInsuredTextField.getText().trim());
                BigDecimal premium = new BigDecimal(premiumTextField.getText().trim());
                BigDecimal amountPaid = new BigDecimal(amountPaidTextField.getText().trim());
                BigDecimal outstandingAmount = new BigDecimal(outstandingAmountTextField.getText().trim());

                PolicyInfo policyInfo = new PolicyInfo(policyId, customerId, productId, startDate, endDate, sumInsured, premium, amountPaid, outstandingAmount);

                if (PolicyInfoDao.updatePolicyInfo(policyInfo)) {
                    JOptionPane.showMessageDialog(null, "保单信息更新成功！");
                } else {
                    JOptionPane.showMessageDialog(null, "保单信息更新失败！");
                }
            }
        });
        updateButton.setBounds(540, 220, 80, 30);
        contentPane.add(updateButton);

        setVisible(true);
    }

    public static void main(String[] args) {
        PolicyInfoGUI gui = new PolicyInfoGUI();
    }
}