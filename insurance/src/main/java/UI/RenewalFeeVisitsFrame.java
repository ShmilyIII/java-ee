package UI;
import dao.RenewalFeeVisitsDao;
import model.RenewalVisit;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class RenewalFeeVisitsFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    private JTextField visitIdField;
    private JTextField policyIdField;
    private JTextField visitDateField;
    private JTextField visitTypeField;
    private JTextField visitDescriptionField;
    private JTextField amountCollectedField;
    private JTextField outstandingAmountField;
    private JButton addButton;
    private JButton updateButton;
    private JButton deleteButton;
    private JButton clearButton;
    private JButton searchButton;
    private JTable visitsTable;
    private DefaultTableModel tableModel;

    private RenewalFeeVisitsDao dao;

    public RenewalFeeVisitsFrame() {
        setTitle("客户访问");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        JPanel inputPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 5));
        inputPanel.setPreferredSize(new Dimension(800, 50));
        inputPanel.setAlignmentX(LEFT_ALIGNMENT);

        JLabel visitIdLabel = new JLabel("访问 ID:");
        visitIdField = new JTextField(5);
        inputPanel.add(visitIdLabel);
        inputPanel.add(visitIdField);

        JLabel policyIdLabel = new JLabel("保单 ID:");
        policyIdField = new JTextField(5);
        inputPanel.add(policyIdLabel);
        inputPanel.add(policyIdField);

        JLabel visitDateLabel = new JLabel("访问日期:");
        visitDateField = new JTextField(10);
        inputPanel.add(visitDateLabel);
        inputPanel.add(visitDateField);

        JLabel visitTypeLabel = new JLabel("访问类型:");
        visitTypeField = new JTextField(10);
        inputPanel.add(visitTypeLabel);
        inputPanel.add(visitTypeField);

        JLabel visitDescriptionLabel = new JLabel("访问详情:");
        visitDescriptionField = new JTextField(20);
        inputPanel.add(visitDescriptionLabel);
        inputPanel.add(visitDescriptionField);

        JLabel amountCollectedLabel = new JLabel("应收账户金额:");
        amountCollectedField = new JTextField(10);
        inputPanel.add(amountCollectedLabel);
        inputPanel.add(amountCollectedField);

        JLabel outstandingAmountLabel = new JLabel("超出金额:");
        outstandingAmountField = new JTextField(10);
        inputPanel.add(outstandingAmountLabel);
        inputPanel.add(outstandingAmountField);

        addButton = new JButton("添加");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVisit();
            }
        });

        updateButton = new JButton("更新");
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateVisit();
            }
        });

        deleteButton = new JButton("删除");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteVisit();
            }
        });

        clearButton = new JButton("清除");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearFields();
            }
        });

        searchButton = new JButton("查询");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchVisit();
            }
        });

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(addButton);
        buttonPanel.add(updateButton);
        buttonPanel.add(deleteButton);
        buttonPanel.add(clearButton);
        buttonPanel.add(searchButton);

        mainPanel.add(inputPanel, BorderLayout.NORTH);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        tableModel = new DefaultTableModel(new Object[][]{}, new String[]{"访问 ID", "保单 ID",
                "访问日期", "访问类型", "访问详情", "应收账户金额", "超出金额"});


        visitsTable = new JTable(tableModel);
        visitsTable.setPreferredSize(new Dimension(800, 450));
        JScrollPane scrollPane = new JScrollPane(visitsTable);

        mainPanel.add(scrollPane, BorderLayout.CENTER);

        setContentPane(mainPanel);

        dao = new RenewalFeeVisitsDao();

        showAllVisits();
    }

    private void addVisit() {
        int visitId = Integer.parseInt(visitIdField.getText().trim());
        int policyId = Integer.parseInt(policyIdField.getText().trim());
        Date visitDate = Date.valueOf(visitDateField.getText().trim());
        String visitType = visitTypeField.getText().trim();
        String visitDescription = visitDescriptionField.getText().trim();
        BigDecimal amountCollected = new BigDecimal(amountCollectedField.getText().trim());
        BigDecimal outstandingAmount = new BigDecimal(outstandingAmountField.getText().trim());

        RenewalVisit visit = new RenewalVisit(visitId, policyId, visitDate, visitType,
                visitDescription, amountCollected, outstandingAmount);

        dao.addVisit(visit);

        JOptionPane.showMessageDialog(this, "访问添加成功!");

        showAllVisits();
    }

    private void updateVisit() {
        int visitId = Integer.parseInt(visitIdField.getText().trim());
        int policyId = Integer.parseInt(policyIdField.getText().trim());
        Date visitDate = Date.valueOf(visitDateField.getText().trim());
        String visitType = visitTypeField.getText().trim();
        String visitDescription = visitDescriptionField.getText().trim();
        BigDecimal amountCollected = new BigDecimal(amountCollectedField.getText().trim());
        BigDecimal outstandingAmount = new BigDecimal(outstandingAmountField.getText().trim());

        RenewalVisit visit = new RenewalVisit(visitId, policyId, visitDate, visitType,
                visitDescription, amountCollected, outstandingAmount);

        dao.updateVisit(visit);

        JOptionPane.showMessageDialog(this, "访问更新成功!");

        showAllVisits();
    }

    private void deleteVisit() {
        int visitId = Integer.parseInt(visitIdField.getText().trim());
        dao.deleteVisit(visitId);
        JOptionPane.showMessageDialog(this, "访问删除成功!");
        showAllVisits();
    }

    private void clearFields() {
        visitIdField.setText("");
        policyIdField.setText("");
        visitDateField.setText("");
        visitTypeField.setText("");
        visitDescriptionField.setText("");
        amountCollectedField.setText("");
        outstandingAmountField.setText("");
    }

    private void showAllVisits() {
        List<RenewalVisit> visits = dao.getAllVisits();

        tableModel.setRowCount(0);

        for (RenewalVisit visit : visits) {
            Object[] row = new Object[]{visit.getVisitId(), visit.getPolicyId(), visit.getVisitDate(),
                    visit.getVisitType(), visit.getVisitDescription(), visit.getAmountCollected(),
                    visit.getOutstandingAmount()};
            tableModel.addRow(row);
        }
    }

    private void searchVisit() {
        int visitId = Integer.parseInt(visitIdField.getText().trim());
        RenewalVisit visit = dao.getVisitById(visitId);
        if (visit != null) {
            policyIdField.setText(String.valueOf(visit.getPolicyId()));
            visitDateField.setText(String.valueOf(visit.getVisitDate()));
            visitTypeField.setText(visit.getVisitType());
            visitDescriptionField.setText(visit.getVisitDescription());
            amountCollectedField.setText(String.valueOf(visit.getAmountCollected()));
            outstandingAmountField.setText(String.valueOf(visit.getOutstandingAmount()));
        } else {
            JOptionPane.showMessageDialog(this, "该访问无法找到!");
        }
    }

    public static void main(String[] args) {
        new RenewalFeeVisitsFrame().setVisible(true);
    }
}