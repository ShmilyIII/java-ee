package UI;

import dao.RenewalFeesDao;
import model.PolicyInfo;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//续期收费：根据保单信息可按要求设定应收时间，生成应收保单信息
public class RenewalFeesGUI extends JFrame {

    private JPanel contentPane;
    private JTextField renewalDateField;
    private JButton generateButton;
    private JTable policyInfoTable;

    public RenewalFeesGUI() {
        setTitle("保单续保费用计算器");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 400);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel renewalDateLabel = new JLabel("截止日期:");
        renewalDateLabel.setBounds(20, 20, 80, 25);
        contentPane.add(renewalDateLabel);

        renewalDateField = new JTextField();
        renewalDateField.setBounds(110, 20, 160, 25);
        contentPane.add(renewalDateField);

        generateButton = new JButton("生成应收保费信息");
        generateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String renewalDateString = renewalDateField.getText().trim();

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date renewalDate;
                try {
                    renewalDate = dateFormat.parse(renewalDateString);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "日期格式错误，请重新输入！");
                    return;
                }

                RenewalFeesDao renewalFeesDao = new RenewalFeesDao();
                try {
                    List<PolicyInfo> policyInfos = renewalFeesDao.generateRenewalFees(renewalDate);

                    Object[][] data = new Object[policyInfos.size()][];
                    for (int i = 0; i < policyInfos.size(); i++) {
                        PolicyInfo policyInfo = policyInfos.get(i);
                        data[i] = new Object[]{policyInfo.getPolicyId(), policyInfo.getCustomerId(),
                                policyInfo.getProductId(), policyInfo.getStartDate(), policyInfo.getEndDate(),
                                policyInfo.getSumInsured(), policyInfo.getPremium(), policyInfo.getAmountPaid(),
                                policyInfo.getOutstandingAmount()};
                    }

                    String[] columnNames = {"保单编号", "客户编号", "产品编号", "起始日期", "终止日期", "保额", "保费", "已付金额", "未付金额"};
                    DefaultTableModel tableModel = new DefaultTableModel(data, columnNames);

                    policyInfoTable.setModel(tableModel);

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "数据库错误：" + ex.getMessage());
                }
            }
        });
        generateButton.setBounds(300, 20, 160, 25);
        contentPane.add(generateButton);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(20, 60, 540, 280);
        contentPane.add(scrollPane);

        policyInfoTable = new JTable();
        scrollPane.setViewportView(policyInfoTable);

        setVisible(true);
    }

    public static void main(String[] args) {
        RenewalFeesGUI gui = new RenewalFeesGUI();
    }
}
