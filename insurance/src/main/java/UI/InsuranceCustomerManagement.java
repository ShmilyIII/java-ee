package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import dao.CustomerInfoDao;
import model.CustomerInfo;
//客户信息管理：客户信息的录入、修改和查询。
public class InsuranceCustomerManagement extends JFrame implements ActionListener {
    // 以下是UI组件
    private JPanel panel;
    private JLabel customerIdLabel;
    private JLabel nameLabel;
    private JLabel genderLabel;
    private JLabel dateOfBirthLabel;
    private JLabel phoneNumberLabel;
    private JLabel emailLabel;
    private JTextField customerIdTextField;
    private JTextField nameTextField;
    private JTextField genderTextField;
    private JTextField dateOfBirthTextField;
    private JTextField phoneNumberTextField;
    private JTextField emailTextField;
    private JButton addButton;
    private JButton queryButton;
    private JButton deleteButton;
    private JButton selectAllButton;
    private JTextArea queryResultTextArea;


    // 数据库DAO操作
    private CustomerInfoDao customerInfoDao = new CustomerInfoDao();

    public InsuranceCustomerManagement() {
        // 初始化UI
        initUI();
        // 设置窗口属性
        setTitle("Insurance Information Management System");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * 初始化UI
     */
    private void initUI() {
        panel = new JPanel(new GridLayout(9, 2));
        customerIdLabel = new JLabel("客户ID:");
        nameLabel = new JLabel("客户姓名:");
        genderLabel = new JLabel("客户性别:");
        dateOfBirthLabel = new JLabel("客户出生日期:");
        phoneNumberLabel = new JLabel("客户电话:");
        emailLabel = new JLabel("客户邮箱:");
        customerIdTextField = new JTextField();
        nameTextField = new JTextField();
        genderTextField = new JTextField();
        dateOfBirthTextField = new JTextField();
        phoneNumberTextField = new JTextField();
        emailTextField = new JTextField();
        addButton = new JButton("添加客户");
        queryButton = new JButton("查询客户");
        deleteButton = new JButton("删除客户");
        queryResultTextArea = new JTextArea();
        selectAllButton = new JButton("查看全部客户信息");


        // 添加监听器
        addButton.addActionListener(this);
        queryButton.addActionListener(this);
        deleteButton.addActionListener(this);
        selectAllButton.addActionListener(this);

        // 将组件添加到面板上
        panel.add(customerIdLabel);
        panel.add(customerIdTextField);
        panel.add(nameLabel);
        panel.add(nameTextField);
        panel.add(genderLabel);
        panel.add(genderTextField);
        panel.add(dateOfBirthLabel);
        panel.add(dateOfBirthTextField);
        panel.add(phoneNumberLabel);
        panel.add(phoneNumberTextField);
        panel.add(emailLabel);
        panel.add(emailTextField);
        panel.add(addButton);
        panel.add(queryButton);
        panel.add(deleteButton);
        panel.add(new JScrollPane(queryResultTextArea));
        add(panel);
        panel.add(selectAllButton);

    }

    /**
     * 按钮监听器
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addButton) { // 处理添加客户的按钮事件
            try {
                int customerId = Integer.parseInt(customerIdTextField.getText());
                String name = nameTextField.getText();
                String gender = genderTextField.getText();
                java.sql.Date dateOfBirth = java.sql.Date.valueOf(dateOfBirthTextField.getText());
                String phoneNumber = phoneNumberTextField.getText();
                String email = emailTextField.getText();
                customerInfoDao.add(new CustomerInfo(customerId, name, gender, dateOfBirth, phoneNumber, email));
                JOptionPane.showMessageDialog(this, "添加客户成功！!");
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(this, "Error: " + exception.getMessage());
            }
        } else if (e.getSource() == queryButton) { // 处理查询客户的按钮事件
            try {
                int customerId = Integer.parseInt(customerIdTextField.getText());
                CustomerInfo customerInfo = customerInfoDao.selectById(customerId);
                if (customerInfo != null) {
                    queryResultTextArea.setText(customerInfo.toString());
                } else {
                    queryResultTextArea.setText("没有找到该ID的客户 " + customerId);
                }
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(this, "Error: " + exception.getMessage());
            }
        } else if (e.getSource() == deleteButton) { // 处理删除客户的按钮事件
            try {
                int customerId = Integer.parseInt(customerIdTextField.getText());
                customerInfoDao.delete(customerId);
                JOptionPane.showMessageDialog(this, "删除客户成功！!");
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(this, "Error: " + exception.getMessage());
            }
        } else if (e.getSource() == selectAllButton) { // 处理查询所有客户的按钮事件
        try {
            List<CustomerInfo> customerInfos = customerInfoDao.selectAll();
            if (customerInfos.isEmpty()) {
                queryResultTextArea.setText("没有客户信息");
            } else {
                StringBuilder sb = new StringBuilder();
                for (CustomerInfo customerInfo : customerInfos) {
                    sb.append(customerInfo.toString()).append("\n");
                }
                queryResultTextArea.setText(sb.toString());
            }
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(this, "Error: " + exception.getMessage());
        }
    }

}

    public static void main(String[] args) {
        InsuranceCustomerManagement system = new InsuranceCustomerManagement();
        system.setVisible(true);
    }
}
