package UI;

import dao.PolicyStatisticsDao;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.PolicyInfo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
//保单业绩统计：根据录入的保单信息，自助选择时间段，统计所有保单、有效保单的保费业绩，系统自动计算保单件数。
public class PolicyStatisticsApp  extends Application {

    private PolicyStatisticsDao dao = new PolicyStatisticsDao();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        // 创建开始日期选择器
        Label startDateLabel = new Label("开始日期：");
        DatePicker startDatePicker = new DatePicker(LocalDate.now().minusDays(7));
        grid.add(startDateLabel, 0, 0);
        grid.add(startDatePicker, 1, 0);

        // 创建结束日期选择器
        Label endDateLabel = new Label("结束日期：");
        DatePicker endDatePicker = new DatePicker(LocalDate.now());
        grid.add(endDateLabel, 0, 1);
        grid.add(endDatePicker, 1, 1);

        // 创建查询按钮
        Button queryBtn = new Button("查询");
        grid.add(queryBtn, 2, 1);

        // 创建结果标签
        Label resultLabel = new Label();
        grid.add(resultLabel, 0, 2);

        // 绑定查询按钮事件，查询指定时间段内的所有保单信息，并计算业绩
        queryBtn.setOnAction(event -> {
            LocalDate startDate = startDatePicker.getValue();
            LocalDate endDate = endDatePicker.getValue();

            // 将LocalDate转化为Date类型
            Date start = java.sql.Date.valueOf(startDate);
            Date end = java.sql.Date.valueOf(endDate);

            List<PolicyInfo> policyList = dao.getAllPolicies(start, end);
            PolicyStatisticsDao.PerformanceInfo performanceInfo = dao.calculatePerformance(policyList);

            // 将结果显示在结果标签中
            BigDecimal totalPremium = performanceInfo.getTotalPremium();
            int totalCount = performanceInfo.getTotalCount();
            int validCount = performanceInfo.getValidCount();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateRange = dateFormat.format(start) + " - " + dateFormat.format(end);
            resultLabel.setText(String.format("%s 时间段内，共查询到%d件保单，总保费为%.2f元，其中%d件保单为有效保单。", dateRange, totalCount, totalPremium, validCount));
        });

        Scene scene = new Scene(grid, 1200, 500);

        primaryStage.setTitle("保单信息查询应用");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
