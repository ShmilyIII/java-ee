package model;

import java.math.BigDecimal;

public class ProductInfo {
    private int productId;
    private String name;
    private String type;
    private String description;
    private BigDecimal sumInsured;
    private BigDecimal premium;

    public ProductInfo(int productId, String name, String type, String description,
                       BigDecimal sumInsured, BigDecimal premium) {
        this.productId = productId;
        this.name = name;
        this.type = type;
        this.description = description;
        this.sumInsured = sumInsured;
        this.premium = premium;
    }

    // getter and setter methods here

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(BigDecimal sumInsured) {
        this.sumInsured = sumInsured;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }
}
