package model;

import java.util.Date;

public class CustomerVisit {
    private int visitId;
    private int customerId;
    private Date visitDate;
    private String visitType;
    private String visitDescription;

    public CustomerVisit(int visitId, int customerId, Date visitDate, String visitType,
                         String visitDescription) {
        this.visitId = visitId;
        this.customerId = customerId;
        this.visitDate = visitDate;
        this.visitType = visitType;
        this.visitDescription = visitDescription;
    }

    // getter and setter methods here

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getVisitDescription() {
        return visitDescription;
    }

    public void setVisitDescription(String visitDescription) {
        this.visitDescription = visitDescription;
    }
}
