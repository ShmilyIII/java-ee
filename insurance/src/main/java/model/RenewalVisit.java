package model;

import java.math.BigDecimal;
import java.sql.Date;

public class RenewalVisit {
    private int visitId;
    private int policyId;
    private Date visitDate;
    private String visitType;
    private String visitDescription;
    private BigDecimal amountCollected;
    private BigDecimal outstandingAmount;

    public RenewalVisit(int visitId, int policyId, Date visitDate, String visitType,
                        String visitDescription, BigDecimal amountCollected, BigDecimal outstandingAmount) {
        this.visitId = visitId;
        this.policyId = policyId;
        this.visitDate = visitDate;
        this.visitType = visitType;
        this.visitDescription = visitDescription;
        this.amountCollected = amountCollected;
        this.outstandingAmount = outstandingAmount;
    }

    // getter and setter methods here

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getVisitDescription() {
        return visitDescription;
    }

    public void setVisitDescription(String visitDescription) {
        this.visitDescription = visitDescription;
    }

    public BigDecimal getAmountCollected() {
        return amountCollected;
    }

    public void setAmountCollected(BigDecimal amountCollected) {
        this.amountCollected = amountCollected;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }
}
