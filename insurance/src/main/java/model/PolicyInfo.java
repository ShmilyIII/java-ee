package model;

import java.math.BigDecimal;
import java.util.Date;

public class PolicyInfo {
    private int policyId;
    private int customerId;
    private int productId;
    private Date startDate;
    private Date endDate;
    private BigDecimal sumInsured;
    private BigDecimal premium;
    private BigDecimal amountPaid;
    private BigDecimal outstandingAmount;

    public PolicyInfo(int policyId, int customerId, int productId, Date startDate, Date endDate,
                      BigDecimal sumInsured, BigDecimal premium, BigDecimal amountPaid, BigDecimal outstandingAmount) {
        this.policyId = policyId;
        this.customerId = customerId;
        this.productId = productId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.sumInsured = sumInsured;
        this.premium = premium;
        this.amountPaid = amountPaid;
        this.outstandingAmount = outstandingAmount;
    }

    // getter and setter methods here

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(BigDecimal sumInsured) {
        this.sumInsured = sumInsured;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }
}
