package dao;


import model.CustomerVisit;
import model.DBUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 客户拜访：生成本月、下月、今天、明天过生日的客户信息、投保信息资料
 */
public class CustomerVisitDao {

    // 查询指定月份客户生日的客户信息
    public List<CustomerVisit> selectCustomersByBirthMonth(int month) throws SQLException {
        List<CustomerVisit> customers = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "SELECT * FROM customervisit WHERE MONTH(visitDate)=? AND visitType='birthday'";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, month);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int visitId = resultSet.getInt("visitId");
                int customerId = resultSet.getInt("customerId");
                Date visitDate = resultSet.getDate("visitDate");
                String visitType = resultSet.getString("visitType");
                String visitDescription = resultSet.getString("visitDescription");
                CustomerVisit customerVisit = new CustomerVisit(visitId, customerId, visitDate,
                        visitType, visitDescription);
                customers.add(customerVisit);
            }
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return customers;
    }

    // 查询下个月有生日的客户信息
    public List<CustomerVisit> selectNextMonthBirthdays() throws SQLException {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        List<CustomerVisit> customers = selectCustomersByBirthMonth(month + 1);
        return customers;
    }

    // 查询今天有生日的客户信息
    public List<CustomerVisit> selectTodayBirthdays() throws SQLException {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        List<CustomerVisit> customers = new ArrayList<>();
        List<CustomerVisit> allCustomers = selectCustomersByBirthMonth(month);
        for (CustomerVisit customer : allCustomers) {
            Date visitDate = customer.getVisitDate();
            Calendar visitCalendar = Calendar.getInstance();
            visitCalendar.setTime(visitDate);
            if (visitCalendar.get(Calendar.DAY_OF_MONTH) == day) {
                customers.add(customer);
            }
        }
        return customers;
    }

    // 查询明天有生日的客户信息
    public List<CustomerVisit> selectTomorrowBirthdays() throws SQLException {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        List<CustomerVisit> customers = new ArrayList<>();
        List<CustomerVisit> allCustomers = selectCustomersByBirthMonth(month);
        for (CustomerVisit customer : allCustomers) {
            Date visitDate = customer.getVisitDate();
            Calendar visitCalendar = Calendar.getInstance();
            visitCalendar.setTime(visitDate);
            visitCalendar.add(Calendar.DAY_OF_MONTH, 1);
            if (visitCalendar.get(Calendar.DAY_OF_MONTH) == day) {
                customers.add(customer);
            }
        }
        return customers;
    }

    // 查询所有的客户拜访记录信息
    public List<CustomerVisit> selectAll() throws SQLException {
        List<CustomerVisit> customerVisits = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "SELECT * FROM customerVisit";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int visitId = resultSet.getInt("visitId");
                int customerId = resultSet.getInt("customerId");
                Date visitDate = resultSet.getDate("visitDate");
                String visitType = resultSet.getString("visitType");
                String visitDescription = resultSet.getString("visitDescription");
                CustomerVisit customerVisit = new CustomerVisit(visitId, customerId, visitDate,
                        visitType, visitDescription);
                customerVisits.add(customerVisit);
            }
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return customerVisits;
    }

    // 插入客户拜访记录信息
    public void insert(CustomerVisit customerVisit) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "INSERT INTO customerVisit(customerId,visitDate,visitType,visitDescription) VALUES(?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, customerVisit.getCustomerId());
            statement.setDate(2, new java.sql.Date(customerVisit.getVisitDate().getTime()));
            statement.setString(3, customerVisit.getVisitType());
            statement.setString(4, customerVisit.getVisitDescription());
            statement.executeUpdate();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
    }
}
