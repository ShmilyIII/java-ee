package dao;

import model.RenewalVisit;
import model.DBUtil;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RenewalFeeVisitsDao {
    //修改下次应收费日期
    public void updateNextDueDate(int policyId, Date nextDueDate) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DBUtil.getConnection();
            String sql = "UPDATE PolicyInfo SET endDate = ? WHERE policyId = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setDate(1,nextDueDate );
            stmt.setInt(2, policyId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(conn, stmt, rs);
        }
    }
//通过拜访id查询拜访信息
    public RenewalVisit getVisitById(int visitId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        RenewalVisit visit = null;

        try {
            conn = DBUtil.getConnection();
            String sql = "SELECT * FROM renewalVisit WHERE visitId = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, visitId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int policyId = rs.getInt("policyId");
                Date visitDate = rs.getDate("visitDate");
                String visitType = rs.getString("visitType");
                String visitDescription = rs.getString("visitDescription");
                BigDecimal amountCollected = rs.getBigDecimal("amountCollected");
                BigDecimal outstandingAmount = rs.getBigDecimal("outstandingAmount");
                visit = new RenewalVisit(visitId, policyId, visitDate, visitType, visitDescription,
                        amountCollected, outstandingAmount);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(conn, stmt, rs);
        }
        return visit;
    }
//添加拜访信息
    public void addVisit(RenewalVisit visit) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBUtil.getConnection();
            String sql = "INSERT INTO renewalVisit (policyId, visitDate, visitType, visitDescription, " +
                    "amountCollected, outstandingAmount) VALUES (?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, visit.getPolicyId());
            stmt.setDate(2, visit.getVisitDate());
            stmt.setString(3, visit.getVisitType());
            stmt.setString(4, visit.getVisitDescription());
            stmt.setBigDecimal(5, visit.getAmountCollected());
            stmt.setBigDecimal(6, visit.getOutstandingAmount());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(conn, stmt, rs);
        }
    }

    //更新拜访信息
    public void updateVisit(RenewalVisit visit) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DBUtil.getConnection();
            String sql = "UPDATE renewalVisit SET policyId = ?, visitDate = ?, visitType = ?, " +
                    "visitDescription = ?, amountCollected = ?, outstandingAmount = ? WHERE visitId = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, visit.getPolicyId());
            stmt.setDate(2, visit.getVisitDate());
            stmt.setString(3, visit.getVisitType());
            stmt.setString(4, visit.getVisitDescription());
            stmt.setBigDecimal(5, visit.getAmountCollected());
            stmt.setBigDecimal(6, visit.getOutstandingAmount());
            stmt.setInt(7, visit.getVisitId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(conn, stmt, rs);
        }
    }
//删除拜访信息
    public void deleteVisit(int visitId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DBUtil.getConnection();
            String sql = "DELETE FROM renewalVisit WHERE visitId = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, visitId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(conn, stmt, rs);
        }
    }


    public List<RenewalVisit> getAllVisits() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<RenewalVisit> visits = new ArrayList<>();
        try {
            conn = DBUtil.getConnection();
            String sql = "SELECT * FROM renewalvisits";
            stmt = conn.prepareStatement(sql);
            stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("visitId");
                int policyId = rs.getInt("policyId");
                Date visitDate = rs.getDate("visitDate");
                String visitType = rs.getString("visitType");
                String visitDescription = rs.getString("visitDescription");
                BigDecimal amountCollected = rs.getBigDecimal("amountCollected");
                BigDecimal outstandingAmount = rs.getBigDecimal("outstandingAmount");
                RenewalVisit visit = new RenewalVisit(id, policyId, visitDate, visitType,
                        visitDescription, amountCollected, outstandingAmount);
                visits.add(visit);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return visits;
    }

}