package dao;
import model.CustomerInfo;
import model.DBUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 客户信息管理：客户信息的录入、修改和查询。
 */
public class CustomerInfoDao {

//1、新增一个客户
public void add(CustomerInfo customerInfo){
    Connection connection=null;
    PreparedStatement statement=null;
    //1、和数据库建立连接
    try {
        connection= DBUtil.getConnection();
        //2、构造SQL语句
        String sql="insert into customerInfo values(?, ?, ?, ?, ?,?);";
        statement=connection.prepareStatement(sql);
        //3、执行sql
        //替换？
        statement.setInt(1,customerInfo.getCustomerId());
        statement.setString(2, customerInfo.getName());
        statement.setString(3,customerInfo.getGender());
        statement.setDate(4,customerInfo.getDateOfBirth());
        statement.setString(5,customerInfo.getPhoneNumber());
        statement.setString(6,customerInfo.getEmail());
        statement.executeUpdate();
    } catch (SQLException e) {
        throw new RuntimeException(e);
    }finally {
        DBUtil.close(connection,statement,null);
    }


}

    //2、根据客户id来查询指定客户
    public CustomerInfo selectById(int customerInfoId)  {
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //2、构造SQL语句
            String sql="select * from customerInfo where customerId = ? ;";
            statement=connection.prepareStatement(sql);
            //3、执行sql
            //替换？
            statement.setInt(1,customerInfoId);
            resultSet=statement.executeQuery();
            //4、遍历结果集合，blogId唯一
            if(resultSet.next()){
               CustomerInfo customerInfo=new CustomerInfo();
                customerInfo.setCustomerId(resultSet.getInt("customerId"));
                customerInfo.setName(resultSet.getString("name"));
                customerInfo.setGender(resultSet.getString("gender"));
                customerInfo.setDateOfBirth(resultSet.getDate("dateOfBirth"));
                customerInfo.setPhoneNumber(resultSet.getString("phoneNumber"));
                customerInfo.setEmail(resultSet.getString("email"));
                return customerInfo;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //3、直接查询出数据库中的所有的客户信息
    public List<CustomerInfo> selectAll(){
        List<CustomerInfo> customerInfos=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //2、构造SQL语句
            String sql="select * from customerInfo order by customerId desc;";
            statement=connection.prepareStatement(sql);
            //3、执行sql
            resultSet=statement.executeQuery();
            //4、遍历结果集合
            while (resultSet.next()){
                CustomerInfo customerInfo=new CustomerInfo();
                customerInfo.setCustomerId(resultSet.getInt("customerId"));
                customerInfo.setName(resultSet.getString("name"));
                customerInfo.setGender(resultSet.getString("gender"));
                customerInfo.setDateOfBirth(resultSet.getDate("dateOfBirth"));
                customerInfo.setPhoneNumber(resultSet.getString("phoneNumber"));
                customerInfo.setEmail(resultSet.getString("email"));
                customerInfos.add(customerInfo);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }

        return customerInfos;
    }

    //4、删除指定客户
    public void delete(int customerId){
        Connection connection=null;
        PreparedStatement statement=null;
        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //connection=statement.getConnection();
            //2、构造SQL语句
            String sql="delete from customerInfo where customerId = ?";
            statement=connection.prepareStatement(sql);
            statement.setInt(1,customerId);
            //3、执行SQL语句
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
