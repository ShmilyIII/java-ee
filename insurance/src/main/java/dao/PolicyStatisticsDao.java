
package dao;

import model.DBUtil;
import model.PolicyInfo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PolicyStatisticsDao {

    /**
     * 获取指定时间段内所有的保单信息
     *
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return 保单信息列表
     */
    public List<PolicyInfo> getAllPolicies(Date startDate, Date endDate) {
        List<PolicyInfo> policyList = new ArrayList<>();

        String sql = "SELECT * FROM policyInfo WHERE startDate >= ? AND endDate <= ?";
        try (Connection connection = DBUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setDate(1, new java.sql.Date(startDate.getTime()));
            statement.setDate(2, new java.sql.Date(endDate.getTime()));
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    int policyId = rs.getInt("policyId");
                    int customerId = rs.getInt("customerId");
                    int productId = rs.getInt("productId");
                    Date start = rs.getDate("startDate");
                    Date end = rs.getDate("endDate");
                    BigDecimal sumInsured = rs.getBigDecimal("sumInsured");
                    BigDecimal premium = rs.getBigDecimal("premium");
                    BigDecimal amountPaid = rs.getBigDecimal("amountPaid");
                    BigDecimal outstandingAmount = rs.getBigDecimal("outstandingAmount");
                    PolicyInfo policyInfo = new PolicyInfo(policyId, customerId, productId, start, end,
                            sumInsured, premium, amountPaid, outstandingAmount);
                    policyList.add(policyInfo);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return policyList;
    }

    /**
     * 根据保单信息计算业绩，并返回件数、总保费、有效保单数
     *
     * @param policyList 保单信息列表
     * @return 业绩信息，包括件数、总保费、有效保单数
     */
    public PerformanceInfo calculatePerformance(List<PolicyInfo> policyList) {
        PerformanceInfo performanceInfo = new PerformanceInfo();
        BigDecimal totalPremium = BigDecimal.ZERO;
        int totalCount = policyList.size();
        int validCount = 0;

        for (PolicyInfo policy : policyList) {
            totalPremium = totalPremium.add(policy.getPremium());
            if (policy.getOutstandingAmount().compareTo(BigDecimal.ZERO) > 0) {
                validCount++;
            }
        }
        performanceInfo.setTotalCount(totalCount);
        performanceInfo.setTotalPremium(totalPremium);
        performanceInfo.setValidCount(validCount);
        return performanceInfo;
    }

    public static class PerformanceInfo {
        private int totalCount;  //总数量
        private BigDecimal totalPremium; //总费用
        private int validCount;  //有效数量

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public BigDecimal getTotalPremium() {
            return totalPremium;
        }

        public void setTotalPremium(BigDecimal totalPremium) {
            this.totalPremium = totalPremium;
        }

        public int getValidCount() {
            return validCount;
        }

        public void setValidCount(int validCount) {
            this.validCount = validCount;
        }
    }
}