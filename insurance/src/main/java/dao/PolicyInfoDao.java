package dao;

import model.DBUtil;
import model.PolicyInfo;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 保单管理：保单信息的录入、修改、查询，可按投保人、被保险人姓名、联系电话、保单号码查询保单信息；
 */
public class PolicyInfoDao {
    //录入保单信息
    public static boolean addPolicyInfo(PolicyInfo policyInfo) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DBUtil.getConnection();
            String sql = "INSERT INTO policyInfo(customerId, productId, startDate, endDate, sumInsured, " +
                    "premium, amountPaid, outstandingAmount) VALUES (?,?,?,?,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, policyInfo.getCustomerId());
            statement.setInt(2, policyInfo.getProductId());
            statement.setDate(3, new java.sql.Date(policyInfo.getStartDate().getTime()));
            statement.setDate(4, new java.sql.Date(policyInfo.getEndDate().getTime()));
            statement.setBigDecimal(5, policyInfo.getSumInsured());
            statement.setBigDecimal(6, policyInfo.getPremium());
            statement.setBigDecimal(7, policyInfo.getAmountPaid());
            statement.setBigDecimal(8, policyInfo.getOutstandingAmount());

            int result = statement.executeUpdate();
            return result == 1; // 成功插入一行数据
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    //修改保单信息
    public static boolean updatePolicyInfo(PolicyInfo policyInfo) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE policyInfo SET customerId=?, productId=?, startDate=?, endDate=?, " +
                    "sumInsured=?, premium=?, amountPaid=?, outstandingAmount=? WHERE policyId=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, policyInfo.getCustomerId());
            statement.setInt(2, policyInfo.getProductId());
            statement.setDate(3, new java.sql.Date(policyInfo.getStartDate().getTime()));
            statement.setDate(4, new java.sql.Date(policyInfo.getEndDate().getTime()));
            statement.setBigDecimal(5, policyInfo.getSumInsured());
            statement.setBigDecimal(6, policyInfo.getPremium());
            statement.setBigDecimal(7, policyInfo.getAmountPaid());
            statement.setBigDecimal(8, policyInfo.getOutstandingAmount());
            statement.setInt(9, policyInfo.getPolicyId());

            int result = statement.executeUpdate();
            return result == 1; // 成功修改一行数据
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    //按保单号查询保单信息
    public static PolicyInfo queryPolicyInfoById(int policyId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DBUtil.getConnection();
            String sql = "SELECT * FROM policyInfo WHERE policyId=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, policyId);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int customerId = resultSet.getInt("customerId");
                int productId = resultSet.getInt("productId");
                Date startDate = resultSet.getDate("startDate");
                Date endDate = resultSet.getDate("endDate");
                BigDecimal sumInsured = resultSet.getBigDecimal("sumInsured");
                BigDecimal premium = resultSet.getBigDecimal("premium");
                BigDecimal amountPaid = resultSet.getBigDecimal("amountPaid");
                BigDecimal outstandingAmount = resultSet.getBigDecimal("outstandingAmount");

                return new PolicyInfo(policyId, customerId, productId, startDate, endDate, sumInsured, premium, amountPaid, outstandingAmount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }

        return null; // 未查询到数据
    }

    //按投保人/被保险人姓名或联系电话查询保单信息
    public static List<PolicyInfo> queryPolicyInfoByCustomer(String nameOrPhone) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<PolicyInfo> policyInfos = new ArrayList<>();

        try {
            connection = DBUtil.getConnection();
            String sql = "SELECT policyInfo.*, customerInfo.name, customerInfo.phoneNumber " +
                    "FROM policyInfo JOIN customerInfo ON policyInfo.customerId=customerInfo.customerId " +
                    "WHERE customerInfo.name LIKE ? OR customerInfo.phoneNumber LIKE ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, "%" + nameOrPhone + "%");
            statement.setString(2, "%" + nameOrPhone + "%");

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int policyId = resultSet.getInt("policyId");
                int customerId = resultSet.getInt("customerId");
                int productId = resultSet.getInt("productId");
                Date startDate = resultSet.getDate("startDate");
                Date endDate = resultSet.getDate("endDate");
                BigDecimal sumInsured = resultSet.getBigDecimal("sumInsured");
                BigDecimal premium = resultSet.getBigDecimal("premium");
                BigDecimal amountPaid = resultSet.getBigDecimal("amountPaid");
                BigDecimal outstandingAmount = resultSet.getBigDecimal("outstandingAmount");

                PolicyInfo policyInfo = new PolicyInfo(policyId, customerId, productId, startDate, endDate, sumInsured, premium, amountPaid, outstandingAmount);
//                policyInfo.setCustomerName(resultSet.getString("name"));
//                policyInfo.setCustomerPhoneNumber(resultSet.getString("phone_number"));
                policyInfos.add(policyInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }

        return policyInfos;
    }

}




//    //按投保人/被保险人、客户生日、险种名称、保费应交时间、保单状态、客户地址排序浏览
//    public static List<PolicyInfo> queryPolicyInfoByCondition(String customerName, String birthday, String productName,
//                                                              String premiumDueDate, String policyStatus, String customerAddress,
//                                                              String sortField, boolean isAscending) {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<PolicyInfo> policyInfos = new ArrayList<>();
//
//        try {
//            connection = DBUtil.getConnection();
//            String sql = "SELECT policyInfo.*, customerInfo.name, customerInfo.dateOfBirth, customerInfo.address, " +
//                    "product.name AS product_name, customer.level AS customer_level " +
//                    "FROM policy_info " +
//                    "JOIN customer ON policy_info.customer_id=customer.customer_id " +
//                    "JOIN product ON policy_info.product_id=product.product_id " +
//                    "WHERE customer.name LIKE ? AND customer.birthday LIKE ? AND product.name LIKE ? " +
//                    "AND policy_info.premium<=" + premiumDueDate + " AND policy_info.outstanding_amount>0 " +
//                    "AND customer.address LIKE ? AND policy_info.status=? " +
//                    "ORDER BY " + sortField + (isAscending ? " ASC" : " DESC");
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, "%" + customerName + "%");
//            statement.setString(2, "%" + birthday + "%");
//            statement.setString(3, "%" + productName + "%");
//            statement.setString(4, "%" + customerAddress + "%");
//            statement.setString(5, policyStatus);
//
//            resultSet = statement.executeQuery();
//
//            while (resultSet.next()) {
//                int policyId = resultSet.getInt("policy_id");
//                int customerId = resultSet.getInt("customer_id");
//                int productId = resultSet.getInt("product_id");
//                Date startDate = resultSet.getDate("start_date");
//                Date endDate = resultSet.getDate("end_date");
//                BigDecimal sumInsured = resultSet.getBigDecimal("sum_insured");
//                BigDecimal premium = resultSet.getBigDecimal("premium");
//                BigDecimal amountPaid = resultSet.getBigDecimal("amount_paid");
//                BigDecimal outstandingAmount = resultSet.getBigDecimal("outstanding_amount");
//                String customerLevel = resultSet.getString("customer_level");
//
//                PolicyInfo policyInfo = new PolicyInfo(policyId, customerId, productId, startDate, endDate, sumInsured, premium, amountPaid, outstandingAmount);
//                policyInfo.setCustomerName(resultSet.getString("name"));
//                policyInfo.setCustomerBirthday(resultSet.getDate("birthday"));
//                policyInfo.setCustomerAddress(resultSet.getString("address"));
//                policyInfo.setProductName(resultSet.getString("product_name"));
//                policyInfo.setCustomerLevel(customerLevel == null ? 0 : Integer.parseInt(customerLevel));
//                policyInfo.setStatus(policyStatus);
//                policyInfos.add(policyInfo);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            DBUtil.close(connection, statement, resultSet);
//        }
//
//        return policyInfos;
//    }

