package dao;
import model.PolicyInfo;
import model.DBUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class RenewalFeesDao {

    //根据保单信息生成应收保单信息
    public List<PolicyInfo> generateRenewalFees(Date renewalDate) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DBUtil.getConnection();
            String sql = "SELECT * FROM policyInfo WHERE endDate > ?";
            statement = connection.prepareStatement(sql);
            statement.setDate(1, new java.sql.Date(renewalDate.getTime()));
            resultSet = statement.executeQuery();

            List<PolicyInfo> policyInfos = new ArrayList<>();

            while (resultSet.next()) {
                int policyId = resultSet.getInt("policyId");
                int customerId = resultSet.getInt("customerId");
                int productId = resultSet.getInt("productId");
                Date startDate = resultSet.getDate("startDate");
                Date endDate = resultSet.getDate("endDate");
                BigDecimal sumInsured = resultSet.getBigDecimal("sumInsured");
                BigDecimal premium = resultSet.getBigDecimal("premium");
                BigDecimal amountPaid = resultSet.getBigDecimal("amountPaid");
                BigDecimal outstandingAmount = resultSet.getBigDecimal("outstandingAmount");
                PolicyInfo policyInfo = new PolicyInfo(policyId, customerId, productId, startDate, endDate,
                        sumInsured, premium, amountPaid, outstandingAmount);
                policyInfos.add(policyInfo); //将保单信息添加到列表中
            }
            return policyInfos;
        } catch (SQLException e) {
            throw new SQLException("Failed to generate renewal fees due to database error.", e);
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
    }
}