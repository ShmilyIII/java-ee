public class Car {
    public static void main(String[] args) {
        Car car=new Car();
        car.Init(50);

    }
    //框架
    static class Frame{
        //车架初始化
        public void FrameInit(int size){
            //在车架中new 车轮
            Tire tire=new Tire();
            //调用初始化方法
            tire.TireInit(size);
            System.out.println("车架已完成初始化");

        }

    }
    //车轮
    static class Tire{
        //车轮初始化
        public void TireInit(int size){
            System.out.println("车轮已完成初始化");
            System.out.println("轮胎的尺寸为："+ size);
        }
    }
    //Car初始化
    private void Init(int size){
        //new 下一层对象
        Frame frame=new Frame();
        //调用下一层对象的初始化方法
        frame.FrameInit(size);//这一层方法中会new下一层的对象并调用初始化方法
        System.out.println("车辆已完成初始化>>  车辆制作完成！");
    }
}