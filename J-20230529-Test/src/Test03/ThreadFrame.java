package Test03;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class ThreadFrame extends JFrame implements ActionListener {
    JTextField showWord;
    JButton button;
    JTextField inputText,showScore;
//【代码 2]〃用 WordThread 声明一个 giveWord 线程对象
    WordThread giveWord;
    int score=0;
    ThreadFrame( ) {
        showWord = new JTextField(6);
        showWord.setFont(new Font ("",Font.BOLD,72));
        showWord.setHorizontalAlignment(JTextField.CENTER );
//【代码 3]〃创建 giveWord 线程
        giveWord = new WordThread();
        giveWord.setJTextField(showWord);
        giveWord.setSleepLength(5000);
        button=new JButton("开始");
        inputText = new JTextField(10);
        showScore = new JTextField(5);
        showScore.setEditable(false);
        button.addActionListener(this);
        inputText.addActionListener(this);
        add(button,BorderLayout.NORTH);
        add(showWord,BorderLayout.CENTER);
        JPanel southP=new JPanel();
        southP.add(new JLabel("输入汉字 (回车)"));
        southP.add(inputText);
        southP.add(showScore);
        add(southP,BorderLayout.SOUTH);
        setBounds(100,100,350,180);
        setVisible(true);
        validate();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==button) {
            if(!(giveWord.isAlive())){
//【代码 4】//创建 giveWord
                giveWord = new WordThread();
                giveWord.setJTextField(showWord);
                giveWord.setSleepLength(5000);
            }
            try {
//【代码 5】//giveWord 调用 start()方法
                giveWord.start();
            }
           catch (Exception exe){}
        }
        else if(e.getSource()==inputText) {
            if(inputText.getText().equals(showWord.getText()))
                score++;
            showScore.setText("得分:"+score);
                    inputText.setText(null);
        }
    }
}