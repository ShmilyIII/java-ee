package Test03;
import javax.swing.JTextField;
public class WordThread extends Thread{
    char word;
    int startPosition =19968; //Unicode 表的 19968~ 32320 位上的汉字
    int endPosition = 32320;
    JTextField showWord;
    int sleepLength = 6000;
    public void setJTextField(JTextField t) {
        showWord = t;
        showWord.setEditable(false);
    }
    public void setSleepLength(int n){
        sleepLength = n;
    }
    public void run() {
        int k=startPosition;
        while(true) {
            word=(char)k;
            showWord.setText(""+word);
            try{ //【代码 11 //调用 sleep 方法使得线程中断 sleepLength 毫秒
                Thread.sleep(sleepLength);
            }
            catch(InterruptedException e){}
            k++;
            if(k>=endPosition)
                k=startPosition;
        }
    }
}
