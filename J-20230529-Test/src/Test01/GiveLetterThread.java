package Test01;

public class GiveLetterThread extends Thread {
    Letter letter;
    char startChar = 'a',endChar ='z';
    int sleepLength = 5000;

    public void setLetter(Letter letter) {
        this.letter = letter;
    }

    public void setSleepLength(int n) {
        sleepLength = n;
    }

    public void run() {
        char c = startChar;
        while (true) {
            letter.setChar(c);
            System.out.printf("显示的字符：%c\n ", letter.getChar());
            try { //【代码 3】//调用 sleep 方法使得线程中断 sleepLength 毫秒
                Thread.sleep(sleepLength);
            } catch (InterruptedException e) {
            }
            c = (char) (c + 1);
            if (c > endChar)
                c = startChar;
        }
    }
}









