import com.spring.demo.Bean.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        //System.out.println("Hello Spring");
        //获取Spring上下文
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        //获取指定Bean对象
        User user = (User)context.getBean("user");
        User user1=(User) context.getBean(User.class);
        User user2=context.getBean("user",User.class);
        user.sayHi();
    }
}
