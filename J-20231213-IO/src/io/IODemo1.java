package io;

import java.io.File;
import java.io.IOException;

public class IODemo1 {
    public static void main(String[] args) throws IOException {
        File file=new File("c:/testDemo");//file对象
        System.out.println(file.getName());//获取到文件的名称
        System.out.println(file.getPath());//获取到文件的路径
        System.out.println(file.isFile());//判断文件是否是普通文件，而不是目录
        System.out.println(file.getParent());//获取file对象的父目录路径
        System.out.println(file.getAbsolutePath());//获取绝对路径
        System.out.println(file.getCanonicalPath());//获取修饰过后的绝对路径
        System.out.println(file.exists());//判断当前文件是否存在
        if(!file.exists()){
            //如果文件不存在，就进行文件的创建
            file.mkdir();
        }

    }
}
