package io;

import java.io.File;
import java.io.IOException;

public class IODemo4 {
    public static void main(String[] args) throws InterruptedException {
        File file=new File("IOTestDemo2");
        file.delete();//文件删除
        file.mkdirs();//再次创建
        Thread.sleep(3000);
        File file1=new File("TestIO1");
        file.renameTo(file1);
        try {
            System.out.println(file1.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
