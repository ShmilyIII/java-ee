package io;

import java.io.*;

public class IODemo6 {

    public static void main(String[] args) throws IOException {
        File file=new File("test.txt");
//        System.out.println(file.exists());
//        file.mkdir();
        if (file.createNewFile()){
            System.out.println("文件创建成功");
        }else {
            System.out.println("文件已存在");
        }


        System.out.println(file.exists());

        Writer writer=new FileWriter(file);

        writer.write("你好，这是一次文件io测试");
        writer.close();

        Reader reader=new FileReader(file);
        while (true){
            if(reader.read()==-1){
                break;
            }
            System.out.println((char) reader.read());
        }


    }
}
