package io;

import java.io.*;

public class IODemo5 {
    //字节流 读
    public static void main(String[] args) throws IOException {
        InputStream inputStream=new FileInputStream("C:/testDemo/test.txt");//打开文件
        //读文件使用read
        while(true){
            int buf=inputStream.read();
            //如果读的值是-1就读完了退出
            if(buf==-1){
                break;
            }
            //没有读完输出一下读的内容
            System.out.println(buf);
        }
        inputStream.close();
        //字节流 写
        OutputStream outputStream=new FileOutputStream("C:/testDemo/test.txt");
        outputStream.write(67);
        outputStream.write(68);
        outputStream.write(69);//CDE

        //读文件使用read
        while(true){
            int buf=inputStream.read();
            //如果读的值是-1就读完了退出
            if(buf==-1){
                break;
            }
            //没有读完输出一下读的内容
            System.out.println(buf);
        }


    }
}
