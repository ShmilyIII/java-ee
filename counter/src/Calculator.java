import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator extends JFrame {


    // 输入框，显示当前输入的数字或结果
    private JTextField textField;

    // 数字0-9及小数点按钮
    private JButton button0, button1, button2, button3, button4, button5, button6, button7, button8, button9, buttonDot;

    // 加减乘除按钮
    private JButton buttonPlus, buttonMinus, buttonMultiply, buttonDivide;

    // 操作（等于、清空、删除）按钮
    private JButton buttonEqual, buttonClear, buttonDelete;

    // 平方、立方、乘方、n次方、开平方、开n次方、倒数按钮
    private JButton buttonSquare, buttonCube, buttonPower, buttonNthPow, buttonRoot, buttonNthRoot, buttonReciprocal;

    // 三角函数、反三角函数按钮
    private JButton buttonSin, buttonCos, buttonTan, buttonAsin, buttonAcos, buttonAtan;

    // 当前操作
    private Operation currentOperation;

    // 当前值
    private int currentValue;

    // 枚举操作类型
    private enum Operation {
        ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, POWER, NTHROOT
    }

    public Calculator() {
        initComponents();
        initListeners();
    }

    /**
     * 初始化组件
     */
    private void initComponents() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calculator");

        // 使用GridBagLayout布局方式
        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);

        // 初始化输入框
        textField = new JTextField();
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 6; // 扩展6列
        constraints.fill = GridBagConstraints.BOTH;
        add(textField, constraints);

        // 初始化数字、小数点按钮
        button7 = new JButton("7");
        button8 = new JButton("8");
        button9 = new JButton("9");
        button4 = new JButton("4");
        button5 = new JButton("5");
        button6 = new JButton("6");
        button1 = new JButton("1");
        button2 = new JButton("2");
        button3 = new JButton("3");
        button0 = new JButton("0");
        buttonDot = new JButton(".");
        addButton(constraints, button7, 0, 1);
        addButton(constraints, button8, 1, 1);
        addButton(constraints, button9, 2, 1);
        addButton(constraints, button4, 0, 2);
        addButton(constraints, button5, 1, 2);
        addButton(constraints, button6, 2, 2);
        addButton(constraints, button1, 0, 3);
        addButton(constraints, button2, 1, 3);
        addButton(constraints, button3, 2, 3);
        addButton(constraints, button0, 0, 4);
        addButton(constraints, buttonDot, 1, 4);

        // 初始化平方、立方、乘方、n次方、开平方、开n次方、倒数按钮
        buttonSquare = new JButton("x²");
        buttonCube = new JButton("x³");
        buttonPower = new JButton("x^y");
        buttonNthPow = new JButton("x^n");
        buttonRoot = new JButton("√x");
        buttonNthRoot = new JButton("n√x");
        buttonReciprocal = new JButton("1/x");
        addButton(constraints, buttonSquare, 3, 1);
        addButton(constraints, buttonCube, 4, 1);
        addButton(constraints, buttonPower, 5, 1);
        addButton(constraints, buttonNthPow, 3, 2);
        addButton(constraints, buttonRoot, 4, 2);
        addButton(constraints, buttonNthRoot, 5, 2);
        addButton(constraints, buttonReciprocal, 5, 3);

        // 初始化加减乘除按钮
        buttonPlus = new JButton("+");
        buttonMinus = new JButton("-");
        buttonMultiply = new JButton("×");
        buttonDivide = new JButton("÷");
        addButton(constraints, buttonPlus, 3, 3);
        addButton(constraints, buttonMinus, 4, 3);
        addButton(constraints, buttonMultiply, 3, 4);
        addButton(constraints, buttonDivide, 4, 4);

        // 初始化操作（等于、清空、删除）按钮
        buttonEqual = new JButton("=");
        buttonClear = new JButton("C");
        buttonDelete = new JButton("DEL");
        constraints.gridwidth = 2;
        addButton(constraints, buttonEqual, 5, 4);
        constraints.gridx = 3;
        addButton(constraints, buttonClear, 0, 5);
        constraints.gridx = 4;
        addButton(constraints, buttonDelete, 1, 5);

        // 初始化三角函数、反三角函数按钮
        buttonSin = new JButton("sin");
        buttonCos = new JButton("cos");
        buttonTan = new JButton("tan");
        buttonAsin = new JButton("asin");
        buttonAcos = new JButton("acos");
        buttonAtan = new JButton("atan");
        addButton(constraints, buttonSin, 0, 6);
        addButton(constraints, buttonCos, 1, 6);
        addButton(constraints, buttonTan, 2, 6);
        addButton(constraints, buttonAsin, 3, 6);
        addButton(constraints, buttonAcos, 4, 6);
        addButton(constraints, buttonAtan, 5, 6);

        pack();
    }

    /**
     * 添加按钮
     */
    private void addButton(GridBagConstraints constraints, JButton button, int x, int y) {
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = 1;
        add(button, constraints);
    }

    /**
     * 初始化各个按钮的监听器
     */
    private void initListeners() {
        // 数字0-9、小数点按钮监听器
        ActionListener numberListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textValue = textField.getText();
                textValue += ((JButton)e.getSource()).getText();
                textField.setText(textValue);
            }
        };
        button0.addActionListener(numberListener);
        button1.addActionListener(numberListener);
        button2.addActionListener(numberListener);
        button3.addActionListener(numberListener);
        button4.addActionListener(numberListener);
        button5.addActionListener(numberListener);
        button6.addActionListener(numberListener);
        button7.addActionListener(numberListener);
        button8.addActionListener(numberListener);
        button9.addActionListener(numberListener);
        buttonDot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!textField.getText().contains(".")) {
                    String textValue = textField.getText();
                    textValue += ".";
                    textField.setText(textValue);
                }
            }
        });

        // 平方、立方、乘方、n次方、开平方、开n次方、倒数按钮监听器
        ActionListener operationListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(textField.getText());
                switch (((JButton)e.getSource()).getText()) {
                    case "x²":
                        textField.setText(Double.toString(Math.pow(value, 2)));
                        break;
                    case "x³":
                        textField.setText(Double.toString(Math.pow(value, 3)));
                        break;
                    case "x^y":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.POWER;
                        textField.setText("");
                        break;
                    case "x^n":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.NTHROOT;
                        textField.setText("");
                        break;
                    case "n√x":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.NTHROOT;
                        textField.setText("");
                        break;
                    case "√x":
                        textField.setText(Double.toString(Math.sqrt(value)));
                        break;
                    case "1/x":
                        if (value != 0) {
                            textField.setText(Double.toString(1 / value));
                        }
                        break;
                }
            }
        };
        buttonSquare.addActionListener(operationListener);
        buttonCube.addActionListener(operationListener);
        buttonPower.addActionListener(operationListener);
        buttonNthPow.addActionListener(operationListener);
        buttonRoot.addActionListener(operationListener);
        buttonNthRoot.addActionListener(operationListener);
        buttonReciprocal.addActionListener(operationListener);

        // 加减乘除按钮监听器
        ActionListener operatorListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(textField.getText());
                switch (((JButton)e.getSource()).getText()) {
                    case "+":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.ADDITION;
                        textField.setText("");
                        break;
                    case "-":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.SUBTRACTION;
                        textField.setText("");
                        break;
                    case "×":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.MULTIPLICATION;
                        textField.setText("");
                        break;
                    case "÷":
                        currentValue = (int)Math.round(value);
                        currentOperation = Operation.DIVISION;
                        textField.setText("");
                        break;
                }
            }
        };
        buttonPlus.addActionListener(operatorListener);
        buttonMinus.addActionListener(operatorListener);
        buttonMultiply.addActionListener(operatorListener);
        buttonDivide.addActionListener(operatorListener);

        // 操作（等于、清空、删除）按钮监听器
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (((JButton)e.getSource()).getText()) {
                    case "=":
                        calculateResult();
                        break;
                    case "C":
                        textField.setText("");
                        break;
                    case "DEL":
                        String textValue = textField.getText();
                        if (!textValue.isEmpty()) {
                            textField.setText(textValue.substring(0, textValue.length() - 1));
                        }
                        break;
                }
            }
        };
        buttonEqual.addActionListener(actionListener);
        buttonClear.addActionListener(actionListener);
        buttonDelete.addActionListener(actionListener);

        // 三角函数、反三角函数按钮监听器
        ActionListener functionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(textField.getText());
                switch (((JButton)e.getSource()).getText()) {
                    case "sin":
                        textField.setText(Double.toString(Math.sin(value)));
                        break;
                    case "cos":
                        textField.setText(Double.toString(Math.cos(value)));
                        break;
                    case "tan":
                        textField.setText(Double.toString(Math.tan(value)));
                        break;
                    case "asin":
                        if (value >= -1 && value <= 1) {
                            textField.setText(Double.toString(Math.asin(value)));
                        }
                        break;
                    case "acos":
                        if (value >= -1 && value <= 1) {
                            textField.setText(Double.toString(Math.acos(value)));
                        }
                        break;
                    case "atan":
                        textField.setText(Double.toString(Math.atan(value)));
                        break;
                }
            }
        };
        buttonSin.addActionListener(functionListener);
        buttonCos.addActionListener(functionListener);
        buttonTan.addActionListener(functionListener);
        buttonAsin.addActionListener(functionListener);
        buttonAcos.addActionListener(functionListener);
        buttonAtan.addActionListener(functionListener);
    }

    /**
     * 计算结果并更新文本框
     */
    private void calculateResult() {
        switch (currentOperation) {
            case ADDITION:
                int addValue = Integer.parseInt(textField.getText());
                int result = currentValue + addValue;
                textField.setText(Integer.toString(result));
                break;
            case SUBTRACTION:
                int subValue = Integer.parseInt(textField.getText());
                result = currentValue - subValue;
                textField.setText(Integer.toString(result));
                break;
            case MULTIPLICATION:
                int mulValue = Integer.parseInt(textField.getText());
                result = currentValue * mulValue;
                textField.setText(Integer.toString(result));
                break;
            case DIVISION:
                int divValue = Integer.parseInt(textField.getText());
                if (divValue != 0) {
                    result = currentValue / divValue;
                    textField.setText(Integer.toString(result));
                }
                break;
            case POWER:
                int powValue = Integer.parseInt(textField.getText());
                result = (int)Math.pow(currentValue, powValue);
                textField.setText(Integer.toString(result));
                break;
            case NTHROOT:
                int rootValue = Integer.parseInt(textField.getText());
                if (rootValue >= 2) {
                    result = (int)Math.round(Math.pow(currentValue, 1.0 / rootValue));
                    textField.setText(Integer.toString(result));
                }
                break;
        }
    }
}

enum Operation {
    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, POWER, NTHROOT
}

