package Test01;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomStudentGrade {
    public static void main(String[] args) throws SQLException {
        int count = 20; // 抽取的学生数目
        String dbName = "test"; // 数据库名
        String table = "students"; // 学生信息表名
        String user = "root"; // 数据库用户名
        String password = "123456"; // 数据库密码

        Connection con = null;
        PreparedStatement queryStmt = null;

        try {
            // 在 MySQL Connector/J 中进行 JDBC 配置：设置连接信息与连接数据库
            Class.forName("com.mysql.jdbc.Driver"); // 加载 MySQL Connector/J 驱动到 JVM
            String uri = "jdbc:mysql://localhost:3306/" + dbName + "?useSSL=false&characterEncoding=utf8";
            con = DriverManager.getConnection(uri, user, password); // 根据用户名、密码、URI 建立数据库连接

            // 执行 SQL 查询语句，读取学生信息表中的所有记录
            String queryString = "SELECT * FROM " + table;
            queryStmt = con.prepareStatement(queryString);
            ResultSet rs = queryStmt.executeQuery();

            List<String> records = new ArrayList<>();
            while (rs.next()) {
                String name = rs.getString("name");
                double grade = rs.getDouble("grade");

                records.add(name + ":" + grade);
            }

            // 随机抽取一些学生
            Random rand = new Random();
            int n = Math.min(count, records.size());
            List<Double> grades = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                int idx = rand.nextInt(records.size());
                String[] parts = records.get(idx).split(":");
                double grade = Double.parseDouble(parts[1].trim());
                grades.add(grade);
                records.remove(idx);
            }

            // 计算平均成绩
            double avgGrade = grades.stream().mapToDouble(it -> it).average().orElse(0.0);
            System.out.println("已随机抽取了 " + grades.size() + " 名学生的成绩，平均成绩为：" + avgGrade);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) con.close();
            if (queryStmt != null) queryStmt.close();
        }
    }
}
