package Test01;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class RandomGetRecord1{
    public static void main(String args[]) throws SQLException {
        int wantRecordAmount = 10; //随机抽取的记录数目
        Random random =new Random();
        try {
            Class.forName("com.mysql.jdbc.Driver"); //加载 MySQL Connector/J 驱动程序
        } catch(ClassNotFoundException e) {
            System.out.print(e);
        }

        Connection con;
        Statement sql;
        ResultSet rs;

        String host = "127.0.0.1"; // 主机地址，如果是本地可以不用修改
        String port = "3306"; // 端口号，根据实际配置进行修改
        String database = "shop"; // 数据库名，将此处替换为实际的数据库名
        String user = "root"; // 数据库用户，将此处替换为实际的用户名
        String password = "123456"; // 数据库密码，将此处替换为实际的密码

        String uri = "jdbc:mysql://" + host + ":" + port + "/" + database + "?characterEncoding=utf8&useSSL=false";
        con = DriverManager.getConnection(uri, user, password);
        sql = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        rs = sql.executeQuery("SELECT * FROM goods");//sql 调用.executeQuery 方法查询 goods 表中的全部记录

        List<Double> records = new ArrayList<>();
        while (rs.next()) {
            String number = rs.getString(1);
            String name = rs.getString(2);
            Date date=rs.getDate(3);
            double price=rs.getDouble(4);
            records.add(price);
        }

        int itemAmount = Math.min(wantRecordAmount, records.size());
        System.out.println("随机抽取" + itemAmount + "条记录.");
        double sum = 0, n = itemAmount;

        for (int i = 0; i < itemAmount; i++) {
            int randomIndex = random.nextInt(records.size());
            //String record = records.get(randomIndex);
            double price = records.size(); // 取出价格
            sum += price;
            records.remove(randomIndex);
        }

        con.close();
        double aver = sum/n;
        System.out.println("均价：" + aver + "元");
    }
}
