package Test01;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class RandomGetRecord {
    public static void main(String args[]) throws SQLException {
        int wantRecordAmount = 10; //随机抽取的记录数目
        Random random =new Random();
     try{Class.forName("sun.jdbc.odbc.JdbcOdbcDriver"); //【代码 1]〃加载 JDBC-0f)BC 桥接器
        }
        catch(ClassNotFoundException e) {
            System.out.print(e);
        }
        Connection con;
        Statement sql;
        ResultSet rs;
        try {
            String uri="jdbc:mysql://127.0.0.1:3306/shop?characterEncoding=utf8&userSSL=false";
            String id ="root";
            String password="123456";
            con=DriverManager.getConnection(uri,id,password);
            sql=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            rs =sql.executeQuery("SELECT * FROM goods");//【代码 2】 //sql 调用.executeQuery 方法查询 goods 表中的全部记录
            rs.last(); //将 rs 的游标移到 rs 的最后一行
            int count=rs.getRow();
            Vector<Integer> vector=new Vector<Integer>();
            for(int i=1;i<=count;i++) {
                    vector.add(new Integer(i));
        }
        int itemAmount=Math.min(wantRecordAmount,count);
        System.out.println("随机抽取"+itemAmount+"条记录.");
        double sum =0, n = itemAmount;
        while(itemAmount>0) {
            int randomindex = random.nextInt(vector.size());
            int index=(vector.elementAt(randomindex)).intValue();
//【代码 3] //将 rs 的游标移到 index
            rs.absolute(index);
            String number = rs.getString(1);
            String name = rs.getString(2);
            Date date=rs.getDate(3);
            double price=rs.getDouble(4);
            sum = sum+price;
            itemAmount--;
            vector.removeElementAt(randomindex);
        }
        con.close();
        double aver = sum/n;
        System.out.println("均价："+aver+"元");
    }
catch(SQLException e) {

        System.out.println(""+e);
    }
}
}