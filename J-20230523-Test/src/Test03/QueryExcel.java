package Test03;

import java.sql.*;

public class QueryExcel {
    public static void main(String[] args) {
        Connection con;
        Statement stmt;
        ResultSet rs;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/myexcel?useSSL=false&characterEncoding=utf8",
                    "root",
                    "123456"
            );

            // 获取表的元数据
            DatabaseMetaData meta = con.getMetaData();
            String tableName = "message";
            ResultSet columns = meta.getColumns(null, null, tableName, null);
            int columnCount = 0;
            while (columns.next()) {
                columnCount++;
            }

            // 执行查询
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + tableName);

            // 输出结果
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    System.out.print("\t" + rs.getString(i));
                }
                System.out.println();
            }

            // 关闭连接
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
