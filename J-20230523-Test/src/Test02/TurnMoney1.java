package Test02;




import java.sql.*;
public class TurnMoney1{
    public static void main(String args[]){
        Connection con = null;
        Statement sql;
        ResultSet rs;
        try { Class.forName("com.mysql.jdbc.Driver");
        }
        catch(ClassNotFoundException e){
            System.out.println(""+e);
        }
        try{ double n =100;
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank?useSSL=false&characterEncoding=utf8","root","123456");

//【代码 1】//关闭自动提交模式
            con.setAutoCommit(false);
            sql = con.createStatement();
            rs = sql.executeQuery("SELECT * FROM card1 WHERE name= 'zhangsan' ");
            rs.next();
            double amountOne = rs.getDouble("amount");
            System.out.println("转账操作之前 zhangsan 的钱款数额 ："+amountOne);
            rs = sql.executeQuery("SELECT * FROM card2 WHERE name='xidanShop'");
            rs.next();
            double amountTwo = rs.getDouble("amount");
            System.out.println("转账操作之前 xidanShop 的钱款数额:"+amountTwo);
            amountOne = amountOne-n;
            amountTwo = amountTwo+n;
            sql.executeUpdate(
                    "UPDATE card1 SET amount ="+amountOne+" WHERE name ='zhangsan'");
            sql.executeUpdate(
                    "UPDATE card2 SET amount ="+amountTwo+" WHERE name ='xidanShop'") ;
            con.commit(); //开始事务处理，如果发生异常直接执行 catch 块
//【代码 2】//恢复自动提交模式
            con.setAutoCommit(true);
            rs = sql.executeQuery("SELECT * FROM card1 WHERE name= 'zhangsan' ");
            rs.next( );
            amountOne = rs.getDouble( "amount");
            System.out.println("转账操作之后 zhangsan 的钱款数额:"+amountOne);
            rs=sql.executeQuery("SELECT * FROM card2 WHERE name=  'xidanShop' ");
            rs.next();
            amountTwo = rs.getDouble("amount");
            System.out.println("转账操作之后 xidanShop 的钱款数额:"+amountTwo);
            con.close();
        }
        catch(SQLException e){
            try{ //【代码 3】//撤销事务所做的操作 ，
                con.rollback();
            }
            catch(SQLException e1){
                System.out.println(""+e1);
            }
            System.out.println(""+e);
        }
    }
}




