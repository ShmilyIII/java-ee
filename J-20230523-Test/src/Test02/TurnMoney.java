package Test02;


import java.sql.*;
public class TurnMoney {
    public static void main(String args[]){
        Connection con = null;
        Statement sql;
        ResultSet rs;
        try { Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        }
        catch(ClassNotFoundException e){
            System.out.println(""+e);
        }
        try{ double n =100;
            con = DriverManager.getConnection("jdbc:odbc:tom","root","123456");

//【代码 1】//关闭自动提交模式
            con.setAutoCommit(false);
            sql = con.createStatement();
            rs = sql.executeQuery("SELECT * FROM cardl WHERE number= 'zhangsan' ");
            rs.next();
            double amountOne = rs.getDouble("amount");
            System.out.println("转账操作之前 zhangsan 的钱款数额 ："+amountOne);
            rs = sql.executeQuery("SELECT * FROM card2 WHERE number= 'xidanShop' ");
            rs.next();
            double amountTwo = rs.getDouble("amount");
            System.out.println("转账操作之前 xidanShop 的钱款数额:"+amountTwo);
            amountOne = amountOne-n;
            amountTwo = amountTwo+n;
            sql.executeUpdate(
                    "UPDATE cardl SET amount ="+amountOne+" WHERE number ='zhangsan'");
            sql.executeUpdate(
                    "UPDATE card2 SET amount ="+amountTwo+" WHERE number ='xidanShop'") ;
            con.commit(); //开始事务处理，如果发生异常直接执行 catch 块
//【代码 2】//恢复自动提交模式
            con.setAutoCommit(true);
            rs = sql.executeQuery("SELECT * FROM cardl WHERE number= 'zhangsan' ");
            rs.next( );
            amountOne = rs.getDouble( "amount");
            System.out.println("转账操作之后 zhangsan 的钱款数额:"+amountOne);
            rs=sql.executeQuery("SELECT * FROM card2 WHERE number= 1 'xidanShop' ");
            rs.next();
            amountTwo = rs.getDouble("amount");
            System.out.println("转账操作之后 xidanShop 的钱款数额:"+amountTwo);
            con.close();
        }
        catch(SQLException e){
            try{ //【代码 3】//撤销事务所做的操作 ，
                con.rollback();
            }
            catch(SQLException exp){}
            System.out.println(e.toString());
        }
    }
}