package Test02;
interface Computerweight {
    public double computeWeight();
}
class Television implements Computerweight {
//【代码1】 //重写 computeWeight()方法
    public double computeWeight() {
        return 3.5;
    }
}
class Computer implements Computerweight {
//【代码 2】 //重写 computeWeight()方法
    public double computeWeight() {
        return 2.67;
    }
}
class WashMachine implements Computerweight {
//【代码 3】〃重写 computeWeight()方法
    public double computeWeight( ) {
        return 13.8;
    }
}

class Refrigerrator implements Computerweight {
    //【代码 3】〃重写 computeWeight()方法
    public double computeWeight( ) {
        return 12.8;
    }
}
class Truck {
    public Object getTotal;
    Computerweight [] goods;
    double totalWeights=0;

    Truck(Computerweight[] goods) {
        this.goods=goods;
    }
    public void setGoods(Computerweight[] goods) {
        this.goods=goods;
    }
    public double getTotalWeights() {
        totalWeights=0;
    //【代码 4】 〃计算 totalweights
        for(int i=0;i<goods.length;i++) {
            totalWeights += goods[i].computeWeight();
        }
        return totalWeights;
    }

}

public class CheckCarWeight {
    public static void main(String args[]) {
        Computerweight[] goods=new Computerweight[650]; //650 件货物
        for(int i=0;i<goods.length;i++) { //简单分成三类
            if(i%3==0)
                goods[i]=new Television();
            else if(i%3==1)
                goods[i]=new Computer();
            else if(i%3==2)
                goods[i]=new WashMachine();
        }
        Truck truck=new Truck(goods);
        System.out.printf("\n货车装载的货物重量:%-8.5f kg\n",truck.getTotalWeights());
        goods=new Computerweight[68]; //68 件货物
        for(int i=0;i<goods.length;i++) { //简单分成两类
            if(i%2==0)
                goods[i]=new Television();
            else
                goods[i]=new WashMachine();
        }
        truck.setGoods(goods);
        System.out.printf("货车装载的货物重量：％-8.5f kg\n",truck.getTotalWeights());
    }

}
