package Test03;

 interface WaterStatus {
    String getTemperatureStatus(int temperature);
    String getColorStatus(String color);
    String getShapeStatus(String shape);
}
public class Cup implements WaterStatus {
    private int temperature; // 水温
    private String color; // 颜色
    private String shape; // 形状

    public Cup() {
        this.temperature = 25; // 初始化水温为常温
        this.color = "clear"; // 初始化颜色为无色
        this.shape = "still"; // 初始化形状为静止
    }

    @Override
    public String getTemperatureStatus(int temperature) {
        if (temperature <= 0) {
            return "Solid";
        } else if (temperature > 0 && temperature < 100) {
            return "Liquid";
        } else {
            return "Gas";
        }
    }

    @Override
    public String getColorStatus(String color) {
        if (color.equals("clear")) {
            return "Transparent";
        } else {
            return "Opaque";
        }
    }

    @Override
    public String getShapeStatus(String shape) {
        if (shape.equals("still")) {
            return "Level";
        } else {
            return "Disturbed";
        }
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getColor() {
        return color;
    }

    public String getShape() {
        return shape;
    }
}

