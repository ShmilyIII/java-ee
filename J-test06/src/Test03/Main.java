package Test03;

public class Main {
    public static void main(String[] args) {
        Cup cup = new Cup();
        cup.setTemperature(-10);
        System.out.println("当前温度为" + cup.getTemperature() + "℃, 状态为" + cup.getTemperatureStatus(cup.getTemperature()));
        cup.setColor("milk");
        System.out.println("颜色为" + cup.getColor() + ", 状态为" + cup.getColorStatus(cup.getColor()));
        cup.setShape("disturbed");
        System.out.println("形状为" + cup.getShape() + ", 状态为" + cup.getShapeStatus(cup.getShape()));
    }

}
