

//单线程和多线程的时间比较
//用一个简单的计算，比较不同线程方式在cpu上运行的时间比较
//取一个较大的计算区间，分别对a和b进行自增

public class ThreadDemo {
    public static void main(String[] args) {

        sigThread();
        toThread();


    }
    public static void sigThread(){
        long beg=System.currentTimeMillis();
        long a=0;
        for(long i=0;i<100_0000_0000L;i++){
            a++;
        }
        long b=0;
        for(long i=0;i<100_0000_0000L;i++){
            b++;
        }
        long end=System.currentTimeMillis();
        System.out.println("单线程所用时间为："+(end-beg)+"ms");
    }

    public static void toThread(){

        Thread t1=new Thread(()->{
           long a=0;
           for(long i=0;i<100_0000_0000L;i++){
               a++;
           }
        });

        Thread t2=new Thread(()->{
            long b=0;
            for(long i=0;i<100_0000_0000L;i++){
                b++;
            }
        });
        long beg=System.currentTimeMillis();
        t1.start();
        t2.start();

        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        long end=System.currentTimeMillis();
        System.out.println("并发所用时间为："+(end-beg)+"ms");
    }
}
