import com.demo.spring.beans.UserController3;
import com.demo.spring.beans.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App4 {
    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        UserController3 userController3=context.getBean("userController3",UserController3.class);
        userController3.sayHi();
    }
}
