import com.demo.spring.beans.UserController2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App3 {
    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        UserController2 userController2=context.getBean("userController2", UserController2.class);
        userController2.sayHi();

    }
}
