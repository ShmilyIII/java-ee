package com.demo.spring.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController2 {
    private UserService userService;
    //在构造方法上加@Autowired注解
    @Autowired
    public UserController2(UserService userService){
        this.userService=userService;
    }
    public void sayHi(){
        userService.sayHi();
    }
}
