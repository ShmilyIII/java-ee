package com.demo.spring.beans;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    public void sayHi(){
        System.out.println("Hi UserService");
    }
}
