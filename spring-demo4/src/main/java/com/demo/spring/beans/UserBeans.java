package com.demo.spring.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class UserBeans {
    //方法返回的对象存储在Spring容器
    @Bean(name = {"userInfo","user2"})
    public User user(){
        //创建User对象
        User user=new User();
        //设置属性
        user.setId(1);
        user.setName("张三");
        //返回对象
        return user;
    }

}
