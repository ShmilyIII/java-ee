package com.demo.spring.beans;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Controller
@Service
@Component
@Repository
@Configuration
public class Student {
    public void sayHi(){

        System.out.println("Hi Student");
    }
}
