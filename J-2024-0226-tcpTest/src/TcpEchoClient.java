import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TcpEchoClient {
    //客户端属性socket指定服务端IP+端口号
    private Socket socket;
    public TcpEchoClient(String serverIp,int port) throws IOException {
        socket=new Socket(serverIp,port);
    }
    //启动客户端
    public void start() throws IOException {
        System.out.println("客户端已上线！");
        //1、利用socket获取到与服务端进行数据交互的输入输出流(inputStream和outputStream)
        Scanner input=new Scanner(System.in);
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream=socket.getOutputStream();//都是相对于客户端来进行输入/输出操作的。
        while(true){
            //2、从控制台获取用户输入的信息
            System.out.println("输入你想要发送的信息：");
            String request=input.next();
            //3、将获取到的request通过流的方式发送给服务端
            PrintWriter printWriter=new PrintWriter(outputStream);
            printWriter.println(request);
            printWriter.flush();//刷新缓冲区
            //4、通过Scanner读取服务端响应并进行回显
            //读取服务端响应
            Scanner scanner=new Scanner(inputStream);
            String response=scanner.next();
            //响应内容回显到界面
            System.out.println(response);
        }
    }

    public static void main(String[] args) throws IOException {
        TcpEchoClient tcpEchoClient=new TcpEchoClient("127.0.0.1",9090);
        tcpEchoClient.start();//启动客户端
    }

}
