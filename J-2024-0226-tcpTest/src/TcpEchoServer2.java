import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class TcpEchoServer2 {
    /**
     * 用于TCP客户端和服务端通信的socket对象
     */
    private ServerSocket serverSocket;

    /**
     * 构造方法指定服务端所占用的端口号
     */
    public TcpEchoServer2(int port) throws IOException {
        serverSocket=new ServerSocket(port);
    }
    /**
     * 启动服务端
     */
    public void start() throws IOException {
        System.out.println("启动服务端！");
        while(true){
            //使用clientSocket与客户端交流
            Socket clientSocket=serverSocket.accept();//1、接收客户端发送的socket
            Thread t=new Thread(()->{
                try {
                    processConnection(clientSocket);//2、处理客户端的连接
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            t.start();

        }
    }

    /**
     * 处理客户端发送来的连接
     * 客户端发送来的连接：clientSocket
     */
    public void processConnection(Socket clientSocket) throws IOException {

        //输出客户端ip地址和端口号
        System.out.println("客户端已上线！客户端的IP是："+clientSocket.getInetAddress()+
                "客户端的端口号是："+clientSocket.getPort());
        //2-1、读取clientSocket中的输入输出流对象
        InputStream inputStream=clientSocket.getInputStream();
        OutputStream outputStream=clientSocket.getOutputStream();
        //使用while循环处理多个请求和响应
        while (true){
            //2-2、通过Scanner来获取输入流
            Scanner scanner=new Scanner(inputStream);
            //读取完毕直接返回
            if(!scanner.hasNext()){
                System.out.println("客户端已经下线!客户端的IP是:"
                        +clientSocket.getInetAddress()+
                        ";客户端的端口是:"+clientSocket.getPort());
                //退出循环
                break;
            }
            //2-3、根据请求计算响应
            String request=scanner.next();
            //构造回写内容
            String response="服务器已经响应："+request;
            //2-4、使用PrintWriter发送OutputStream
            PrintWriter printWriter=new PrintWriter(outputStream);
            printWriter.println(response);
            //刷新缓冲区保证能够发送出去
            printWriter.flush();
        }
        //2-5、关闭连接
        clientSocket.close();

    }

    public static void main(String[] args) throws IOException {
        TcpEchoServer tcpEchoServer=new TcpEchoServer(9090);
        tcpEchoServer.start();//服务端启动
    }
}
