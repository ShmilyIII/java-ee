package Thread;

import javax.swing.plaf.synth.SynthRadioButtonMenuItemUI;
import java.util.Scanner;

public class ThreadDemo16 {
    public static int istrue=0;
    public static void main(String[] args) {
        Thread t=new Thread(()->{

                while(istrue==0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                }

            System.out.println("t线程结束！");
        });
        t.start();
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入一个数字:");
        istrue=scanner.nextInt();
        System.out.println("main线程执行完毕");

    }
}
