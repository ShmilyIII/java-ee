package Thread;

class Count{
    private int count=0;
        public  void increase(){
            synchronized (this){
                count++;
            }

        }

    public int getCount() {
        return count;
    }
}
public class ThreadDemo13 {
    public static void main(String[] args) {
        Count count=new Count();
        Thread t1=new Thread(()->{
            for (int i = 0; i <50000; i++) {

                count.increase();
            }
        });

        Thread t2=new Thread(()->{
            for (int i = 0; i <50000; i++) {
                count.increase();
            }
        });
        t1.start();
        t2.start();

        System.out.println(t1.getState());
        System.out.println(t2.getState());
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println(count.getCount());
    }
}
