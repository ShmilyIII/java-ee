package Thread;

class Countsum{
    private static int count=0;
    public  void CountAdd(){
        synchronized (this){
            count++;
        }

    }
    public int getCount(){

        return count;
    }
}
public class ThreadDemo15 {
    public static void main(String[] args) {
        Countsum countsum=new Countsum();
        //第一个线程t1
        Thread t1 =new Thread(()->{

            for (int i = 0; i <50000; i++) {
                countsum.CountAdd();
            }

        });
        //第二个线程t2
        Thread t2=new Thread(()->{
            for(int i=0;i<50000;i++){
                countsum.CountAdd();
            }
        });
        //两个线程操作同一个变量
        t1.start();
        t2.start();
        //让t1，t2两个线程执行完，再执行main线程，这里让main线程阻塞
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //打印最后的结果，看和预期值10_0000是否一致。
        System.out.println(countsum.getCount());
    }


}
