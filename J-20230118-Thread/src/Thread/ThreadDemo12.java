package Thread;

public class ThreadDemo12 {
    public static void main(String[] args)   {
        Thread t=new Thread(()->{

        });
        t.start();
        //使用一个sleep使得t线程先执行完毕
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(t.getState());

        }
}
