package Thread;

class myThread extends Thread{
    //使用一个boolean类型进行判断，让该线程只打印100次
    private boolean flag=true;
    private int count=1;
    @Override
    public void run() {

        while(flag){
            if(count==100){
                flag=false;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("hello Thread");
            count++;
        }

    }
}
public class ThreadDemo2 {
    public static void main(String[] args) throws InterruptedException {
        Thread t=new myThread();
        t.start();
        //主线程打印50次
        for (int i = 0; i <50 ; i++) {
            Thread.sleep(500);
            System.out.println("hello main");
        }


    }
}
