package Thread;

public class ThreadDemo10 {
    public static void main(String[] args) {
        Thread t1=new Thread(()->{

        },"T1");
        Thread t2=new Thread(()->{

        },"T2");

        t1.start();
        t2.start();
        System.out.println(t1.getId());
        System.out.println(t2.getId());
        System.out.println(t1.getName());
        System.out.println(t2.getName());
    }
}
