import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SimpleWebCrawler {

    public static void main(String[] args) {
        String url = "https://movie.douban.com/top250";
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies", "root", "123456")) {
            // 将自动提交模式设置为false
            conn.setAutoCommit(false);

            // 获取豆瓣电影Top250页面
            Document doc = Jsoup.connect(url).get();

            // 解析网页，提取电影信息，并插入到MySQL中
            Elements movies = doc.select("ol.grid_view li");
            for (Element movie : movies) {
                String name = movie.select("div.hd a span.title").text();
                String director = movie.select("div.bd p:first-child").text();
                String actors = movie.select("div.bd p:nth-child(2)").text();
                String rating = movie.select("div.star span.rating_num").text();

                // 使用预编译语句
                String sql = "INSERT INTO movies(name, director, actors, rating) VALUES (?, ?, ?, ?)";
                try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                    pstmt.setString(1, name);
                    pstmt.setString(2, director);
                    pstmt.setString(3, actors);
                    pstmt.setString(4, rating);

                    // 执行插入操作
                    pstmt.executeUpdate();
                }
            }

            // 手动提交事务
            conn.commit();

            System.out.println("抓取完成，共抓取了" + movies.size() + "部电影。");

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}