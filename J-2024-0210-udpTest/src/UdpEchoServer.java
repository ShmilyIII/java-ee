import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;

public class UdpEchoServer {

    /**
     * 与服务端建立联系的datagramSocket对象
     */
    private DatagramSocket datagramSocket;
    /**
     * 服务器当中一定要关联一个端口号
     * 端口号@param port
     * 异常@throws SocketException
     */
    public UdpEchoServer(int port) throws SocketException {
        //构造一个对象

        //port就是服务端主机的进程端口号
        datagramSocket=new DatagramSocket(port);
    }

    public void start() throws IOException {
        System.out.println("服务器启动");
        while (true){
            //用于接收应用层的内容的字节数组一定要长度足够长
            DatagramPacket receivePacket=new DatagramPacket(new byte[4096],4096);
            datagramSocket.receive(receivePacket);
            //截取到实际数据，例如应用层发来的"hello"被存放在byte数组当中的时候，可能仅仅占用了
            //一点点的空间，因此截取的实际长度为"hello"字节数组的长度
            // packet.getLength()
            //获取到数据报的实际长度部分
            String request=new String(receivePacket.getData(),0, receivePacket.getLength());
            //这里模拟一下回显服务器
            String response=process(request);

            //把需要回应的字符串转化为字节数组
            byte[] receiveBytes=response.getBytes();
            //获取到这个字节数组的长度
            int receiveLength=receiveBytes.length;
            //获取到对应客户端的IP和端口号(SocketAddress)
            SocketAddress address =receivePacket.getSocketAddress();
            //构造返回给客户端的socket对象
            DatagramPacket responsePacket=new DatagramPacket(receiveBytes,receiveLength,address);
            //发送客户端
            datagramSocket.send(responsePacket);
            //输出处理结果
            System.out.println("客户端IP:"+
                    receivePacket.getAddress()+
                    ";客户端端口:"
                    +receivePacket.getPort());
        }
    }
    //响应
    public String process(String request){
        return "服务端已经响应:"+request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer2 udpEchoServer2=new UdpEchoServer2(9090);
        udpEchoServer2.start();
    }
}