import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;

public class UdpEchoServer2 {
    //与服务端建立联系的socket
    private DatagramSocket datagramSocket;

    //使用构造方法并传入端口(关联端口)
    public UdpEchoServer2(int port) throws SocketException {
        //创建对象
        datagramSocket=new DatagramSocket(port);//port是服务端进程端口号
    }

    //创建start方法作为服务器启动
    public void start() throws IOException {
        System.out.println("服务器启动！");
        while(true){
            //1、packet存放接收应用层内容
            DatagramPacket receivePacket=new DatagramPacket(new byte[4096],4096);
            //2、使用socket来接收应用层信息并将其存放在packet的byte数组中
            datagramSocket.receive(receivePacket);
            //3、截取byte数组中应用层信息的实际长度的内容，比如hello就截取hello长度的内容
            //也就是获取数据报的实际长度部分
            String request = new String(receivePacket.getData(),0,receivePacket.getLength());

            //4、模拟回显服务器，将提取出来的数据报传给process进行处理并把响应赋值给response
            String response= process(request);

            //5、将响应字符串转化为字节数组
            byte[] responseByte=response.getBytes();
            //6、获取响应数组长度
            int responseByteLength= responseByte.length;
            //7、获取对应的客户端的IP和端口号（SocketAddress）根据packet的信息获取对应的地址
            SocketAddress address = receivePacket.getSocketAddress();
            //8、构造返回给客户端的socket对象
            DatagramPacket responsePacket=new DatagramPacket(responseByte,responseByteLength,address);
            //9、使用构造的socket对象将响应发送给客户端
            datagramSocket.send(responsePacket);
            //输出处理结果作为验证
            System.out.println("客户端IP:"+receivePacket.getAddress()+
                    "客户端端口号:"+receivePacket.getPort());

        }

    }
    //服务器响应
    public String process(String request){
        return "udp服务器已响应"+request;
    }

    //服务端启动
    public static void main(String[] args) throws IOException {
        UdpEchoServer2 udpEchoServer2=new UdpEchoServer2(9090);
        udpEchoServer2.start();
    }

}
