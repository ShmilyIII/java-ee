import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class UdpEchoClient {
    /**
     * 客户端需要有的属性：
     * 1、和服务端建立联系的socket
     * 2、服务端的ip地址
     * 3、服务端的端口号
     */
    private DatagramSocket socket;//和服务端建立联系的socket

    private String serverIp;//服务端的ip地址(目的IP)

    private int serverPort;//服务端的端口号(目的端口）

    //构造方法
    public UdpEchoClient(String serverIp,int serverPort) throws SocketException {
        socket=new DatagramSocket();
        this.serverIp=serverIp;
        this.serverPort=serverPort;
    }

    //启动客户端
    public void start() throws IOException {
        System.out.println("客户端已启动！");
        Scanner input=new Scanner(System.in);
        while(true){
            //1.用户从控制台输入想要发送给服务端的数据
            System.out.println("请输入您想要发送给服务端的数据：");
            String request= input.next();
            //2、构造Udp请求
                //将请求转为请求数组
            byte[] requestBytes=request.getBytes();
                //获取请求数组的长度
            int length= requestBytes.length;
            //3、指定服务端的ip和端口号
            DatagramPacket requestPacket=new DatagramPacket(requestBytes,length,
                    InetAddress.getByName(serverIp),serverPort);
            //4、将请求发送到服务端的receive方法中
            socket.send(requestPacket);
            //5、读取并接收服务端的响应结果
            DatagramPacket responsePacket=new DatagramPacket(new byte[4096],4096);//存放读取的结果
                //接收服务端的响应
            socket.receive(responsePacket);
            //6、构造响应的字符串
            String response=new String(responsePacket.getData(),0,responsePacket.getLength());
            //7、输出响应的字符串
            System.out.println(response);
        }

    }

    public static void main(String[] args) throws IOException {
        UdpEchoClient udpEchoClient=new UdpEchoClient("127.0.0.1",9090);
        udpEchoClient.start();//客户端启动
    }

}
