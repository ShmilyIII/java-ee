package ThreadDemo;

//由于多线程之间是随机调度的，那么我们如何来指定线程的调度呢？
//这里以3个线程分别打印ABC来简单的实现一下指定线程的调度顺序问题
public class ThreadDemo1 {
    public static void main(String[] args) {
        Object object1=new Object();
        Object object2=new Object();

        Thread t1=new Thread(()->{
            System.out.println("A");
            synchronized (object1){
                object1.notify();
            }
        });

        Thread t2=new Thread(()->{
            //加锁并wait使得t2线程等待t1线程中的内容打印后唤醒t2打印内容
            synchronized (object1){
                try {
                    object1.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("B");
            //执行完之后进行通知
            synchronized(object2){
                object2.notify();
            }
        });

        Thread t3=new Thread(()->{
            //加锁并wait使得t3线程等待t2线程打印后再次唤醒t3打印内容
            synchronized(object2){
                try {
                    object2.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("C");
        });
        //同时这里要防止t1线程先于t2线程启动，不然的话t2线程的wait就无法获得唤醒，就进入的阻塞（死等）的状态，导致出现bug
        //所以我们可以在t1启动之前加一段时间的休眠，当然这个时间也不是准确的，只能大概设定t1启动的时间要晚一些
        //在具体的业务当中还是要具体的对待问题，马虎不得

        t2.start();
        t3.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        t1.start();

    }
}
