package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@Slf4j
public class UserController {
//    //通过slf4j获取日志对象
//    private static Logger logger= LoggerFactory.getLogger(UserController.class);
    //输出日志
    @RequestMapping("/hi")
    public void sayhi(){
        log.info("hi i'm info");
        log.debug("hi i'm debug");
        log.error("hi i'm error");
        log.warn("hi i'm warn");
    }
}
