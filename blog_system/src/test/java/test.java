import model.Blog;
import model.DBUtil;

//测试连接
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class test {
    public static void main(String[] args) {
        Blog blog=new Blog();
        try {
            Connection connection=DBUtil.getConnection();
            String sql="insert into blog values(null, ?, ?, ?, ? );";
            PreparedStatement statement=connection.prepareStatement(sql);
            //3、执行sql
            //替换？
            statement.setString(1,"第一");
            statement.setString(2, "天天写博客");
            statement.setTimestamp(3,null);
            statement.setInt(4,2);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
