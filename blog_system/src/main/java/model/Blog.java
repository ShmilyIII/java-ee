package model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Blog {
    private int blogId;
    private String title;
    private String content;
    private Timestamp postTime;
    private int userId;

    public int getBlogId() {
        return blogId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
    public Timestamp getPostTimestamp(){
        return postTime;
    }
//    public Timestamp getPostTime(){
//        return postTime;
//    }

    public String getPostTime() {
        //return postTime;
        //时间转换为格式化数据
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(postTime);
    }


    public int getUserId() {
        return userId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPostTime(Timestamp postTime) {
        this.postTime = postTime;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
