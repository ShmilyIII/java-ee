package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//通过这个类，封装针对博客表的基本操作
public class BlogDao {
    //1、新增一个博客
    public void add(Blog blog){
        Connection connection=null;
        PreparedStatement statement=null;
        //1、和数据库建立连接
        try {
            connection=DBUtil.getConnection();
            //2、构造SQL语句
            String sql="insert into blog values(null, ?, ?, ?, ? );";
            statement=connection.prepareStatement(sql);
            //3、执行sql
            //替换？
            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3,blog.getPostTimestamp());
            statement.setInt(4,blog.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(connection,statement,null);
        }


    }

    //2、根据博客id来查询指定博客（博客详情页）
    public Blog selectById(int blogId)  {
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //2、构造SQL语句
            String sql="select * from blog where blogId = ? ;";
            statement=connection.prepareStatement(sql);
            //3、执行sql
            //替换？
            statement.setInt(1,blogId);
            resultSet=statement.executeQuery();
            //4、遍历结果集合，blogId唯一
            if(resultSet.next()){
                Blog blog=new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //3、直接查询出数据库中的所有的博客列表（博客列表页）
    public List<Blog> selectAll(){
        List<Blog> blogs=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //2、构造SQL语句
            String sql="select * from blog order by postTime desc;";
            statement=connection.prepareStatement(sql);
            //3、执行sql
            resultSet=statement.executeQuery();
            //4、遍历结果集合
            while (resultSet.next()){
                Blog blog=new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //这里的正文是摘要，所以需要截断一下
                String content=resultSet.getString("content");
                if(content.length()>=120){
                    content=content.substring(0,120)+"...";
                }
                blog.setContent(content);
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }

        return blogs;
    }

    //4、删除指定博客
    public void delete(int blogId){
        Connection connection=null;
        PreparedStatement statement=null;
        try {
            //1、和数据库建立连接
            connection=statement.getConnection();
            //2、构造SQL语句
            String sql="delete from blog where blogId = ?";
            statement.setInt(1,blogId);
            //3、执行SQL语句
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
