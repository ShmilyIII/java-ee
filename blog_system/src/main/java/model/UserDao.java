package model;//没有写注册，先不搞add
//没有用户删号，也不搞delete

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    //1、根据userId来查用户信息
    public User selectById(int userId){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;

        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //2、构造sql语句
            String sql="select * from user where userId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);

            //3、执行sql语句
            resultSet=statement.executeQuery();
            //4、遍历结果集合
            if(resultSet.next()){
                User user=new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //2、根据username来查客户信息
    public User selectByUsername(String username){

        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;

        try {
            //1、和数据库建立连接
            connection=DBUtil.getConnection();
            //2、构造sql语句
            String sql="select * from user where username = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);

            //3、执行sql语句
            resultSet=statement.executeQuery();
            //4、遍历结果集合
            if(resultSet.next()){
                User user=new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;

    }
}
