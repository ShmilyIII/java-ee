package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/login")

public class LoginServlet extends HttpServlet {
    private ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码
        req.setCharacterEncoding("utf8");
        //resp.setCharacterEncoding("utf8");
        resp.setContentType("text/html;charset=utf8");
        //读取用户名和密码
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        if(username==null||password==null||"".equals(username)||"".equals(password)){
            //登录失败
            String html="<h3>登录失败！，缺少username或者password字段</h3>";
            resp.getWriter().write(html);
            return;
        }
        //读取数据库对用户名和密码进行匹配
        UserDao userDao=new UserDao();
        User user=userDao.selectByUsername(username);
        if(user==null){
            //用户不存在
            String html="<h3>登录失败！用户名或密码错误</h3>";
            resp.getWriter().write(html);
            return;
        }
        if(!password.equals(user.getPassword())){
            //密码错误
            String html="<h3>登录失败！用户名或密码错误</h3>";
            resp.getWriter().write(html);
            return;
        }
        //验证成功，成功登录，创建会话，保存用户信息
        HttpSession session=req.getSession(true);
        session.setAttribute("user",user);//存储了user对象
        //重定向，跳转博客列表页
        resp.sendRedirect("blog_list.html");
    }

    //用这个强制用户登录
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置响应字符集
        resp.setContentType("application/json;charset=utf8");
        //使用这个方法获取用户登录的状态
        HttpSession session=req.getSession(false);
        if(session==null){
            //未登录,返回一个空的user对象
            User user=new User();
            String respJson=objectMapper.writeValueAsString(user);
            resp.getWriter().write(respJson);
            return;
        }
        //取user对象
        User user =(User) session.getAttribute("user");
        if(user==null){
            user=new User();
            String respJson=objectMapper.writeValueAsString(user);
            resp.getWriter().write(respJson);
            return;
        }
        //成功取出user对象，直接返回
        String respJson=objectMapper.writeValueAsString(user);
        resp.getWriter().write(respJson);

    }
}
