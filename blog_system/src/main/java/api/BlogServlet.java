package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    private ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //尝试获取querystring中的blogId字段
        BlogDao blogDao=new BlogDao();
        String blogId=req.getParameter("blogId");
        if(blogId==null){
            //querystring不存在，说明请求的是博客列表页
            List<Blog> blogs=blogDao.selectAll();
            //把blogs转成符合要求的json字符串
            String respJson=objectMapper.writeValueAsString(blogs);
            resp.setContentType("application/json; charset=utf8");
            resp.getWriter().write(respJson);
        }else{
            //querystring存在，说明是请求的博客详情页，并且是指定id的博客
            Blog blog=blogDao.selectById(Integer.parseInt(blogId));
            //如果blog为空
            if(blog==null){
                System.out.println("当前 blogId = "+blogId + "对应的博客不存在！");
            }
            //把获取到的blog传到响应当中
            String respJson=objectMapper.writeValueAsString(blog);
            resp.setContentType("application/json; charset=utf8");
            resp.getWriter().write(respJson);
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //发布博客
        //读取请求，构造Blog对象，插入数据库
        HttpSession httpSession=req.getSession(false);
        if(httpSession==null){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("当前未登录，无法发布博客！");
            return;
        }
        User user=(User)httpSession.getAttribute("user");
        if(user==null){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("当前未登录，无法发布博客！");
            return;
        }
        //登录之后，获取作者

        //获取标题与正文
        req.setCharacterEncoding("utf8");
        String title=req.getParameter("title");
        String content=req.getParameter("content");
        if(title==null||"".equals(title)||content==null||"".equals(content)){
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("当前未登录，无法发布博客！");
            return;
        }
        //构造Blog对象
        Blog blog=new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        //插入到数据库
        BlogDao blogDao=new BlogDao();
        blogDao.add(blog);
        //跳转到列表页
        resp.sendRedirect("blog_list.html");
    }
}