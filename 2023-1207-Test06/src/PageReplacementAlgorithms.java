import java.util.*;

public class PageReplacementAlgorithms {
    // OPT页面置换算法
    public static int OPT(int[] referenceString, int frames) {
        int faults = 0;

        List<Integer> pageFrames = new ArrayList<>();  // 存储当前的页面帧
        for (int i = 0; i < referenceString.length; i++) {
            int page = referenceString[i];
            if (!pageFrames.contains(page)) {  // 如果页面帧中不包含当前页面，则发生缺页中断
                faults++;
                if (pageFrames.size() >= frames) {  // 如果页面帧已满，则根据OPT算法找到最远的页面进行替换
                    int index = findFarthestPage(referenceString, i + 1, pageFrames);
                    pageFrames.set(index, page);
                } else {  // 如果页面帧未满，则直接将页面加入页面帧
                    pageFrames.add(page);
                }
            }
        }
        return faults;//返回缺页数
    }

    // 查找最远的页面，用于OPT算法
    private static int findFarthestPage(int[] referenceString, int start, List<Integer> pageFrames) {
        int farthestIndex = -1;
        int farthestDistance = -1;

        for (int i = 0; i < pageFrames.size(); i++) {
            int current = pageFrames.get(i);//当前
            int distance = Integer.MAX_VALUE;//最大距离

            for (int j = start; j < referenceString.length; j++) {
                if (referenceString[j] == current) {  // 计算当前页面距离下一次引用的距离
                    distance = j;
                    break;
                }
            }

            if (distance > farthestDistance) {  // 找出距离最远的页面
                farthestDistance = distance;
                farthestIndex = i;
            }
        }

        return farthestIndex;  //返回最远的页面
    }


    // FIFO页面置换算法
    public static int FIFO(int[] referenceString, int frames) {
        int faults = 0;

        Queue<Integer> pageFrames = new LinkedList<>();  // 使用队列存储当前的页面帧
        for (int page : referenceString) {
            if (!pageFrames.contains(page)) {  // 如果页面帧中不包含当前页面，则发生缺页中断
                faults++;
                if (pageFrames.size() >= frames) {  // 如果页面帧已满，则使用先进先出策略，将最早进入的页面替换出去
                    pageFrames.poll();
                }
                pageFrames.add(page);  // 将当前页面加入页面帧
            }
        }
        return faults;//返回缺页数
    }


    // LRU页面置换算法
    public static int LRU(int[] referenceString, int frames) {
        int faults = 0;

        List<Integer> pageFrames = new ArrayList<>();  // 存储当前的页面帧
        for (int page : referenceString) {
            if (!pageFrames.contains(page)) {  // 如果页面帧中不包含当前页面，则发生缺页中断
                faults++;
                if (pageFrames.size() >= frames) {  // 如果页面帧已满，则根据LRU算法找到最久未被使用的页面进行替换
                    int index = findLeastRecentlyUsedPage(referenceString, pageFrames);
                    pageFrames.set(index, page);
                } else {  // 如果页面帧未满，则直接将页面加入页面帧
                    pageFrames.add(page);
                }
            }
        }
        return faults;
    }

    // 查找最久未使用的页面,用于LRU算法
    private static int findLeastRecentlyUsedPage(int[] referenceString, List<Integer> pageFrames) {
        int leastRecentlyUsedIndex = 0;
        int leastRecentlyUsedDistance = -1;  // 这里修改为-1，代表上次使用距离的初始值

        for (int i = 0; i < pageFrames.size(); i++) {
            int current = pageFrames.get(i);
            int distance = -1;  // 这里修改为-1，代表上次使用距离的初始值

            for (int j = 0; j < referenceString.length; j++) {
                if (referenceString[j] == current) {  // 计算当前页面距离最近一次引用的距离
                    distance = j;
                    break;
                }
            }
            if (distance == -1) {  // 如果页面从未被使用过，则直接选择进行替换
                return i;
            }
            if (distance > leastRecentlyUsedDistance) {  // 找出最久未使用的页面
                leastRecentlyUsedDistance = distance;
                leastRecentlyUsedIndex = i;
            }
        }
        return leastRecentlyUsedIndex;
    }


    //NRU页面置换算法
    public static int NRU(int[] referenceString, int frames) {
        int faults = 0;

        List<Integer> pageFrames = new ArrayList<>();  // 存储当前的页面帧
        for (int page : referenceString) {
            if (!pageFrames.contains(page)) {  // 如果页面帧中不包含当前页面，则发生缺页中断
                faults++;
                if (pageFrames.size() >= frames) {  // 如果页面帧已满，则根据LRU算法找到最久未被使用的页面进行替换
                    int index = findNotRecentlyUsedPage(referenceString, pageFrames);
                    pageFrames.set(index, page);
                } else {  // 如果页面帧未满，则直接将页面加入页面帧
                    pageFrames.add(page);
                }
            }
        }
        return faults;
    }
    //查找最近没有使用页面，用于NRU算法
    public static int findNotRecentlyUsedPage(int[] referenceString,List<Integer> pageFrames){
        int notRecentlyUsedIndex=0;
        boolean found=false;
        // 遍历页面帧，寻找最近未使用的页面
        for (int i = 0; i < pageFrames.size(); i++) {
            int current = pageFrames.get(i);
            found=false;

            //在当前页面号引用串中查看是否有最近访问的
            for(int j=0;j<referenceString.length;j++){
                if (referenceString[j]==current){
                    found=true;
                    break;
                }
            }
            // 如果该页面没有在最近的引用中出现过，则选择进行替换
            if(!found){
                return i;
            }

        }
        //都出现过就随机替换吧
        return (int)Math.random()*pageFrames.size();

    }

    //LFU置换算法
    // LFU页面置换算法
    public static int LFU(int[] referenceString, int frames) {
        int faults = 0;

        Map<Integer, Integer> pageFrequency = new HashMap<>(); // 存储页面的访问频率
        Map<Integer, Integer> pageIndex = new HashMap<>(); // 存储页面在页面帧中的位置
        List<Integer> pageFrames = new ArrayList<>();  // 存储当前的页面帧

        for (int i = 0; i < referenceString.length; i++) {
            int page = referenceString[i];
            if (pageFrequency.containsKey(page)) { // 如果页面已经存在于页面帧中，更新其访问频率
                pageFrequency.put(page, pageFrequency.get(page) + 1);
            } else { // 如果页面不在页面帧中，发生缺页中断
                faults++;
                if (pageFrames.size() >= frames) { // 如果页面帧已满，根据LFU算法找到访问频率最低的页面进行替换
                    int index = findLFUPage(pageFrames, pageFrequency);
                    int evictedPage = pageFrames.get(index);
                    pageFrames.set(index, page);
                    pageFrequency.put(page, 1); // 新页面的访问频率为 1
                    pageIndex.put(page, index); // 更新页面在页面帧中的位置信息
                    pageFrequency.remove(evictedPage); // 移除被替换页面的访问频率信息
                    pageIndex.remove(evictedPage); // 移除被替换页面的位置信息
                } else { // 如果页面帧未满，直接将新页面加入页面帧
                    pageFrames.add(page);
                    pageFrequency.put(page, 1); // 新页面的访问频率为 1
                    pageIndex.put(page, pageFrames.size() - 1); // 更新页面在页面帧中的位置信息
                }
            }
        }
        return faults;//返回缺页数
    }

    // 查找访问频率最低的页面，用于LFU算法
    private static int findLFUPage(List<Integer> pageFrames, Map<Integer, Integer> pageFrequency) {
        int minFrequency = Integer.MAX_VALUE;
        int index = -1;

        for (int i = 0; i < pageFrames.size(); i++) {
            int page = pageFrames.get(i);
            int frequency = pageFrequency.get(page);

            if (frequency < minFrequency) {
                minFrequency = frequency;
                index = i;
            }
        }

        return index;  // 返回访问频率最低的页面在页面帧中的位置
    }


    public static void main(String[] args) {
        int[] referenceString = {7, 0, 1, 2, 0, 3, 0, 4, 2, 3, 0, 3, 2, 1, 2, 0, 1, 7, 0, 1};//页面号引用串
        int frames = 3;//3个物理块
        //OPT算法
        System.out.println("OPT缺页数: " + OPT(referenceString, frames));
        System.out.println("OPT缺页率: " + ((double) OPT(referenceString, frames) / referenceString.length)*100+"%");
        //FIFO算法
        System.out.println("FIFO缺页数: " + FIFO(referenceString, frames));
        System.out.println("FIFO缺页率: " + ((double) FIFO(referenceString, frames) / referenceString.length)*100+"%");

        System.out.println("LRU缺页数: " + LRU(referenceString, frames));
        System.out.println("LRU缺页率: " + ((double) LRU(referenceString, frames) / referenceString.length)*100+"%");

        System.out.println("NRU缺页数: " + NRU(referenceString, frames));
        System.out.println("NRU缺页率: " + ((double) NRU(referenceString, frames) / referenceString.length)*100+"%");

        System.out.println("LFU缺页数: " + LFU(referenceString, frames));
        System.out.println("LFU缺页率: " + ((double) LFU(referenceString, frames) / referenceString.length)*100+"%");
    }
}