import java.util.*;

class Sum{
   private long[]arr=new long[1000_0000];
    long ret1=0L;
    long ret2=0L;

   public void creatArr(){
       Random random=new Random();
       for (int i = 0; i <arr.length ; i++) {
           arr[i]=random.nextInt(100);
       }
   }

   public long sumOfEven(){
       for (int i = 0; i < arr.length ; i++) {
           if(i%2==0){
               ret1+=arr[i];
           }
       }
       return ret1;
   }

   public long sumOfOdd(){
       for (int i = 0; i < arr.length ; i++) {
           if(i%2!=0){
               ret2+=arr[i];
           }
       }
       return ret2;
    }

    public long sum(){
       return ret1+ret2;
    }

}
public class ThreadDemo2 {
    public static void main(String[] args) {
        //开始执行
        long start_time=System.currentTimeMillis();
        Sum s=new Sum();
        s.creatArr();
        //偶数下标元素和
        Thread t1=new Thread(()->{
           long res1= s.sumOfEven();
        });
        //奇数下标元素和
        Thread t2=new Thread(()->{
            long rss2=s.sumOfOdd();
        });
        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("总和为："+s.sum());
        long end_time=System.currentTimeMillis();
        System.out.println("程序运行时间为："+(end_time-start_time)+"ms");
    }
}
