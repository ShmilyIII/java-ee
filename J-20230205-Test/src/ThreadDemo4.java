
//单例模式：饿汉模式

class Singleton{
    //第一步实例化对象
   public static Singleton singleton=new Singleton();
    //构造方法为空
    private Singleton(){}
    //获取对象实例
    public static Singleton getInstance(){
        return  singleton;
    }
}


public class ThreadDemo4 {
    public static void main(String[] args) {
        //Singleton singleton1=new Singleton();无法创建对象！！！
        //验证单一性：
        Singleton singleton1=Singleton.getInstance();
        Singleton singleton2=Singleton.getInstance();
        System.out.println(singleton1==singleton2);


    }
}
