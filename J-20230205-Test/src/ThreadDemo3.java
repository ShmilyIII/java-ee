
public class ThreadDemo3 {
    public static void main(String[] args) {

        Object loker1=new Object();
        Object loker2=new Object();
        Thread a=new Thread(()->{
            synchronized (loker2){
                try {
                    loker2.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("a");
            System.out.println("打印正确！");
        });

        Thread b=new Thread(()->{
            synchronized (loker1){
                try {
                    loker1.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("b");
            synchronized (loker2){
                loker2.notify();
            }
        });
        Thread c=new Thread(()->{
            System.out.println("c");
            synchronized (loker1){
                loker1.notify();
            }
        });

        b.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        c.start();
        a.start();
    }
}
