
class Count{
    private int count=0;

    public synchronized void increase(){
        count++;
    }
    public int getCount() {
        return count;
    }
}

public class ThreadDemo1 {
    public static void main(String[] args) {
        Count count=new Count();

        Thread t1=new Thread(()->{
            for (int i = 0; i <10000 ; i++) {
                count.increase();
            }
        });

        Thread t2=new Thread(()->{
            for (int i = 0; i <10000 ; i++) {
                count.increase();
            }
        });

        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(count.getCount());

    }
}
