
class Singleton1{
    public volatile static Singleton1 singleton1=null;//先为空
    //同样构造方法私有化
    private Singleton1(){}
    //懒汉模式是在获取对象实例的方法中进行创建实例的
    public  static Singleton1 getSingleton1() {
        if(singleton1==null){
            synchronized (Singleton1.class){
                if(singleton1==null){
                    singleton1=new Singleton1();
                }
            }
        }
        return singleton1;
    }
}

public class ThreadDemo5 {
    public static void main(String[] args) {
        //Singleton1 singleton1=new Singleton1();无法创建实例
        Singleton1 s1=Singleton1.getSingleton1();
        Singleton1 s2=Singleton1.getSingleton1();
        System.out.println(s1==s2);

    }
}
