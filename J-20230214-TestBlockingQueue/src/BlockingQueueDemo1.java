import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueDemo1 {
    public static void main(String[] args) throws InterruptedException {
        //阻塞队列
        BlockingQueue<String> queue=new LinkedBlockingQueue<>();
        //入队列
        queue.put("abc");
        //出队列，如果没有就进行阻塞
        String elem=queue.take();
        System.out.println(elem);
        queue.take();
    }
}
