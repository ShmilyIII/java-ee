
class MyBlockingQueue1{

    //利用数组实现
    private int[] arr=new int[1000];//设定数组长度为1000

    private int size=0;//记录数组的内容长度
    //利用end和begin两个指针使得数组变为循环数组（逻辑上的循环）
    private int end=0;
    private int begin=0;

    //实现put方法
    //阻塞考虑使用wait和notify进行唤醒（sleep不太靠谱）
    public void put(int value) throws InterruptedException {
        //判断是否满了（这里要用循环判断，因为在多线程当中，线程被唤醒的时候不一定不满）
        //加锁保证原子性
        synchronized (this){
            while(size>= arr.length){
                this.wait();
            }
            //不满之后放入元素
            arr[end]=value;
            //调整长度
            end++;
            size++;
            //如果放满了则将end变为0
            if(end>= arr.length){
                end=0;
            }
            //进行唤醒
            this.notify();
        }
    }
    //实现take方法
    public int take() throws InterruptedException {
        synchronized (this){
            //判断是否为空
            while (size==0){
                this.wait();
            }
            //不空之后开始取出元素
            int ret=arr[begin];
            begin++;
            if(begin>= arr.length){
                begin=0;
            }
            size--;
            this.notify();
            return ret;
        }

    }
    //长度
    public synchronized int Size(){
        return size;
    }

}


public class BlockingQueueDemo4 {
    public static void main(String[] args) throws InterruptedException {
        MyBlockingQueue1 queue1=new MyBlockingQueue1();
        //生产者
        Thread producer =new Thread(()->{
            int count=0;
            while(true){
                try {
                    queue1.put(count);
                    System.out.println("生产者生产元素："+count);
                    count++;
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //消费者
        Thread customer =new Thread(()->{
            while(true){
                try {
                    int ret=queue1.take();
                    System.out.println("消费者消费元素："+ret);
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        producer.start();
        customer.start();
    }
}
