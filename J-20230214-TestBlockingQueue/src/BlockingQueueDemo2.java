import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueDemo2 {
    public static void main(String[] args) {
        //创建阻塞队列
        BlockingQueue<Integer>queue=new LinkedBlockingQueue<>();
        //使用两个线程：一个线程充当生产者，一个线程充当消费者
        //生产者
        Thread t1=new Thread(()->{
            int count=0;
            while(true){
                try {
                    queue.put(count);
                    System.out.println("生产者生产："+count);
                    count++;
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        Thread t2=new Thread(()->{

            while(true){
                try {
                    int ret=queue.take();
                    System.out.println("消费者消费："+ret);
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t1.start();
        t2.start();
    }
}
