import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
public class Code27_BQCP {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue=new LinkedBlockingQueue<>();
        Thread customer=new Thread(()->{
            while(true){
                try {
                    Thread.sleep(500);
                    int thing= (blockingQueue.take());
                    System.out.println("消费元素："+thing);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        Thread producer=new Thread(()->{
            int count=0;
            while(true){
                try {
                    blockingQueue.put(count);
                    count++;
                    System.out.println("生产元素："+count);
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        //这里顺序不用手动控制，因为是在阻塞队列中，随便的顺序都可以
        customer.start();
        producer.start();
    }
}
