import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/getParameter")
public class GetparameterServlet extends HelloServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String aa=req.getParameter("aa");
        String bb=req.getParameter("bb");
        resp.setContentType("text/html");
        resp.getWriter().write("aa = "+aa+" bb = "+bb);

    }
}
