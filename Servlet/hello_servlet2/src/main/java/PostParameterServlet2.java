import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
//将json字符串转换为Student对象
class Student{
    public int studentId;
    public int classId;
}

@WebServlet("/postParameter2")
public class PostParameterServlet2 extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        //通过这个处理json格式的数据
//        //直接把req中body读取处理
//        //getInputStream
//        //流对象中读多少个字节取决于Content-Length
//        int length=req.getContentLength();
//        byte[]buffer=new byte[length];
//        //读取body的内容
//        InputStream inpputStream=req.getInputStream();
//        inpputStream.read(buffer);
//        //把这个字节数组给构造成String，打印出来
//        String body=new String(buffer,0,length);
//        System.out.println("body = "+body);
        //使用jackson的核心对象
        ObjectMapper objectMapper=new ObjectMapper();
        //使用这个方法将json字符串转换为java对象
       Student s= objectMapper.readValue(req.getInputStream(),Student.class);
       //打印一下这个对象
        System.out.println(s.studentId+", "+s.classId);
        //resp.getWriter().write(body);
    }
}
