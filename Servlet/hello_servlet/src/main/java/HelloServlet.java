import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/hello")//注解，针对类和方法进行解释说明，赋予类和方法额外的功能/含义
//这的作用是把当前的类和http请求的路径关联起来
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
        //在服务器控制台打印
        System.out.println("Hello world");
        //将helloworld返回到客户端
        //getWriter()会得到一个writer对象
        resp.getWriter().write("Hello world");//往resp的body部分写入，等resp对象构造好了，Tomcat统一转化为http响应的格式，再写socket


    }
}
