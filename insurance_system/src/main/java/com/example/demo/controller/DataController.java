package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/test")
public class DataController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/data")
    public List<Map<String, Object>> getData() {
//       // List<Map<String, Object>> result = jdbcTemplate.queryForList("SELECT * FROM customer_info ");
        return jdbcTemplate.queryForList("SELECT * FROM customer_info ");
    }

}
