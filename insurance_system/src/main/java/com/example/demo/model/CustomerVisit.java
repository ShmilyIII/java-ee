package com.example.demo.model;

import lombok.Data;

import java.util.Date;
@Data
public class CustomerVisit {
    private int visitId;            //拜访id
    private int customerId;         //客户id
    private Date visitDate;        // 拜访日期
    private String visitType;      // 拜访类型
    private String visitDescription;   // 拜访详情

    public CustomerVisit(int visitId, int customerId, Date visitDate, String visitType,
                         String visitDescription) {
        this.visitId = visitId;
        this.customerId = customerId;
        this.visitDate = visitDate;
        this.visitType = visitType;
        this.visitDescription = visitDescription;
    }

    // getter and setter methods here
}
