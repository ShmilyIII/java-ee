package com.example.demo.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class PolicyInfo {
    private int policyId;     // 保单id
    private int customerId;   // 客户id
    private int productId;   // 保险产品id
    private Date startDate;  // 保险开始时间
    private Date endDate;    // 保险截止时间
    private BigDecimal sumInsured;   // 保险金额
    private BigDecimal premium;      // 保险产品费用
    private BigDecimal amountPaid;   // 保险支付金额
    private BigDecimal outstandingAmount;   // 保险欠款金额

    public PolicyInfo(int policyId, int customerId, int productId, Date startDate, Date endDate,
                      BigDecimal sumInsured, BigDecimal premium, BigDecimal amountPaid, BigDecimal outstandingAmount) {
        this.policyId = policyId;
        this.customerId = customerId;
        this.productId = productId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.sumInsured = sumInsured;
        this.premium = premium;
        this.amountPaid = amountPaid;
        this.outstandingAmount = outstandingAmount;
    }

    // getter and setter methods here
}
