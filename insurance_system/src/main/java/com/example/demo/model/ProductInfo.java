package com.example.demo.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 保险产品表对应的类
 */
@Data
public class ProductInfo {
    private int productId;   //保险产品id
    private String name;    // 保险产品名称
    private String type;   // 保险产品类型
    private String description;   // 保险产品描述
    private BigDecimal sumInsured;   // 保险金额
    private BigDecimal premium;     //保险产品价格

    public ProductInfo(int productId, String name, String type, String description,
                       BigDecimal sumInsured, BigDecimal premium) {
        this.productId = productId;
        this.name = name;
        this.type = type;
        this.description = description;
        this.sumInsured = sumInsured;
        this.premium = premium;
    }

    // getter and setter methods here
}
