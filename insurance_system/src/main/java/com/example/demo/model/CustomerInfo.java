package com.example.demo.model;

import lombok.Data;

import java.util.Date;

/**
 * 客户信息表的类
 */
@Data
public class CustomerInfo {
    private int customerId; // 客户id
    private String name;    // 客户姓名
    private Gender gender;  // 客户性别
    private Date dateOfBirth;   //客户生日
    private String phoneNumber;  // 客户电话
    private String email;   // 客户邮箱
    public enum Gender {
        MALE, FEMALE, OTHER
    }


//    public CustomerInfo(int customerId, String name, String gender, Date dateOfBirth,
//                        String phoneNumber, String email) {
//        this.customerId = customerId;
//        this.name = name;
//
//        this.dateOfBirth = dateOfBirth;
//        this.phoneNumber = phoneNumber;
//        this.email = email;
//    }

    // getter and setter methods here
}
