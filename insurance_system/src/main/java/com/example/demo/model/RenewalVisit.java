package com.example.demo.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class RenewalVisit {
    private int visitId;      // 拜访记录id
    private int policyId;     //保单id
    private Date visitDate;   // 拜访日期
    private String visitType;   // 拜访类型
    private String visitDescription;    //拜访的详情（描述）
    private BigDecimal amountCollected;    //账户收取
    private BigDecimal outstandingAmount;  // 超出金额（欠款）

    public RenewalVisit(int visitId, int policyId, Date visitDate, String visitType,
                        String visitDescription, BigDecimal amountCollected, BigDecimal outstandingAmount) {
        this.visitId = visitId;
        this.policyId = policyId;
        this.visitDate = visitDate;
        this.visitType = visitType;
        this.visitDescription = visitDescription;
        this.amountCollected = amountCollected;
        this.outstandingAmount = outstandingAmount;
    }

    // getter and setter methods here
}
