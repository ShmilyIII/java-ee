public class HomeWork {
    public static void main(String[] args) {
        Thread[]threads=new Thread[20];//20个线程
        for (int i = 0; i <20 ; i++) {
            int finalI = i;
            threads[i]=new Thread(()->{
                System.out.println(finalI);
            });
        }
        for (Thread t: threads) {
            t.start();
        }
        for(Thread t:threads){
            try {
                t.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("ok");

    }
}
