
class MyRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println("hello runnable!");
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) {

        Runnable r=new MyRunnable();
        Thread t=new Thread(r);
        t.start();
    }
}
