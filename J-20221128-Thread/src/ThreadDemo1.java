
class MyThread extends Thread {
    @Override
    public void run() {
        while (true){
            System.out.println("Hello Thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }
}

public class ThreadDemo1 {

    public static void main(String[] args)  {
        Thread thread=new MyThread();
        thread.start();
        while(true){
            System.out.println("Hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
