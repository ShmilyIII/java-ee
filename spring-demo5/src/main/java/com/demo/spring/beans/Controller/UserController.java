package com.demo.spring.beans.Controller;

import com.demo.spring.beans.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    @Autowired
    private User user1;
    public User getUser1(){
        User user=user1;
        user.setId(1);
        user.setName("李四");
        return user;
    }

}
