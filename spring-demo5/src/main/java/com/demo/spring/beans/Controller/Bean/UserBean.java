package com.demo.spring.beans.Controller.Bean;

import com.demo.spring.beans.User;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
public class UserBean {

    @Bean(name = "user")
    //使用getUser方法返回Bean对象
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public User getUser(){
        User user=new User();
        user.setId(1);
        user.setName("张三");
        return user;
    }
}
