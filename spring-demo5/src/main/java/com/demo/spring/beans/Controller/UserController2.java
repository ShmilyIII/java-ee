package com.demo.spring.beans.Controller;

import com.demo.spring.beans.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController2 {
    @Autowired
    private User user3;
    public User getUser3() {
        return user3;
    }
}
