import com.demo.spring.beans.Controller.UserController;
import com.demo.spring.beans.Controller.UserController2;
import com.demo.spring.beans.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context=new
                ClassPathXmlApplicationContext("spring-config.xml");
        UserController userController=  context.getBean("userController", UserController.class);
        User user=userController.getUser1();
        System.out.println("修改后的bean："+ user);
        UserController2 userController2=context.getBean("userController2",UserController2.class);
        User user1=userController2.getUser3();
        System.out.println("最后获取的bean："+user1);
    }
}
