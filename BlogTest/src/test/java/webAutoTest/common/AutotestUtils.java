package webAutoTest.common;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class AutotestUtils {
    public static ChromeDriver driver;
    //创建驱动
    public static ChromeDriver createDriver() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--remote-allow-origins=*");
            driver = new ChromeDriver(options);
            // 隐式等待一下，让页面渲染完成，防止找不到页面中的元素
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            System.out.println("隐式等待让页面渲染完毕！");
        }
        return driver;
    }
}
