package webAutoTest.logined_tests;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

import java.io.File;
import java.io.IOException;

/**
 * 用户已经登录的情况下，对个人主页进行测试
 */
public class myBlogListTest extends AutotestUtils {
    public static ChromeDriver driver = createDriver();
    // 先到登录页面
    @Test
    @BeforeAll
    static void init() {
        driver = createDriver();
        driver.get("http://47.121.28.16:8080/login.html");
    }
    //先进行登录
    @ParameterizedTest // 写了该注解就不用在写@Test注解了(多参数）
    @Order(1)
    @CsvSource({"admin, 123456"})
    void loginRightTest(String username, String password) throws InterruptedException, IOException {
        // 多个账号登录，在重新输入账号时，需要把之前的输入的内容清空
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        alert.accept(); // 选择确认

        // 上述步骤只是说明输入了账号和密码，但还不知道点击提交后是否会跳转到我的博客列表页
        String expect = "http://47.121.28.16:8080/myblog_list.html";
        String actual = driver.getCurrentUrl();
        Assertions.assertEquals(expect, actual); // 查看当前的url是否在我的博客列表页面

    }

    @Test
    @Order(2)
    void pageTest() {
        // 测试是否存在博客列表页的超链接（以及该超链接所显示的文本是否正确，只有在个人主页该超链接的文本才是“首页”这两个字）
        String expect = "主页";
        String actual = driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).getText();
        Assertions.assertEquals(expect, actual);
        // 是否能找到注销
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)"));

        //是否找到写博客
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)"));


        // 测试删除博客按钮是否存在(文本显示是否正确）
        String expect1 = "删除 >>";
        String actual1 = driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a:nth-child(6)")).getText();
        Assertions.assertEquals(expect1, actual1);
        // 测试修改博客按钮是否存在(文本显示是否正确）
        String expect2 = "修改 >>";
        String actual2 = driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a:nth-child(5)")).getText();
        Assertions.assertEquals(expect2, actual2);

        String expect3 = "查看全文 >>";
        String actual3 = driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a:nth-child(4)")).getText();
        Assertions.assertEquals(expect1, actual1);


    }

}
