package webAutoTest.logined_tests;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

import java.io.IOException;

/**
 * 用户登录状态下对博客修改页面进行测试
 */
public class blogUpdateTest extends AutotestUtils {
    public static ChromeDriver driver = createDriver();
    // 先到登录页面
    @Test
    @BeforeAll
    static void init() {
        driver = createDriver();
        driver.get("http://47.121.28.16:8080/login.html");
    }

    //进行登录
    @ParameterizedTest // 写了该注解就不用在写@Test注解了(多参数）
    @Order(1)
    @CsvSource({"admin, 123456"})
    void loginRightTest(String username, String password) throws InterruptedException, IOException {
        // 多个账号登录，在重新输入账号时，需要把之前的输入的内容清空
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        alert.accept(); // 选择确认

        // 上述步骤只是说明输入了账号和密码，但还不知道点击提交后是否会跳转到我的博客列表页
        String expect = "http://47.121.28.16:8080/myblog_list.html";
        String actual = driver.getCurrentUrl();
        Assertions.assertEquals(expect, actual); // 查看当前的url是否在我的博客列表页面
    }


    @Test
    @Order(2)
    void pageTest() throws InterruptedException {
        // 查看修改博客和删除博客按钮是否存在
        driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a:nth-child(6)"));//删除按钮
        driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a:nth-child(5)"));//修改按钮
        //点击修改按钮进入修改页面
        driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a:nth-child(5)")).click();
        // 查看修改页面博客标题是否存在
        driver.findElement(By.cssSelector("#title"));
        //将标题改为测试开发实战2并进行发布
        //先清空标题
        driver.findElement(By.cssSelector("#title")).clear();
        Thread.sleep(300);
        //再写测试开发实战2
        driver.findElement(By.cssSelector("#title")).sendKeys("测试开发实战2");
        Thread.sleep(300);
        driver.findElement(By.cssSelector("body > div.blog-edit-container > div.title > button")).click(); // 点击发布按钮
        // 如果发布成功，会出现一个弹窗——》提示发布成功
        Thread.sleep(100); // 等待弹窗出现
        Alert alert = driver.switchTo().alert();
        alert.dismiss();//关闭弹窗(点取消键)
        Thread.sleep(1000);
        // 页面会跳到我们的博客列表页面，在博客列表页的第一个一个元素看是否能够找到我们刚刚提交的数据
        String actual =driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > div.title")).getText();
        Thread.sleep(1000);
        String expect = "测试开发实战2";
        Assertions.assertEquals(expect, actual);

    }
//    @Test
//    @AfterAll
//    static void exit() {
//        driver.quit();
//    }
}
