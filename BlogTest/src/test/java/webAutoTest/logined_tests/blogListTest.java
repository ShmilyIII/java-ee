package webAutoTest.logined_tests;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

import java.io.File;
import java.io.IOException;

/**
 * 用户登录状态下的博客列表测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 说明当前类中的测试方法要按一定的顺序来执行---和order配合使用
public class blogListTest extends AutotestUtils {
    public static ChromeDriver driver = createDriver();
    // 先到登录页面
    @Test
    @BeforeAll
    static void init() {
        driver = createDriver();
        driver.get("http://47.121.28.16:8080/login.html");
    }

    //进行登录
    @ParameterizedTest // 写了该注解就不用在写@Test注解了(多参数）
    @Order(1)
    @CsvSource({"admin, 123456"})
    void loginRightTest(String username, String password) throws InterruptedException, IOException {
        // 多个账号登录，在重新输入账号时，需要把之前的输入的内容清空
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        alert.accept(); // 选择确认

        // 上述步骤只是说明输入了账号和密码，但还不知道点击提交后是否会跳转到我的博客列表页
        String expect = "http://47.121.28.16:8080/myblog_list.html";
        String actual = driver.getCurrentUrl();
        Assertions.assertEquals(expect, actual); // 查看当前的url是否在我的博客列表页面
    }

    /**
     * 测试总的博客列表页面的完整性
     */
    @Test
    @Order(2)
    void pageTest() throws InterruptedException { // 不能用private修饰该方法——》我们的selenium还要调用该方法
        //点击我的博客列表页的主页按钮进行跳转到主页
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).click();
        Thread.sleep(300);
        // 看看是否能够获取到博客列表页面的翻页按钮（只有总的博客列表页有，个人主页没有）
        driver.findElement(By.cssSelector("#pageDiv > button:nth-child(1)"));//翻页的首页按钮。找到就说明在总博客页
        // 查看页面的文本内容显示是否正确
        String expect = "selenium常用API的使用";
        String actual = driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > div.title")).getText();
        Assertions.assertEquals(expect, actual); // 断言
        // 查看是否有个人主页的超链接并进行跳转
        driver.findElement(By.cssSelector("#isLoginElement > a:nth-child(1)")).click();
        String  expectURL = "http://47.121.28.16:8080/myblog_list.html";
        String actualURL = driver.getCurrentUrl().substring(0, 41);
        // 利用断言看：在登录成功的情况下，界面是否跳转到了个人主页
        Assertions.assertEquals(expectURL, actualURL);
    }


}
