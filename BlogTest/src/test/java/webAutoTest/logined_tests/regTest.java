package webAutoTest.logined_tests;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

import java.io.File;
import java.io.IOException;

/**
 * 注册界面的自动化测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 说明当前该类下面的测试方法要按一定的顺序执行
public class regTest extends AutotestUtils { // 继承用于隐式等待的公共方法
    public static ChromeDriver driver = createDriver();
    @Test      // @Test说明方法 是测试方法，执行当前这个类时，会自动的执行该类下的所有带@Test注解的用例
    @BeforeAll // 带有BeforeAll注解的方法会在当前类下的所有测试用例之前（方法）执行一次，注意只是执行一次
    public static void init() {
        // 既然是对注册界面的测试，自然要先跳转到该界面
        driver.get("http://47.121.28.16:8080/reg.html");
    }

    /**
     * 对页面内容的完整性进行测试
     */
    @Test
    @Order(1)
    public void regPageTest() {
        // 利用断言验证页面显示的文本是否正确
        String expect = "注册";
        String actual = driver.findElement(By.cssSelector("body > div.login-container > div > h3")).getText();
        Assertions.assertEquals(expect, actual); // 如果不正确

        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")); // 检查博客登录页的主页超链接是否存在
        // 检查提交按钮是否存在
        driver.findElement(By.cssSelector("#submit"));
    }
    /**
     * 正常注册
     */
    @ParameterizedTest // 多参数——加了该注解就不用@Test了
    @Order(2)
    @CsvSource({"ABCDEFG, 123456, 123456"}) // 多参数
    public void regRightTest(String username, String password, String password2) throws InterruptedException, IOException {
        // 每次都要提前把之前输入框的内容给清除（不管有没有内容）
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password2")).clear();
        // 将信息填入输入框
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#password2")).sendKeys(password2);
        // 找到提交按钮，并点击提交
        driver.findElement(By.cssSelector("#submit")).click();
        // 强制等待，让弹窗显示出来（避免我们页面还没加载完成，我们下面的代码就尝试获取弹窗
        Thread.sleep(500);

        // 注册成功后，会出现弹窗，获取弹窗并且关闭
        Alert alert = driver.switchTo().alert();
        alert.accept(); // 点击弹窗中的确定，以便让程序继续执行下去
        // 注册成功后，应该会跳转到登录页面
        Thread.sleep(100);
        String expectURL = "http://47.121.28.16:8080/login.html";
        String actualURL = driver.getCurrentUrl(); // 获取当前页面的URL 与登录页面url进行对比
        Assertions.assertEquals(expectURL, actualURL);
        // 获取此时的屏幕截图，此时应该以及跳转到了登录页面
        File srcFile = driver.getScreenshotAs(OutputType.FILE);
        String fileName = "regRightTest.png";
        FileUtils.copyFile(srcFile, new File(fileName));
        // 因为注册成功会跳转到登录界面，所以但接下来我们还有在注册界面测试，所以要回退到注册界面
        driver.navigate().back();
        Thread.sleep(1000);//停留1s给下个测试缓冲

    }
    /**
     * 测试注册失败的情况
     * (admin这个用户名我以及注册过了再次注册，由于用户名的唯一约束，会导致注册失败）
     * (前后两次输入的密码不一致）
     */
    @ParameterizedTest
    @Order(3)
    @CsvSource({ "力量, 123, 123456"})
    public void regFailTest(String username, String password, String password2) throws InterruptedException {
        // 每次输入信息前， 先要清除输入框的原有内容
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password2")).clear();
        // 往输入框中输入数据
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#password2")).sendKeys(password2);
        Thread.sleep(100);
        driver.findElement(By.cssSelector("#submit")).click();
        // 等待弹窗加载完成
        Thread.sleep(1000);
        Alert alert = driver.switchTo().alert(); // 获取弹窗
            String expect = "两次密码输入不一致！请重新输入！";
            String acutal = alert.getText();
            alert.accept();
            Assertions.assertEquals(expect, acutal);//看浏览器的实际弹窗内容是否和我们预期的一样
    }

    /**
     * 关闭注册弹窗
     */
    @Test
    @AfterAll  // 带有AfterAll注解的方法会在当前类下的所有测试用例(方法）执行之后 执行一次，注意只是执行一次
    public static void close() {
        driver.quit();
    }

}
