package webAutoTest.logined_tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

import java.io.IOException;

/**
 * 用户已经登录的状态下，对博客详情页进行测试
 */
public class blogDetailTest extends AutotestUtils {
    public static ChromeDriver driver = createDriver();
    // 先到登录页面
    @Test
    @BeforeAll
    static void init() {
        driver = createDriver();
        driver.get("http://47.121.28.16:8080/login.html");
    }

    //进行登录
    @ParameterizedTest // 写了该注解就不用在写@Test注解了(多参数）
    @Order(1)
    @CsvSource({"admin, 123456"})
    void loginRightTest(String username, String password) throws InterruptedException, IOException {
        // 多个账号登录，在重新输入账号时，需要把之前的输入的内容清空
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        alert.accept(); // 选择确认

        // 上述步骤只是说明输入了账号和密码，但还不知道点击提交后是否会跳转到我的博客列表页
        String expect = "http://47.121.28.16:8080/myblog_list.html";
        String actual = driver.getCurrentUrl();
        Assertions.assertEquals(expect, actual); // 查看当前的url是否在我的博客列表页面
    }

    /**
     * 测试主页的标题是否和详情页一致
     * @throws InterruptedException
     */
    @Test
    @Order(2)
    void pageTest() throws InterruptedException {
        //登录后点击主页到总博客列表页
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).click();
        Thread.sleep(300);
        //获取到第一篇文章的标题，为后续进入详情页对比标题做准备、
        String expect=driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > div.title")).getText();
        //点击第一篇文章的查看详情，跳转到博客详情页
        driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a")).click();
        Thread.sleep(300);
        //获取当前文章标题与前面的标题进行对比
        String actual=driver.findElement(By.cssSelector("#title")).getText();
        //利用断言
        Assertions.assertEquals(expect, actual);

    }

    /**
     * 测试详情页的右侧作者名是否和标题下的一致和测试详情页的各个超链接是否存在
     * @throws InterruptedException
     */

    @Test
    @Order(3)
    void pageTest2() throws InterruptedException {

        //测试右侧的作者名与标题下的作者名是否一致
        //右侧作者名
        String expect=driver.findElement(By.cssSelector("#auther2")).getText();
        //左侧作者名
        String actual=driver.findElement(By.cssSelector("#auther")).getText();
        Assertions.assertEquals(expect, actual);

        //是否可以找到主页、写博客、注销按钮
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)"));//主页
        driver.findElement(By.cssSelector("#isLoginElement > a:nth-child(1)"));//写博客
        driver.findElement(By.cssSelector("#isLoginElement > a:nth-child(2)"));//注销

    }
}
