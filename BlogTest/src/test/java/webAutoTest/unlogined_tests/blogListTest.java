package webAutoTest.unlogined_tests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

/**
 * 用户未登录状态下的博客列表测试和博客详情页进行测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 说明当前类中的测试方法要按一定的顺序来执行---和order配合使用
public class blogListTest extends AutotestUtils {

    private static ChromeDriver driver =createDriver();
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://47.121.28.16:8080/blog_list.html");
    }

    /**
     * 测试总的博客列表页面的完整性
     */
    @Test
    @Order(1)
    void pageTest() throws InterruptedException { // 不能用private修饰该方法——》我们的selenium还要调用该方法
        // 看看是否能够获取到博客列表页面的翻页按钮（只有总的博客列表页有，个人主页没有）
        driver.findElement(By.cssSelector("#pageDiv > button:nth-child(1)"));//翻页的首页按钮。找到就说明在总博客页
        // 查看页面的文本内容显示是否正确
        String expect = "测试开发实战2";
        String actual = driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > div.title")).getText();
        Assertions.assertEquals(expect, actual); // 断言

        //查看是否有登录按钮
        driver.findElement(By.cssSelector("#noLoginElement"));
        //点击查看全文跳转到详情页
        driver.findElement(By.cssSelector("#articlelist > div:nth-child(1) > a")).click();
        //文章标题为测试开发实战2
        String expect2="测试开发实战2";
        String result=driver.findElement(By.cssSelector("#title")).getText();
        Assertions.assertEquals(expect, actual); // 断言

    }
    @Test
    @AfterAll
    static void exit() {
        driver.quit();
    }

}
