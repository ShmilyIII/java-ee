package com.example.demo.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")//路由
public class UserController {
    @RequestMapping("/sayhello")//路由
    public String sayhello(){
        return "Hello SpringBoot";
    }

}
