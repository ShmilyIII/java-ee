package MySynchronized;


class Count{
    private int sum=0;

    public  void increase(){
        synchronized(this){
            sum++;
        }

    }

    public int getSum() {
        return sum;
    }
}
public class SynchronizedDemo1 {
    private int sum=0;

    public synchronized static void fun(int a){
        a++;
    }
    //synchronized修饰普通方法
    public  void increase(){
        synchronized(this){
            sum++;
        }

    }

    public int getSum() {
        return sum;
    }

    public static void main(String[] args) throws InterruptedException {

        Count count=new Count();
        Thread t1=new Thread(()->{
            for(int i=0;i<5000;i++){
                count.increase();
            }
        });

        Thread t2=new Thread(()->{
            for (int i = 0; i <5000 ; i++) {
                count.increase();
            }
        });

        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        t2.join();
        int result=count.getSum();
        System.out.println("结果是");
        System.out.println(result);

    }
}

