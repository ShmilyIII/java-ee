package MySynchronized;

public class SynchronizedDemo3 {
    public static void main(String[] args) {

        Object loker1=new Object();
        Object loker2=new Object();
        Thread t1=new Thread(()->{
            System.out.println("A");
            //A打印完进行通知t2
            synchronized (loker1){
                loker1.notify();
            }
        });

        Thread t2=new Thread(()->{
            //B在A前面，所以让t2阻塞等待
            synchronized (loker1){
                try {
                    loker1.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("B");
            //打印B之后，进行通知
            synchronized (loker2){
                loker2.notify();
            }
        });
        //为保证ABC的顺序，所以我们要保证C要在B的后面
        //所以我们在打印C之前，要让它阻塞等待
        Thread t3=new Thread(()->{
            //阻塞等待，等待t2线程的通知
            synchronized (loker2){
                try {
                    loker2.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("C");
            System.out.println("打印正确！");
        });
        //这里有个细节要注意：如果t1比t2先执行了，那么t1中的通知就失去了作用，也就是说t2还没有排队等待呢，t1已经通知了，这个时候通知就没有用了
        //等到t2等待的时候，t1就没有等二次通知的机会了，所以要让t2先执行。
        t2.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        t1.start();
        t3.start();
    }
}
