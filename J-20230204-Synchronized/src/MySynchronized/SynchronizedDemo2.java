package MySynchronized;

import java.util.Scanner;

class MyCount{
    volatile public int flag=0;
}

public class SynchronizedDemo2 {
    public static void main(String[] args) {

        MyCount myCount=new MyCount();

        Thread t1=new Thread(()->{
            while(myCount.flag==0){

            }
            System.out.println("循环结束！");

        });

        Thread t2=new Thread(()->{
            Scanner scan=new Scanner(System.in);
            System.out.println("输入你要输入的数字：");
            int result=scan.nextInt();
            myCount.flag=result;

        });
        t1.start();
        t2.start();
    }
}
