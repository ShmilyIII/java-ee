package Thread;

import static java.lang.Thread.sleep;
//第一种创建多线程的方法
//子类继承Thread,重写run方法

//第二种创建多线程方法
//实现Runable，重写run方法

//第三种创建多线程的方法
//Thread使用匿名内部类

//第四种创建多线程的方法
//Runable使用匿名内部类

//第五种创建多线程的方法
//使用lambda表达式

//1、
//class  MyThread extends Thread{
//
//    @Override
//    public void run() {
//        while(true) {
//            try {
//                sleep(1000);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("hello Thread");
//        }
//    }
//
//
//
//}
//public class ThreadDemo1 {
//    public static void main(String[] args) {
//        Thread t=new MyThread();
//        t.start();
//
//
//        while(true){
//            try {
//                sleep(1000);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("hello");
//        }
//
//    }
//}
//2、

//class Myrunable implements Runnable{
//
//    @Override
//    public void run() {
//        while(true){
//            try {
//                sleep(1000);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("hello Thread");
//        }
//
//    }
//}
//public class ThreadDemo1 {
//    public static void main(String[] args) {
//        Runnable runnable=new Myrunable();
//        Thread t=new Thread(runnable);
//        t.start();
//
//
//        while(true){
//            try {
//                sleep(1000);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("hello");
//        }
//
//    }
//}

//3、
//public class ThreadDemo1 {
//    public static void main(String[] args) {
//
//        Thread t=new Thread(){
//            @Override
//            public void run() {
//                while (true){
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
//                    System.out.println("hello Thread");
//                }
//            }
//        };
//        t.start();
//
//
//        while(true){
//            try {
//                sleep(1000);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("hello");
//        }
//
//    }
//}

//4、

//public class ThreadDemo1 {
//    public static void main(String[] args) {
//        Runnable runnable=new Runnable() {
//            @Override
//            public void run() {
//                while(true){
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
//                    System.out.println("hello Thread");
//                }
//            }
//        };
//        Thread t=new Thread(runnable);
//        t.start();
//
//
//        while(true){
//            try {
//                sleep(1000);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("hello");
//        }
//
//    }
//}

//5、
public class ThreadDemo1 {
    public static void main(String[] args) {

        Thread t=new Thread(()->{
            while(true){
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("hello Thread");
            }
        });
        t.start();


        while(true){
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("hello");
        }

    }
}




